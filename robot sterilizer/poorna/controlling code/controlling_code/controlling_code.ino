//header 
#include <MemoryFree.h>
#include <EEPROM.h>
// Stepper Configer
#define m1_1   54   //STEP_PIN 
#define m1_2   55   //DIR_PIN
#define m1_3   38   //ENABLE_PIN

#define m2_1   60   //STEP_PIN
#define m2_2   61   //DIR_PIN
#define m2_3   56   //ENABLE_PIN

#define m3_1   46   //STEP_PIN
#define m3_2   48   //DIR_PIN 
#define m3_3   62   //ENABLE_PIN

#define m4_1   26   //STEP_PIN 
#define m4_2   28   //DIR_PIN
#define m4_3   24   //ENABLE_PIN

#define m5_1   36   //STEP_PIN
#define m5_2   34   //DIR_PIN
#define m5_3   30   //ENABLE_PIN 
const int stepsPerRevolution = 600;
//limit switch 
int sw_1=40;
int sw_2=42;
int relay=63;
void  forward()
{
 Serial.println("forward");
 digitalWrite(m1_2, HIGH);
 digitalWrite(m2_2, HIGH);
 digitalWrite(m3_2, HIGH);
 digitalWrite(m4_2, HIGH);
  for(int x = 0; x < stepsPerRevolution; x++)
  {
    digitalWrite(m1_1, HIGH);
    digitalWrite(m2_1, HIGH);
    digitalWrite(m3_1, HIGH);
    digitalWrite(m4_1, HIGH);
    delayMicroseconds(500);
    digitalWrite(m1_1, LOW);
    digitalWrite(m2_1, LOW);
    digitalWrite(m3_1, LOW);
    digitalWrite(m4_1, LOW); 
    delayMicroseconds(500);
  }  
}
void backward()
{
 Serial.println("backward");
 digitalWrite(m1_2,LOW);
 digitalWrite(m2_2,LOW);
 digitalWrite(m3_2,LOW);
 digitalWrite(m4_2,LOW);
  for(int x = 0; x < stepsPerRevolution; x++)
  {
    digitalWrite(m1_1, HIGH);
    digitalWrite(m2_1, HIGH);
    digitalWrite(m3_1, HIGH);
    digitalWrite(m4_1, HIGH);
    delayMicroseconds(500);
    digitalWrite(m1_1, LOW);
    digitalWrite(m2_1, LOW);
    digitalWrite(m3_1, LOW);
    digitalWrite(m4_1, LOW); 
    delayMicroseconds(500);
  }  
}
void right()
{
   Serial.println("right");
   digitalWrite(m1_2,LOW);
   digitalWrite(m2_2, HIGH);
   digitalWrite(m3_2, LOW);
   digitalWrite(m4_2, HIGH);
  for(int x = 0; x < stepsPerRevolution; x++)
  {
    digitalWrite(m1_1, HIGH);
    digitalWrite(m2_1, HIGH);
    digitalWrite(m3_1, HIGH);
    digitalWrite(m4_1, HIGH);
    delayMicroseconds(500);
    digitalWrite(m1_1, LOW);
    digitalWrite(m2_1, LOW);
    digitalWrite(m3_1, LOW);
    digitalWrite(m4_1, LOW);
    delayMicroseconds(500);
  }
  }
void left ()
{
  Serial.println("left");
   digitalWrite(m1_2,HIGH);
   digitalWrite(m2_2,LOW);
   digitalWrite(m3_2,HIGH);
   digitalWrite(m4_2,LOW);
  for(int x = 0; x < stepsPerRevolution; x++)
  {
    digitalWrite(m1_1, HIGH);
    digitalWrite(m2_1, HIGH);
    digitalWrite(m3_1, HIGH);
    digitalWrite(m4_1, HIGH);
    delayMicroseconds(500);
    digitalWrite(m1_1, LOW);
    digitalWrite(m2_1, LOW);
    digitalWrite(m3_1, LOW);
    digitalWrite(m4_1, LOW);
    delayMicroseconds(500);
  }
}
void stop1()
{
 Serial.println("stop");
  digitalWrite(m1_3,LOW);
  digitalWrite(m2_3,LOW);
  digitalWrite(m3_3,LOW);
  digitalWrite(m4_3,LOW);
  digitalWrite(m5_3,LOW);
}
void up()
{
  Serial.println("up");
  digitalWrite(m5_2, HIGH);
 for(int x = 0; x < 50000; x++)
 {
  if(digitalRead(sw_2)==HIGH)
  {
    digitalWrite(m5_1,HIGH);
    delayMicroseconds(500);
    digitalWrite(m5_1, LOW);
    delayMicroseconds(500);
  }
  else
  {
    lighton();
    serialEvent3();
  }
     }
}
void down()
{
  Serial.println("down");
  lightoff();
  digitalWrite(m5_2,LOW);
 for(int x = 0; x < 50000; x++)
 {
  if(digitalRead(sw_1)==HIGH)
  {
    digitalWrite(m5_1, HIGH);
    delayMicroseconds(500);
    digitalWrite(m5_1, LOW);
    delayMicroseconds(500);
 }
 else
  {
    
    serialEvent3();
  }
   }
   }
void lighton()
{
   digitalWrite(relay,HIGH);
   Serial.println("light on");
}
void lightoff()
{
   digitalWrite(relay,LOW);
    Serial.println("light off");
}
void setup()
{
  Serial.begin(115200);
  Serial3.begin(115200);
  pinMode(sw_1,INPUT_PULLUP);
  pinMode(sw_2,INPUT_PULLUP);
  pinMode(relay,OUTPUT);
  digitalWrite(relay,LOW);
  
  pinMode(m1_1,OUTPUT);
  pinMode(m1_2,OUTPUT);
  pinMode(m1_3,OUTPUT);

  pinMode(m2_1,OUTPUT);
  pinMode(m2_2,OUTPUT);
  pinMode(m2_3,OUTPUT);

  pinMode(m3_1,OUTPUT);
  pinMode(m3_2,OUTPUT);
  pinMode(m3_3,OUTPUT);

  pinMode(m4_1,OUTPUT);
  pinMode(m4_2,OUTPUT);
  pinMode(m4_3,OUTPUT);
  
  pinMode(m5_1,OUTPUT);
  pinMode(m5_2,OUTPUT);
  pinMode(m5_3,OUTPUT);
  digitalWrite(m1_3,HIGH);
  digitalWrite(m2_3,HIGH);
  digitalWrite(m3_3,HIGH);
  digitalWrite(m4_3,HIGH);

  
  Serial3.println("AT+CIPMUX=1");
  delay(2000);
  Serial3.println("AT+CIPSERVER=1,5000");
  delay(2000);
  Serial3.println("AT+CIPSTO=3600");
  delay(2000);
}
void loop()
{
  
}
void serialEvent3() {
while(Serial3.available())
  {
    char Rdata;
    Rdata=Serial3.read();
    Serial.println(Rdata);
    Serial.write(Rdata);
    switch(Rdata)
    {
      case 'f':
      Serial.println("forward");
            forward();
            break;
      case 'w':
            Serial.println("backward");
            backward();
            break;
      case 'q': 
            Serial.println("right");
            right();
            break;
      case 'k':
             Serial.println("left");
             left();
             break;
      case 'z':
             Serial.println("stop");
             stop1();
             break; 
      case 'U':
             Serial.println("UP");
             up();
             break; 
      case '$':
             Serial.println("DOWN");
             down();
             break;                             
    }  
    }
}
