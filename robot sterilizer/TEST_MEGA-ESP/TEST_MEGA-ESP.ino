#include <MemoryFree.h>
#include <EEPROM.h>

#define PIN_LED1 4 
#define PIN_LED2 5 
#define PIN_LED3 6 
#define PIN_LED4 7   
String inString;
void setup() {
  
  Serial.begin(115200);
  Serial3.begin(115200);
  pinMode(PIN_LED1, OUTPUT);
  digitalWrite(PIN_LED1, LOW);
  pinMode(PIN_LED2, OUTPUT);
  digitalWrite(PIN_LED2, LOW);
  pinMode(PIN_LED3, OUTPUT);
  digitalWrite(PIN_LED3, LOW);
  pinMode(PIN_LED4, OUTPUT);
  digitalWrite(PIN_LED4, LOW);
}
void loop() {
}
void serialEvent3() {
  while (Serial3.available()) {
    char inChar = Serial3.read();
    Serial.write(inChar);
    //Serial.println(inChar);
    inString += inChar;
    if (inChar == 'F') {
      if (inString.indexOf("F")>0) {
        digitalWrite(PIN_LED1,HIGH);
      }
      else if (inString.indexOf("B")>0) {
         //Serial.println("BACKWARD");
         digitalWrite(PIN_LED2,HIGH);
      }
      else if (inString.indexOf("L")>0) {
         //Serial.println("LEFT");
         digitalWrite(PIN_LED3,HIGH);
      }
      else if (inString.indexOf("R")>0) {
       // Serial.println("RIGHT");
        digitalWrite(PIN_LED4,HIGH);
      }
      else if (inString.indexOf("S")>0) {
        Serial.println("STOP");
        digitalWrite(PIN_LED1, LOW);
        digitalWrite(PIN_LED2, LOW);
        digitalWrite(PIN_LED3, LOW);
        digitalWrite(PIN_LED4, LOW);
        
      }
      else
      {
        Serial.println("Wrong command");
      }
      inString = "";
    }
  }
}
