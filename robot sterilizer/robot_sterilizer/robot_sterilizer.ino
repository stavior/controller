//HEADER
#include <Arduino.h>
#include <menu.h>
#include <menuIO/u8g2Out.h>
 #include <menuIO/encoderIn.h>
// #include <menuIO/keyIn.h>
#include <menuIO/chainStream.h>
#include <menuIO/serialOut.h>
#include <menuIO/serialIn.h>
#include <menuIO/U8GLibOut.h>
//https://www.arduino.cc/reference/en/libraries/arduinomenu-library/


#include <TimerOne.h>
#include <ClickEncoder.h>
#include <menuIO/clickEncoderIn.h>

#include <SD.h>

#include <ArduinoJson.h>
//RTC
#include <RTClib.h>
#include <Time.h>
#include <TimeLib.h>
char t[32];

 int Year =2020;
 int Month=10;
 int Date=23;

 int Hr = 00;
 int Min = 00;

//#include "WorldClock.h"
//Sensor Lib
#include <DallasTemperature.h>
#include <OneWire.h>
#include <GravityTDS.h>

using namespace Menu;

#define LEDPIN LED_BUILTIN

// rotary encoder pins
#define encA    33   //31
#define encB    31   //33
#define encBtn  35   //35

//EPROM
#include <EEPROMex.h>

//WatchDogTimer
#include <avr/wdt.h>

//Define
#define BUZZER 37
#define DEBUG 0                                // change value to 1 to enable debuging using serial monitor  
#define SERIAL_PORT_SPEED 115200                //9600, 19200, 38400, 57600, 115200
#define PH_PIN A9                         //pH meter Analog output to Arduino Analog Input 0
//#define TDS_PIN A9                       //TDS sensor
//#define WATER_TURBIDITY_PIN A2             // Water & Nutrient Turbidity 
//#define WATER_PUMP_PIN 2  // Flow Meter Pin number
#define NUTRIENT_PUMP_PIN 3  // Flow Meter Pin number


#define ONE_WIRE_BUS 59   // Pin 4 is used for SD card in ethernet shield
#define STATUS_LED 13


#define STEP_MODE 4 // (1: Full Step, 2: Half Step, 4: Quarter Step, ...)

//Doser Stepper Configer

#define PHLOW_STEP_PIN         54
#define PHLOW_DIR_PIN          55
#define PHLOW_ENABLE_PIN       38

#define PHHIGH_STEP_PIN         60
#define PHHIGH_DIR_PIN          61
#define PHHIGH_ENABLE_PIN       56

#define EC_A_STEP_PIN           46
#define EC_A_DIR_PIN            48
#define EC_A_ENABLE_PIN         62

#define EC_B_STEP_PIN           26
#define EC_B_DIR_PIN            28
#define EC_B_ENABLE_PIN         24

#define EC_C_STEP_PIN           36
#define EC_C_DIR_PIN            34
#define EC_C_ENABLE_PIN         30

//********************************** ENCODER

ClickEncoder clickEncoder(encA,encB,encBtn,2);
ClickEncoderStream encStream(clickEncoder,1);
MENU_INPUTS(in,&encStream);
void timerIsr() {clickEncoder.service();}


//********************Display Configuration **************************
#include <SPI.h>
  #define USE_SWSPI
  #define U8_DC 17
  #define U8_CS 16
  #define U8_RST 23
  #define fontName  u8g2_font_5x7_tf // u8g2_font_synchronizer_nbp_tf           
//  #define fontX 5
//  #define fontY 9
  #define offsetX 0
  #define offsetY 0
  #define U8_Width 128
  #define U8_Height 64
  int x=0; 
  int scroll_direction=1;                 //Direction of title scroll, 1 right, -1 left//Offset postion of title  
//U8GLIB_ST7920_128X64_4X u8g(23, 17, 16);  // SPI Com: SCK = en = 18, MOSI = rw = 16, CS = di = 17
U8G2_ST7920_128X64_F_SW_SPI u8g2(U8G2_R0, /* clock=*/ 23 /* A4 */ , /* data=*/ 17 /* A2 */, /* CS=*/ 16 /* A3 */, /* reset=*/ U8X8_PIN_NONE);

#define LEDPIN 13

//************SD Card ********///
#define SD_DETECT_PIN   49
#define SDSS            53
File sdcard_file;


GravityTDS gravityTds;

OneWire oneWireBus(ONE_WIRE_BUS);
DallasTemperature sensors (&oneWireBus);

RTC_DS3231 rtc; //for date time
char daysOfTheWeek[7][12] = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};

float phsetvalue ;
float phSetHysteris ;
int phplus;
int phmin = 0;
float ECA ;
float ECB ;
float ECC ;
float temp1 = 0;


//**** hysteris****//
float pH= 0.1; //generates the value of pH
float phUp = 2; 
float phDown = 2; // 

float EC; //generates the value of EC
int ECUp = 100; 
int ECDown = 50; 


float ECSetpoint; 
float ECHysterisMin;
float ECHysterisPlus;


int autopin = LOW;

bool autostart;
bool EcCalStart;
bool TankM;
bool ScheduleAuto;
bool Resetfun;

//****************************************** Water Level US Sensor **************************************************************
#define RORelay 8

const int trigPin = 57;
const int echoPin = 58;
long duration;
long distance;
float waterHeight;
float tankHeight ; //enter your water tank height here in [cm]
float totalVolume ; // volume here is in [cm^3], change depending on tank volume
float waterQuantity;
float volume;
float pi = 3.14159265359;
float offset = 245;
//Water distance
float Wtime=0,Wdistance=0;
int Wtemp=0;
//***********************************************************************************************************************

#define Offset 0.00 //deviation compensate
#define LED 13
#define samplingInterval 20
#define printInterval 800
#define ArrayLenth 40 //times of collection
int pHArray[ArrayLenth]; //Store the average value of the sensor feedback
int pHArrayIndex = 0;

//float Setpoint; 
float HysterisMin;
float HysterisPlus;
//float SetHysteris;

//float Setpoint = 4.5;
//float SetHysteris = 0.5;

long previousMillis = 0; //             
long pinHighTime = 100; //             
long pinLowTime = 100; // OLD 7500             
long pinTime = 100; // 

long ECpreviousMillis = 0; //             |
long ECpinHighTime = 100; //             |
long ECpinLowTime = 7500; //             |
long ECpinTime = 100; // 

//*******Sensor Int***********///


//*******EC SENSOR************///

#define TdsSensorPin A11
#define VREF 5.0 // analog reference voltage(Volt) of the ADC
#define SCOUNT 30 // sum of sample point
int analogBuffer[SCOUNT]; // store the analog value in the array, readfrom ADC
int analogBufferTemp[SCOUNT];
int analogBufferIndex = 0,copyIndex = 0;
float averageVoltage = 0,tdsValue = 0,temperature = 25;
float ecValue=0;

float nutrientTDS = 0;
float nutrientEC = 0;
float nutrientCF = 0;
float nutrientTemperature;
float nutrientPH;
float Fahrenheit=0;
unsigned long currentTime;
unsigned long int avgValue;  //Store the average value of the sensor feedback
int buf[10],temp;

//***EPROM***//
//*PH*//
int EeprompHSetpoint = 10;      //location of pHSetpoint in Eeprom
int EepromSetpHHysteris = 20;   //location of SetpHHysteris in Eeprom

//*EC*//
int EepromECSetpoint = 40;      //location of ECSetpoint in Eeprom
int EepromSetECHysteris = 50;   //location of SetECHysteris in Eeprom

//***Time***save eprom adress///

int Eepromhur = 200;
int Eeprommin = 203;

int Eepromhur1 = 206;
int Eeprommin1 = 209;

int Eepromhur2 = 212;
int Eeprommin2 = 215;

int Eepromhur3 = 218;
int Eeprommin3 = 221;

int Eepromhur4 = 224;
int Eeprommin4 = 227;

int Eepromhur5 = 230;
int Eeprommin5 = 233;

int EepromDate = 240;
int EepromMonth = 245;
int EepromYear = 250;

int EeprompHs1 = 300;
int EeprompHs2 = 305;
int EeprompHs3 = 310;
int EeprompHs4 = 315;
int EeprompHs5 = 320;
int EepromECs1 = 325;
int EepromECs2 = 330;
int EepromECs3 = 335;
int EepromECs4 = 340;
int EepromECs5 = 345;
//********************** NL ***********
//int EepromNTHset = 90;
//int EepromNTVset = 91;
//

//*************************************
int test=55;

char Tomato = -1;
char Onion= 1;
char Basil=2;
char Beetroot =3;
int Crop= Tomato;


// define menu colors --------------------------------------------------------
//each color is in the format:
//  {{disabled normal,disabled selected},{enabled normal,enabled selected, enabled editing}}
// this is a monochromatic color table
const colorDef<uint8_t> colors[6] MEMMODE={
  {{0,0},{0,1,1}},//bgColor
  {{1,1},{1,0,0}},//fgColor
  {{1,1},{1,0,0}},//valColor
  {{1,1},{1,0,0}},//unitColor
  {{0,1},{0,0,1}},//cursorColor
  {{1,1},{1,0,0}},//titleColor
};

result doAlert(eventMask e, prompt &item);

result showEvent(eventMask e,navNode& nav,prompt& item) {
  Serial.print("event: ");
  Serial.println(e);
  return proceed;
}


result action1(eventMask e,navNode& nav, prompt &item) {
  Serial.print(e);
  Serial.println(" action1 executed, proceed menu");
  Serial.flush();
  return proceed;
}

result action2(eventMask e,navNode& nav, prompt &item) {
  Serial.print(e);
  Serial.print(" action2 executed, quiting menu");
  return quit;
}

result home(eventMask e,navNode& nav, prompt &item) {
  Serial.print(e);
  Serial.print(" home");
  return quit;
}


int ledCtrl=LOW;

result myLedOn() {
  ledCtrl=HIGH;
  digitalWrite (BUZZER,HIGH);
  return proceed;
}
result myLedOff() {
  ledCtrl=LOW;
  digitalWrite (BUZZER,LOW);
  return proceed;
}

TOGGLE(ledCtrl,setLed,"Led: ",doNothing,noEvent,noStyle
  ,VALUE("On",HIGH,doNothing,noEvent)
  ,VALUE("Off",LOW,doNothing,noEvent)
);

bool SPstart;
TOGGLE(SPstart,SPset,"PH Calibiration ",doNothing,noEvent,noStyle//,doExit,enterEvent,noStyle
  ,VALUE("OFF",true,doNothing,enterEvent)
  ,VALUE("ON",false,doExit,noEvent)
);

TOGGLE(autostart,AutoDose,"Auto Dosing :",doNothing,noEvent,noStyle//,doExit,enterEvent,noStyle
  ,VALUE("STOP",true,doNothing,enterEvent)
  ,VALUE("START",false,doExit,noEvent)
);

TOGGLE(ScheduleAuto,AutoSchedule,"Auto Schedule :",doNothing,noEvent,noStyle//,doExit,enterEvent,noStyle
  ,VALUE("STOP",true,doNothing,enterEvent)
  ,VALUE("START",false,doExit,noEvent)
);

TOGGLE(EcCalStart,EcCalDose,"EC Calibiration ",doNothing,noEvent,noStyle//,doExit,enterEvent,noStyle
  ,VALUE("STOP",true,doNothing,enterEvent)
  ,VALUE("START",false,doExit,noEvent)
);

TOGGLE(TankM,Tankcontrol,"Tank Management ",doNothing,noEvent,noStyle//,doExit,enterEvent,noStyle
  ,VALUE("STOP",true,doNothing,enterEvent)
  ,VALUE("START",false,doExit,noEvent)
);
int selTest=0;
SELECT(selTest,selMenu,"Select",doNothing,noEvent,noStyle
  ,VALUE("Zero",0,doNothing,noEvent)
  ,VALUE("One",1,doNothing,noEvent)
  ,VALUE("Two",2,doNothing,noEvent)
);


result DoserautoOn() {
  autostart = true;
  return proceed;
}
result DoserautoOff() {
  autostart = false;
  return proceed;
}

result schedautoOn() {
  ScheduleAuto = true;
  return proceed;
}
result schedautoOff() {
  ScheduleAuto = false;
  return proceed;
}
result SPautoOn() {
  SPstart = true;
  return proceed;
}
result SPautoOff() {
  SPstart = false;
  return proceed;
}

result EcCaloff() {
  EcCalStart = true;
  return proceed;
}
result EcCalon() {
  EcCalStart = false;
  return proceed;
}

result TankMgStart() {
  TankM = true;
  return proceed;
}
result TankMgStop() {
  TankM = false;
  return proceed;
}

float SetpHHysteris;
float pHSetpoint ;

MENU(phsubmenu," pH",showEvent,anyEvent,noStyle
  ,SUBMENU(SPset)
  ,FIELD(pHSetpoint," pH Set: ","",0,14,0.20,1,phset,anyEvent,wrapStyle)
  
  ,FIELD(phSetHysteris," pH SetHys: ","",0,2,0.2,1,phsetHys,anyEvent,wrapStyle)
  ,EXIT("<Back")
  
);

///*********EPROM*******************************************************************************************///
result phset() {
  EEPROM.updateFloat(EeprompHSetpoint, pHSetpoint);  // 6.5 pH for tomato growth
  
  return proceed;
}

result phsetHys() {
  
  EEPROM.writeFloat(EepromSetpHHysteris, phSetHysteris);
  return proceed;
}

void EepromRead()
{
  pHSetpoint = EEPROM.readFloat(EeprompHSetpoint);
  phSetHysteris = EEPROM.readFloat(EepromSetpHHysteris);
}




//******************************  MANUAL DOSING ******************************************************************************

MENU(MDoseMenu," Manual Dose",showEvent,noEvent,noStyle
  ,FIELD(phplus," pH Plus  ","ml",0,100,10,1,doNothing,enterEvent,wrapStyle)
  ,OP("        Confirm To Dose",manualdoseplus,enterEvent)
  
  ,FIELD(phmin," pH Minus  ","ml",0,100,10,1,doNothing,enterEvent,wrapStyle)
  ,OP("        Confirm To Dose",manualdosemin,enterEvent)

  ,FIELD(ECA," Nutrient A ","ml",0,100,10,1,doNothing,enterEvent,wrapStyle)
  ,OP("       Confirm To Dose",manualdoseEcA,enterEvent)
  ,FIELD(ECB," Nutrient B  ","ml",0,100,10,1,doNothing,enterEvent,wrapStyle)
  ,OP("       Confirm To Dose",manualdoseEcB,enterEvent)
  ,FIELD(ECC," Nutrient C  ","ml",0,100,10,1,doNothing,enterEvent,wrapStyle)
  ,OP("       Confirm To Dose",manualdoseEcC,enterEvent) 
   ,OP("       RESET",MDreset,enterEvent)
  ,EXIT("<Back")
);



result MDreset(){

  {
    float phplus,phmin,ECA,ECB,ECC =0;
    }

 
  return ;
   
  }

int oneml = 125;

result manualdoseplus(){

  digitalWrite(PHLOW_DIR_PIN, HIGH);

 // Motor rotate with ml input
  for(int x = 0; x < (oneml*phplus); x++)
  {
    digitalWrite(PHLOW_STEP_PIN, HIGH);
    delayMicroseconds(500);
    digitalWrite(PHLOW_STEP_PIN, LOW);
    delayMicroseconds(500);
  
    }
  
  return ;
   
  }

int manualdosemin(){
  
  digitalWrite(PHHIGH_DIR_PIN, HIGH);

 // Spin motor slowly
  for(int x = 0; x < (oneml*phmin); x++)
  {
    digitalWrite(PHHIGH_STEP_PIN, HIGH);
    delayMicroseconds(500);
    digitalWrite(PHHIGH_STEP_PIN, LOW);
    delayMicroseconds(500);
  }
  return proceed;
  }
  

result manualdoseEcA(){

  digitalWrite(EC_A_DIR_PIN, HIGH);

 // Motor rotate with ml input
  for(int x = 0; x < (oneml*ECA); x++)
  {
    digitalWrite(EC_A_STEP_PIN, HIGH);
    delayMicroseconds(500);
    digitalWrite(EC_A_STEP_PIN, LOW);
    delayMicroseconds(500);
  
    }
  
  return ;
   
  }

int manualdoseEcB(){
  
  digitalWrite(EC_B_DIR_PIN, HIGH);

 // Spin motor slowly
  for(int x = 0; x < (oneml*ECB); x++)
  {
    digitalWrite(EC_B_STEP_PIN, HIGH);
    delayMicroseconds(500);
    digitalWrite(EC_B_STEP_PIN, LOW);
    delayMicroseconds(500);
  }
  return proceed;
  }

int manualdoseEcC(){
  
  digitalWrite(EC_C_DIR_PIN, HIGH);

 // Spin motor slowly
  for(int x = 0; x < (oneml*ECC); x++)
  {
    digitalWrite(EC_C_STEP_PIN, HIGH);
    delayMicroseconds(500);
    digitalWrite(EC_C_STEP_PIN, LOW);
    delayMicroseconds(500);
  }
  return proceed;
  } 

//*********************************************************************************************************

float ECSetHysteris;
float ECsetvalue;

float SetECHysteris;

MENU(ecsubmenu," EC",showEvent,anyEvent,noStyle
  ,SUBMENU(EcCalDose)
  ,FIELD(ECsetvalue," EC Set:  ","PPM",0,5,1,0.1,ECset,anyEvent,wrapStyle)
  ,FIELD(ECSetHysteris," EC SetHys: ","",0,5,1,0.1,ECsethys,anyEvent,wrapStyle)

  ,EXIT("<Back")
);
///*********EPROM********************///
result ECset() {
  EEPROM.updateFloat(EepromECSetpoint, ECsetvalue);  
 
  return proceed;
}
result ECsethys() {
 
  EEPROM.writeFloat(EepromSetECHysteris, ECSetHysteris);
  return proceed;
}
void EepromReadEC()
{
  ECsetvalue = EEPROM.readFloat(EepromECSetpoint);
  ECSetHysteris = EEPROM.readFloat(EepromSetECHysteris);
}
//************************************************************************************************************

MENU(CalMenu," CALIBRATION",showEvent,anyEvent,noStyle
  ,SUBMENU(phsubmenu)
  ,SUBMENU(ecsubmenu)
  ,EXIT("<Back")
);

TOGGLE(ledCtrl,pumpset,"       Pump : ",doNothing,noEvent,noStyle
  ,VALUE("On",HIGH,doNothing,noEvent)
  ,VALUE("Off",LOW,doNothing,noEvent)
);

MENU(tanksetting,"      TANK SETTING",showEvent,anyEvent,noStyle
  ,SUBMENU(Tankcontrol)
  ,FIELD(tankHeight,"    Height : ","cm",0,500,10,1,NTHset,anyEvent,wrapStyle)
  ,FIELD(totalVolume,"    Volume : ","cm^3",0,5000,100,1,NTVset,anyEvent,wrapStyle)
  ,OP("          SET",EepromReadNT,enterEvent)
    
  ,EXIT("<Back")
);

MENU(WLmenu," TANK LEVEL",showEvent,noEvent,noStyle
  
  ,FIELD(distance,"      Level  "," cm",0,0,0,0,doNothing,noEvent,noStyle)
  ,FIELD(waterHeight,"     Height  "," cm",0,0,0,0,doNothing,noEvent,noStyle)
  ,FIELD(waterQuantity,"   Quantity  "," ml",0,0,0,0,doNothing,noEvent,noStyle)
  ,SUBMENU(pumpset)
  ,SUBMENU(tanksetting)
  
  ,EXIT("<Back")
);



//*********************************************************************************************************



float EepromNTHset=90;
float EepromNTVSet=94;

///*********EPROM********************///
result NTHset() {
  EEPROM.updateFloat(EepromNTHset, tankHeight);  
  
  return proceed;
}
result NTVset() {
  
  EEPROM.writeFloat(EepromNTVSet, totalVolume);
  return proceed;
}

void EepromReadNT()
{
  tankHeight = EEPROM.readFloat(EepromNTHset);
  totalVolume = EEPROM.readFloat(EepromNTVSet);
}
//****************TIME AND DATE**************************************************************************************************
int S1Hr;
int S1Min;
//unsigned long secs;

int S2Hr;
int S2Min;
//unsigned long secs0;

int S3Hr;
int S3Min;
//unsigned long secs1;

int S4Hr;
int S4Min;
//unsigned long secs1;

int S5Hr;
int S5Min;
//unsigned long secs1;

uint16_t hrs;
uint16_t mins;
uint16_t secs;



//define a pad style menu (single line menu)
//here with a set of fields to enter a date in YYYY/MM/DD format
altMENU(menu,timeMenu0,"Edit",showEvent,enterEvent,noStyle,(systemStyles)(_asPad|Menu::_menuData|Menu::_canNav|_parentDraw)
  ,FIELD(S1Hr,":"," (HH)",0,23,1,0,Shift1Clockhr,enterEvent,wrapStyle)
  ,FIELD(S1Min,""," (MM)",0,59,10,1,Shift1Clockmin,enterEvent,wrapStyle)
  
  //,FIELD(secs,"","",0,59,10,1,doNothing,noEvent,wrapStyle)
);

altMENU(menu,timeMenu1,"Edit",showEvent,enterEvent,noStyle,(systemStyles)(_asPad|Menu::_menuData|Menu::_canNav|_parentDraw)
  ,FIELD(S2Hr,":"," (HH)",0,23,1,0,Shift2Clock,enterEvent,wrapStyle)
  ,FIELD(S2Min,""," (MM)",0,59,10,1,Shift2Clock,enterEvent,wrapStyle)
//  ,FIELD(secs,"","",0,59,10,1,doNothing,enterEvent,wrapStyle)
);
altMENU(menu,timeMenu2,"Edit",showEvent,enterEvent,noStyle,(systemStyles)(_asPad|Menu::_menuData|Menu::_canNav|_parentDraw)
  ,FIELD(S3Hr,":"," (HH)",0,23,1,0,Shift3Clock,enterEvent,wrapStyle)
  ,FIELD(S3Min,""," (MM)",0,59,10,1,Shift3Clock,enterEvent,wrapStyle)
//  ,FIELD(secs,"","",0,59,10,1,doNothing,noEvent,wrapStyle)
);
altMENU(menu,timeMenu3,"Edit",showEvent,enterEvent,noStyle,(systemStyles)(_asPad|Menu::_menuData|Menu::_canNav|_parentDraw)
  ,FIELD(S4Hr,":"," (HH)",0,23,1,0,Shift4Clock,enterEvent,wrapStyle)
  ,FIELD(S4Min,""," (MM)",0,59,10,1,Shift4Clock,enterEvent,wrapStyle)
//  ,FIELD(secs,"","",0,59,10,1,doNothing,noEvent,wrapStyle)
);
altMENU(menu,timeMenu4,"Edit",showEvent,enterEvent,noStyle,(systemStyles)(_asPad|Menu::_menuData|Menu::_canNav|_parentDraw)
  ,FIELD(S5Hr,":"," (HH)",0,23,1,0,Shift5Clock,enterEvent,wrapStyle)
  ,FIELD(S5Min,""," (MM)",0,59,10,1,Shift5Clock,enterEvent,wrapStyle)
//  ,FIELD(secs,"","",0,59,10,1,doNothing,noEvent,wrapStyle)
);

//******************************************Shift Time Setting Save ********************************************************

//*****************************************
result Shift1Clockhr() {
  EEPROM.write(Eepromhur1, S1Hr);  

  return proceed;
}
result Shift1Clockmin() {
 
  EEPROM.write(Eeprommin1, S1Min);
  return proceed;
}
void EepromReadTime1()
{
   S1Hr = EEPROM.read(Eepromhur1);
   S1Min = EEPROM.read(Eeprommin1);
  return proceed;
}
//=================================================
//**********************SCHEDULER ALARM*******************
result Shift2Clock() {
  EEPROM.write(Eepromhur2, S2Hr);  
  EEPROM.write(Eeprommin2, S2Min);
  return proceed;
}

void EepromReadTime2()
{
  S2Hr = EEPROM.read(Eepromhur2);
  S2Min = EEPROM.read(Eeprommin2);
  return proceed;
}
//=================================================
//**********************SCHEDULER ALARM*******************
result Shift3Clock() {
  EEPROM.write(Eepromhur3, S3Hr);  
  EEPROM.write (Eeprommin3, S3Min);
  return proceed;
}
void EepromReadTime3()
{
  S3Hr = EEPROM.read(Eepromhur3);
  S3Min = EEPROM.read(Eeprommin3);
   return proceed;
}

//**********************SCHEDULER ALARM*******************
result Shift4Clock() {
  EEPROM.write(Eepromhur4, S4Hr);  
  EEPROM.write (Eeprommin4, S4Min);
  return proceed;
}
void EepromReadTime4()
{
  S4Hr = EEPROM.read(Eepromhur4);
  S4Min = EEPROM.read(Eeprommin4);
   return proceed;
}

//**********************SCHEDULER ALARM*******************
result Shift5Clock() {
  EEPROM.write(Eepromhur5, S5Hr);  
  EEPROM.write(Eeprommin5, S5Min);
  return proceed;
}
void EepromReadTime5()
{
  S5Hr = EEPROM.read(Eepromhur5);
  S5Min = EEPROM.read(Eeprommin5);
   return proceed;
}

void allReadTime(){
  EepromReadTime1();
  EepromReadTime2();
  EepromReadTime3();
  EepromReadTime4();
  EepromReadTime5();
 
  }

//******************************************Time Date Setting**************************************************************

altMENU(menu,timeMenu," Time Edit",doNothing,anyEvent,noStyle,(systemStyles)(_asPad|Menu::_menuData|Menu::_canNav|_parentDraw)
  ,FIELD(Hr,"",":",0,23,1,0,ClockSetting,anyEvent,noStyle)
  ,FIELD(Min,"","",0,59,10,1,ClockSetting,anyEvent,wrapStyle)
  ,FIELD(secs,"","",0,59,10,1,doNothing,noEvent,wrapStyle)
);


altMENU(menu,dateMenu," Date",doNothing,anyEvent,noStyle,(systemStyles)(_asPad|Menu::_menuData|Menu::_canNav|_parentDraw)
  ,FIELD(Date,"",":",0,31,1,0,DateSetting,anyEvent,noStyle)
  ,FIELD(Month,"","",0,12,10,1,DateSetting,anyEvent,wrapStyle)
  ,FIELD(Year,"","",0,2050,10,1,doNothing,anyEvent,wrapStyle)
);

//**********************Time Date*******************
result ClockSetting() {
  EEPROM.update(Eepromhur, Hr);  
  EEPROM.update(Eeprommin, Min);
  return proceed;
}

void EepromReadTime()
{
  Hr = EEPROM.read(Eepromhur);
  Min = EEPROM.read(Eeprommin);
}


result DateSetting() {
  EEPROM.update(EepromDate, Date);  
  EEPROM.write(EepromMonth, Month);
  EEPROM.write(EepromYear, Year);
  return proceed;
}

void EepromReadDate()
{
  Date = EEPROM.read(EepromDate);
  Month = EEPROM.read(EepromMonth);
  Year = EEPROM.read(EepromYear);
  
}

//=========================================================

//int Tomato [4]= {6.5, 1.80, 28, 100 }

CHOOSE(Crop,choosecrop,"Choose Crops:",doNothing,anyEvent,wrapStyle
  ,VALUE("Onion",1,info,enterEvent)
  ,VALUE("Basil Green",2,info,enterEvent)
  ,VALUE("Beetroot",3,info,enterEvent)
  ,VALUE("Cherry Tomato",-1,info,enterEvent)
);


MENU(S1St,"Slot 1 Time Edit",showEvent,enterEvent,noStyle
  
  ,SUBMENU(timeMenu0)
  ,OP("      CONFIRM"  ,EepromReadTime1,enterEvent)
  
  ,EXIT("<Back")
);

MENU(S2St,"Slot 2 Time Edit",showEvent,enterEvent,noStyle
  
  ,SUBMENU(timeMenu1)
  ,OP("       CONFIRM"  ,EepromReadTime2,enterEvent)
  
  ,EXIT("<Back")
);
MENU(S3St,"Slot 3 Time Edit",showEvent,enterEvent,noStyle
  
  ,SUBMENU(timeMenu2)
  ,OP("      CONFIRM"  ,EepromReadTime3,enterEvent)
  
  ,EXIT("<Back")
);

MENU(S4St,"Slot 4 Time Edit",showEvent,enterEvent,noStyle
  
  ,SUBMENU(timeMenu3)
  ,OP("       CONFIRM"  ,EepromReadTime4,enterEvent)
  
  ,EXIT("<Back")
);
MENU(S5St,"Slot 5 Time Edit",showEvent,enterEvent,noStyle
  
  ,SUBMENU(timeMenu4)
  ,OP("       CONFIRM"  ,EepromReadTime5,enterEvent)
  
  ,EXIT("<Back")
);
float pHs1;
float ECs1;
float temps1;
float dos1;

float pHs2;
float ECs2;
float temps2;
float dos2;

float pHs3;
float ECs3;
float temps3;
float dos3;

float pHs4;
float ECs4;
float dos4;

float pHs5;
float ECs5;
float dos5;
MENU(slot1,"Slot 1",showEvent,anyEvent,wrapStyle
    ,SUBMENU(S1St)
    ,FIELD(pHs1,"  pH :    "," ",0,14,1,0.20,saveslot1,enterEvent,wrapStyle)
    ,FIELD(ECs1,"  Ec :    "," ds/m",0,5,1,0.1,saveslot1,enterEvent,wrapStyle)
    ,FIELD(dos1,"  DO :    "," PPM",0,1000,10,100,doNothing,noEvent,wrapStyle)
    ,OP("         Confirm",S1Read,enterEvent)
  ,EXIT("<Back")
);

MENU(slot2,"Slot 2",showEvent,anyEvent,wrapStyle
    ,SUBMENU(S2St)
    ,FIELD(pHs2,"  pH  :    "," ",0,14,1,0.20,saveslot2,enterEvent,wrapStyle)
    ,FIELD(ECs2,"  Ec  :    "," ds/m",0,5,1,0.1,saveslot2,enterEvent,wrapStyle)
    ,FIELD(dos2,"  DO  :    "," PPM",0,1000,10,100,doNothing,noEvent,wrapStyle)
    ,OP("       Confirm",S2Read,enterEvent)
  ,EXIT("<Back")
);

MENU(slot3,"Slot 3",showEvent,anyEvent,wrapStyle
    ,SUBMENU(S3St)
    ,FIELD(pHs3,"  pH  :    "," ",0,14,1,0.20,saveslot3,enterEvent,wrapStyle)
    ,FIELD(ECs3,"  Ec  :    ","  ds/m",0,5,1,0.1,saveslot3,enterEvent,wrapStyle)
    ,FIELD(dos3,"  DO  :    "," PPM",0,1000,10,100,doNothing,noEvent,wrapStyle)
    ,OP("       Confirm",S3Read,enterEvent)
  ,EXIT("<Back")
);
MENU(slot4,"Slot 4",showEvent,anyEvent,wrapStyle
    ,SUBMENU(S4St)
    ,FIELD(pHs4,"  pH  :    "," ",0,14,0.20,1,saveslot4,enterEvent,wrapStyle)
    ,FIELD(ECs4,"  Ec  :    ","  ds/m",0,5,1,0.1,saveslot4,enterEvent,wrapStyle)
    ,FIELD(dos4,"  DO  :    "," PPM",0,1000,10,100,doNothing,noEvent,wrapStyle)
    ,OP("       Confirm",S4Read,enterEvent)
  ,EXIT("<Back")
);
MENU(slot5,"Slot 5",showEvent,anyEvent,wrapStyle
    ,SUBMENU(S5St)
    ,FIELD(pHs5,"  pH  :    "," ",0,14,0.20,1,savepHs5,enterEvent,wrapStyle)
    ,FIELD(ECs5,"  Ec  :    ","  ds/m",0,5,1,0.1,saveECs5,enterEvent,wrapStyle)
    ,FIELD(dos5,"  DO  :     "," PPM",0,1000,10,100,doNothing,noEvent,wrapStyle)
    ,OP("       Confirm",S5Read,enterEvent)
  ,EXIT("<Back")
);

//****Save Slot1*****
result saveslot1() {
  EEPROM.updateFloat(EeprompHs1, pHs1);  
  EEPROM.updateFloat(EepromECs1, ECs1);
  return proceed;
}

void S1Read()
{
  pHs1 = EEPROM.readFloat(EeprompHs1);
  ECs1 = EEPROM.readFloat(EepromECs1);
}
//****Save Slot2*****
result saveslot2() {
  EEPROM.updateFloat(EeprompHs2, pHs2);  
  EEPROM.updateFloat(EepromECs2, ECs2);
  return proceed;
}

void S2Read()
{
  pHs2 = EEPROM.readFloat(EeprompHs2);
  ECs2 = EEPROM.readFloat(EepromECs2);
}

//****Save Slot3*****
result saveslot3() {
  EEPROM.updateFloat(EeprompHs3, pHs3);  
  EEPROM.updateFloat(EepromECs3, ECs3);
  return proceed;
}

void S3Read()
{
  pHs3 = EEPROM.readFloat(EeprompHs3);
  ECs3 = EEPROM.readFloat(EepromECs3);
}

//****Save Slot4*****
result saveslot4() {
  EEPROM.updateFloat(EeprompHs4, pHs4);  
  EEPROM.updateFloat(EepromECs4, ECs4);
  return proceed;
}

void S4Read()
{
  pHs4 = EEPROM.readFloat(EeprompHs4);
  ECs4 = EEPROM.readFloat(EepromECs4);
}

//****Save Slot5*****
result savepHs5() {
  EEPROM.updateFloat(EeprompHs5, pHs5);  
  
  return proceed;
}

result saveECs5() {
 
  EEPROM.updateFloat(EepromECs5, ECs5);
  return proceed;
}

void S5Read()
{
  pHs5 = EEPROM.readFloat(EeprompHs5);
  ECs5 = EEPROM.readFloat(EepromECs5);
}

void Readall()
{
  pHs1 = EEPROM.readFloat(EeprompHs1);
  ECs1 = EEPROM.readFloat(EepromECs1);
  pHs2 = EEPROM.readFloat(EeprompHs2);
  ECs2 = EEPROM.readFloat(EepromECs2);
  pHs3 = EEPROM.readFloat(EeprompHs3);
  ECs3 = EEPROM.readFloat(EepromECs3);
  pHs4 = EEPROM.readFloat(EeprompHs4);
  ECs4 = EEPROM.readFloat(EepromECs4);
  pHs5 = EEPROM.readFloat(EeprompHs5);
  ECs5 = EEPROM.readFloat(EepromECs5);
}
//***********************************************************************************


int CSDate ;
int CSMonth  ;
int CSYear ;

//char CSMonth[7] = {"jan", "feb", "mar", "apr", "may", "jun", "jul"};

altMENU(menu,setdate," ",cropsetdate,enterEvent,wrapStyle,(systemStyles)(_asPad|Menu::_menuData|Menu::_canNav|_parentDraw)
  ,FIELD(CSDate," ","  :",0,31,1,0,cropsetdate,enterEvent,wrapStyle)
  ,FIELD(CSMonth," ","  :",1,12,10,1,cropsetmonth,enterEvent,wrapStyle)
  ,FIELD(CSYear,"  ","",19,23,1,1,cropsetyear,enterEvent,wrapStyle)
);

CHOOSE(CSMonth,choosemonth,"Month",cropsetmonth,enterEvent,wrapStyle
  ,VALUE("January",1,cropsetmonth,enterEvent)
  ,VALUE("February ",2,cropsetmonth,enterEvent)
  ,VALUE("March",3,cropsetmonth,enterEvent)
  ,VALUE("April ",4,cropsetmonth,enterEvent)
  ,VALUE("May",5,cropsetmonth,enterEvent)
  ,VALUE("June ",6,cropsetmonth,enterEvent)
  ,VALUE("July",7,cropsetmonth,enterEvent)
  ,VALUE("August ",8,cropsetmonth,enterEvent)
  ,VALUE("September",9,cropsetmonth,enterEvent)
  ,VALUE("October ",10,cropsetmonth,enterEvent)
  ,VALUE("November",11,cropsetmonth,enterEvent)
  ,VALUE("December ",12,cropsetmonth,enterEvent)
);

//January  31 days
//2 February  28 days (common year)
//29 days (leap year*)
//3 March 31 days
//4 April 30 days
//5 May 31 days
//6 June  30 days
//7 July  31 days
//8 August  31 days
//9 September 30 days
//10  October 31 days
//11  November  30 days
//12  December


CHOOSE(CSYear,chooseyear,"Year",cropsetyear,enterEvent,wrapStyle
  ,VALUE("2019",19,cropsetyear,enterEvent)
  ,VALUE("2020",20,cropsetyear,enterEvent)
  ,VALUE("2021 ",21,cropsetyear,enterEvent)
  ,VALUE("2022",22,cropsetyear,enterEvent)
  ,VALUE("2023",23,cropsetyear,enterEvent)
  
);

int EepromCSD = 100;
int EepromCSM = 110;
int EepromCSY = 120;


result cropsetdate() {
  EEPROM.write(EepromCSD, CSDate);
  EEPROM.write(EepromCSM, CSMonth);  
  EEPROM.write(EepromCSY, CSYear);  
  return proceed;
}

result cropsetmonth() {
  EEPROM.write(EepromCSM, CSMonth);  
  
  return proceed;
}
result cropsetyear() {
  EEPROM.write(EepromCSY, CSYear);  
  
  return proceed;
}


result cropsetdateR() {
  
  CSDate = EEPROM.read(EepromCSD);
  CSMonth = EEPROM.read(EepromCSM);
  CSYear = EEPROM.read(EepromCSY);
 
  return proceed;
}
//MENU(selectdate,"Crop start date",cropsetdateR,enterEvent,noStyle
//   ,SUBMENU(setdate)
//
//   ,OP("           SET",cropsetdate,enterEvent)
////   ,OP("          READ",cropsetdateR,enterEvent)
//
//   ,FIELD(CSDate,"Date","  ",1,31,1,1,cropsetdate,enterEvent,wrapStyle)
//   ,SUBMENU(choosemonth)
//   ,SUBMENU(chooseyear)
//   //,FIELD(CSMonth,"Month","  ",1,12,1,1,cropsetmonth,enterEvent,wrapStyle)
//   //,FIELD(CSYear,"Year","  ",19,20,10,1,cropsetyear,enterEvent,wrapStyle)
//   
//   
//  ,EXIT("<Back")
//);
//
//MENU(WeekList,"Weekly",showEvent,anyEvent,noStyle
//   ,SUBMENU(selectdate)
//  ,SUBMENU(FirstWeek)
//  ,SUBMENU(SecWeek)
//  ,SUBMENU(ThiredWeek)
//  ,SUBMENU(FourthWeek)
//  ,SUBMENU(FivithWeek)
//  ,EXIT("<Back")
//);
//MENU(MonthList,"  Monthly",showEvent,anyEvent,noStyle
//  ,OP("Coming soon......."  ,showEvent,anyEvent)
//  ,EXIT("<Back")
//);



MENU(Shift," Roster",showEvent,enterEvent,wrapStyle
  ,SUBMENU(S1St)
  ,SUBMENU(S2St)
  ,SUBMENU(S3St)
  ,SUBMENU(S4St)
  ,SUBMENU(S5St)
  ,EXIT("<Back")
);

MENU(schedule," Schedule",showEvent,anyEvent,wrapStyle
    ,SUBMENU(slot1)
    ,SUBMENU(slot2)
    ,SUBMENU(slot3)
    ,SUBMENU(slot4)
    ,SUBMENU(slot5)
   ,SUBMENU(AutoSchedule)
//  ,SUBMENU(MonthList)
  
  ,EXIT("<Back")
);


int FDate;
int FMonth;
int FYear;
int TDate;
int TMonth;
int TYear;
altMENU(menu,From," From Date",doNothing,anyEvent,noStyle,(systemStyles)(_asPad|Menu::_menuData|Menu::_canNav|_parentDraw)
  ,FIELD(FDate,"","/",0,31,1,0,SPFD,anyEvent,noStyle)
  ,FIELD(FMonth,"","/",0,12,10,1,SPFM,anyEvent,wrapStyle)
  ,FIELD(FYear,"","",2020,2022,0,1,doNothing,anyEvent,wrapStyle)
);
altMENU(menu,To," To Date  ",doNothing,anyEvent,noStyle,(systemStyles)(_asPad|Menu::_menuData|Menu::_canNav|_parentDraw)
  ,FIELD(TDate,"","/",0,31,1,0,SPTD,anyEvent,noStyle)
  ,FIELD(TMonth,"","/",0,12,10,1,SPTM,anyEvent,wrapStyle)
  ,FIELD(TYear,"","",2020,2022,0,1,doNothing,anyEvent,wrapStyle)
);

int slotslect=0;
SELECT(slotslect,slots," Select Slot :",doNothing,noEvent,noStyle
 ,VALUE("All Slot",0,doNothing,enterEvent)
 ,VALUE("Slot 1",1,doNothing,enterEvent)
  ,VALUE("Slot 2 ",2,doNothing,enterEvent)
  ,VALUE("Slot 3",3,doNothing,enterEvent)
  
);

//***************Eprom Save SP****************//

int EepromSPFD = 260;
int EepromSPFM =265;
int EepromSPTD =270;
int EepromSPTM =275;

result SPFD() {
  EEPROM.write(EepromSPFD, FDate);  
  
  return proceed;
}
result SPFM() {
  EEPROM.write(EepromSPFM, FMonth);  
  
  return proceed;
}
result SPTD() {
  EEPROM.write(EepromSPTD, TDate);  
  
  return proceed;
}
result SPTM() {
  EEPROM.write(EepromSPTM, Month);  
  
  return proceed;
}


float pHsp;
float ECsp;
float tempsp;
float dosp;

MENU(SetPoint," Automatic",showEvent,anyEvent,noStyle  
  
    ,FIELD(pHsp,"  pH  :    "," ",0,14,0.20,1,SPph,enterEvent,wrapStyle)
    ,FIELD(ECsp,"  Ec  :    "," ds/m",0,3,1,0.5,SPec,enterEvent,wrapStyle)
    //,FIELD(tempsp,"  Temp:    "," 'C",0,100,1,5,SPtemp,noEvent,wrapStyle)
    ,FIELD(dosp,"  DO  :    "," PPM",0,1000,10,100,SPdosp,enterEvent,wrapStyle)
    ,OP("       Confirm",SPFTRead,enterEvent)
    ,SUBMENU(AutoDose)
  ,EXIT("<Back")
);

MENU(AutoDoser," DOSING MODE",showEvent,anyEvent,noStyle
  ,SUBMENU(SetPoint)  //Automatic
  ,SUBMENU(schedule) //schedule
  ,SUBMENU(MDoseMenu) //Manual Dose
  ,EXIT("<Back")
);
//***************Eprom Save SP****************//

int EeprompHsp =280;
int EepromECsp =285;
int Eepromtempsp =290;
int Eepromdosp = 295;

result SPph() {
  EEPROM.writeFloat(EeprompHsp, pHsp);  
  
  return proceed;
}
result SPec() {
  EEPROM.writeFloat(EepromECsp, ECsp);  
  
  return proceed;
}
result SPtemp() {
  EEPROM.writeFloat(Eepromtempsp, tempsp);  
  
  return proceed;
}
result SPdosp() {
  EEPROM.writeFloat(Eepromdosp, dosp);  
  
  return proceed;
}


result SPFTRead() {
  FDate = EEPROM.read(EepromSPFD);
  TDate = EEPROM.read(EepromSPFM);
  FMonth = EEPROM.read(EepromSPTD);
  TMonth = EEPROM.read(EepromSPTM);
  pHsp = EEPROM.readFloat(EeprompHsp);
  ECsp = EEPROM.readFloat(EepromECsp);
  tempsp = EEPROM.readFloat(Eepromtempsp);
  dosp = EEPROM.readFloat(Eepromdosp);
  return proceed;
}

MENU(timedateset," Time & Date",showEvent,anyEvent,noStyle

  ,SUBMENU(timeMenu)
  ,SUBMENU(dateMenu)
  ,EXIT("<Back")
);


int ECUnite=0;
CHOOSE(ECUnite,ecsetunite," EC :",doNothing,noEvent,noStyle
 ,VALUE("TDS ppm (mS/cm)",0,doNothing,enterEvent)
  ,VALUE("EC (d/ms)",1,doNothing,enterEvent)
  ,VALUE("CF (μS/cm)",2,doNothing,enterEvent)
 
  
);
int tempUnite=0;
CHOOSE(tempUnite,tempsetunite," Temp :",doNothing,noEvent,noStyle
 ,VALUE("Centigrade",0,doNothing,enterEvent)
  ,VALUE("Farenheit",1,doNothing,enterEvent)

);


MENU(Units," Unit",showEvent,anyEvent,noStyle
  ,SUBMENU(ecsetunite)
  ,SUBMENU(tempsetunite)
  
  ,EXIT("<Back")
);

MENU(SDCard," SD Card",showEvent,enterEvent,noStyle
  
  ,SUBMENU(Units)
  ,EXIT("<Back")
);

MENU(settings," SETTINGS",showEvent,anyEvent,noStyle
  ,SUBMENU(Units)
  ,SUBMENU(timedateset)
  ,SUBMENU(Shift)
  ,SUBMENU(SDCard)
  
  ,EXIT("<Back")
);



MENU(mainMenu,"    DOSING CONTROLLER",doNothing,noEvent,wrapStyle
  ,OP(" MONITOR",goHome,enterEvent)
  //,OP("Op1",action1,anyEvent)
  //,OP("",action2,anyEvent) //enterEvent
  //,OP("Op2",action2,enterEvent)
  ,SUBMENU(AutoDoser)
  ,SUBMENU(CalMenu)
  
  ,SUBMENU(WLmenu)
  ,SUBMENU(settings)
  
  ,OP(" INFO",Cinfo,enterEvent)

//  ,EXIT("<Back")
);


//encoderIn<encA,encB> encoder;//simple quad encoder driver
//encoderInStream<encA,encB> encStream(encoder,4);// simple quad encoder fake Stream

//a keyboard with only one key as the encoder button
//keyMap encBtn_map[]={{-encBtn,defaultNavCodes[enterCmd].ch}};//negative pin numbers use internal pull-up, on = low
//keyIn<1> encButton(encBtn_map);//1 is the number of keys

serialIn serial(Serial);

////input from the encoder + encoder button + serial
//menuIn* inputsList[]={&encStream,&encButton,&serial};
//chainStream<3> in(inputsList);//3 is the number of inputs

//fontY should now account for fontMarginY
#define fontX 3
#define fontY 10
#define MAX_DEPTH 10

//this macro replaces all the above commented lines
MENU_OUTPUTS(out,MAX_DEPTH
 // ,U8GLIB_OUT(u8g2,colors,fontX,fontY,{0,0,128/fontX,64/fontY})
  ,U8G2_OUT(u8g2,colors,fontX,fontY,offsetX,offsetY,{0,0,U8_Width/fontX,U8_Height/fontY})
  ,SERIAL_OUT(Serial)
  ,NONE
);

NAVROOT(nav,mainMenu,MAX_DEPTH,in,out);

result alert(menuOut& o,idleEvent e) {
  if (e==idling) {
    o.setCursor(0,0);
    o.print("alert test");
    o.setCursor(0,1);
    o.print("press [select]");
    o.setCursor(0,2);
    o.print("to continue...");
  }
  return proceed;
}
void updateScreen(){
  DateTime now = rtc.now();
  //getNutrientPH();
  //EepromReadTime();
  //EepromReadDate();
  
  {
      u8g2.drawFrame(0,0,128,64);
      u8g2.drawFrame(0,0,128,12);
      u8g2.drawFrame(0,0,128,53);
      u8g2.drawFrame(0,0,128,43);
      u8g2.drawFrame(0,0,128,33);
      u8g2.drawFrame(53,42,128,11);
      u8g2.drawFrame(64,11,128,32);
      
      u8g2.drawStr(16, 10, "  DOSING CONTROLLER");
      //u8g2.drawStr( 2+x, 10, "DOSING CONTROLLER");
      u8g2.drawStr(5, 21, "pH 1:");
      u8g2.setCursor(32,21);
      u8g2.print(float( nutrientPH));
      
      u8g2.drawStr(5, 30, "pH 2:");
      u8g2.setCursor(32,30);
      u8g2.print("  --");
     
      u8g2.drawStr(67, 21, "EC 1:");
      u8g2.setCursor(95,21);
      //u8g2.print(float( tdsValue));
      u8g2.print(float( nutrientEC));
//      u8g2.drawStr(67, 30, "TDS :");
//      u8g2.setCursor(95,30);
//      u8g2.print(float( tdsValue));

      u8g2.setCursor(67, 30);
      switch (ECUnite){
        case 0:{
          u8g2.print("TDS :");
        
          break;
        }
        case 1:{
          u8g2.print("EC  :");
          break; }
        case 2:{
          u8g2.print("CF  :");
          break; }
         default :
         break;
          }
        u8g2.setCursor(95,30);
      switch (ECUnite){
        case 0:{
          u8g2.print(float( tdsValue));
          break;
        }
        case 1:{
          u8g2.print(float( nutrientEC));
          break; }
        case 2:{
          u8g2.print(float( nutrientCF));
          break; }

         default :
         break;
          }
      
      u8g2.setFont(u8g2_font_5x7_tr);
      //u8g2.drawStr(110, 26, "ds.m");
//      u8g2.drawRFrame(81,15,30,15,2);
//      u8g2.drawRFrame(50,42,30,15,2);
      
      u8g2.drawStr(5, 41, "TEMP:");
      u8g2.setCursor(32,41);
      //u8g2.print( nutrientTemperature);
      switch (tempUnite){
        case 0:{
          u8g2.print(float( nutrientTemperature));
          u8g2.print("°C");
          break;
        }
        case 1:{
          u8g2.print(float( Fahrenheit));
          u8g2.print(" F");
          break; }

         default :
         break;
          }
          
      u8g2.drawStr(67, 41, "DO  :");
      u8g2.setCursor(93,41);
      u8g2.print( " --");

      u8g2.drawStr(5, 62, "Status :");
      u8g2.drawStr(5, 62, "Status :");
      
       u8g2.drawStr(105, 62, "MENU");
       
//      u8g2.setCursor(92,40);
//      u8g2.print( " --");

      u8g2.drawStr(2, 51, "Time ");
      u8g2.setCursor(26,51);
      u8g2.print( now.hour());
      u8g2.drawStr(36, 51, ":");
      u8g2.setCursor(41,51);
      u8g2.print( now.minute());
      
      u8g2.drawStr(56, 51, "Date ");
      //u8g2.drawStr(58, 52, "Date:");
      u8g2.setCursor(80,51);
      u8g2.print( now.day());
      //u8g2.print( ndaysOfTheWeek[now.dayOfTheWeek()]);
      u8g2.setCursor(92,51);
      u8g2.print( now.month());
      u8g2.setCursor(105,51);
      u8g2.print( now.year());
//    u8g2.drawFrame(0,0,128,64);
//    u8g2.drawFrame(0,0,128,12);
//    u8g2.setFont(fontName);

    //u8g2.setFont(u8g2_font_5x7_tr);
  }
}
result home(menuOut& o,idleEvent e) {
  {
  //u8g2.setFont(fontName);
  updateScreen();
    
  }
  return proceed;
}
result goHome(eventMask e, prompt &item) {
   nav.idleOn(home);
  return proceed;
}
result doAlert(eventMask e, prompt &item) {
  nav.idleOn(alert);
  return proceed;
}

result info(eventMask e, prompt &item) {
  nav.idleOn(onioninfo);
  return proceed;
}
result Cinfo(eventMask e, prompt &item) {
  nav.idleOn(controllerinfo);
  return proceed;
}
result controllerinfo(eventMask e,navNode& nav, prompt &item) {
  DateTime now = rtc.now();
  {
    u8g2.drawFrame(0,0,128,64);
    u8g2.drawFrame(5,5,118,54);
    u8g2.drawStr(9, 15, "    Name: ZG02OTB0");
   // u8g2.drawStr(40, 25, (Crop));
    u8g2.drawStr(25, 35, "    YoM:2020");
    u8g2.drawStr(20, 48, "Product By Cenaura");
  u8g2.setCursor(40,51);
  
  }
  return proceed;
}

result onioninfo(eventMask e,navNode& nav, prompt &item) {

  {
   
    u8g2.drawStr(45,51, "none");
  }
  return proceed;
}

///***************Read all data from memoery******************************
void readalldata(){
  EepromRead();
  EepromReadEC();
  phset();
  phsetHys();
  SPFTRead();
  cropsetdateR();
  EepromReadNT();
  allReadTime();
  EepromReadTime();
  EepromReadDate();
  Readall();
  }
//when menu is suspended
result idle(menuOut& o,idleEvent e) {
  o.clear();
  switch(e) {
    case idleStart:o.println("suspending menu!");break;
    case idling:o.println("suspended...");break;
    case idleEnd:o.println("resuming menu.");break;
  }
  return proceed;
}

void setup() {
  pinMode(LEDPIN,OUTPUT);
  Serial.begin(115200);
  while(!Serial);
  Serial.println("Cenaura...........");
  Serial.flush();
  
  rtc.begin();
  
  nav.idleTask=home;//point a function to be used when menu is suspended
  //mainMenu[0].enabled=disabledStatus;
  
   
   WLmenu[0].enabled=disabledStatus;
   WLmenu[1].enabled=disabledStatus;
   WLmenu[2].enabled=disabledStatus;
  //change input burst for slow output devices
  //this is the number of max. processed inputs before drawing
  //nav.inputBurst=10;
  
  //BOOT
  nav.showTitle=true;
  
  u8g2.drawStr(0, 0, "WELCOME TO CENAURA");
  
  u8g2.drawStr(10, 10, "BOOTING.....");
  Timer1.initialize(1000);
  Timer1.attachInterrupt(timerIsr);
  SPI.begin();
  u8g2.begin();
  //  noScreenSetup();
  
  readalldata();

  

  pinMode(encBtn, INPUT_PULLUP);
//  encButton.begin();
//  encoder.begin();
//  pinMode(TDS_PIN,INPUT);
   // Start up the libraries
  sensors.begin(); // DALLAS

  pinMode(SD_DETECT_PIN, OUTPUT);
  // SD Card Initialization
  if (SD.begin(SDSS))
//  filePickMenu.begin();//need this after sd begin
//  Serial.println("initialization done.");
//  nav.useAccel=false;
  
  
  
  Serial.print(F("  Date  "));   
  Serial.print(F("      "));
  Serial.print(F("   Time  "));
  Serial.print("     ");
  Serial.print(F("   Temperature  "));
  Serial.print("     ");
  Serial.print(F("   PH  "));
  Serial.print("     ");
  Serial.print(F("   EC  "));
  Serial.print("     ");
  sdcard_file = SD.open("data.txt", FILE_WRITE);
  if (sdcard_file) { 
    sdcard_file.print(F("Date  "));   
    sdcard_file.print("      ");
    sdcard_file.print("   Time  ");
    sdcard_file.print("     ");
    sdcard_file.print("   Temperature   ");
    sdcard_file.print("     ");
    sdcard_file.print("   PH   ");
    sdcard_file.print("     ");
    sdcard_file.print("   EC   ");
    sdcard_file.print("     ");
    sdcard_file.close(); // close the file
  }
  // if the file didn't open, print an error:
  else {
    Serial.println("error opening test.txt");
  }
  
  // Stepper configer
  pinMode(PHLOW_STEP_PIN  , OUTPUT);
  pinMode(PHLOW_DIR_PIN    , OUTPUT);
  pinMode(PHLOW_ENABLE_PIN    , OUTPUT);
  
  pinMode(PHHIGH_STEP_PIN  , OUTPUT);
  pinMode(PHHIGH_DIR_PIN    , OUTPUT);
  pinMode(PHHIGH_ENABLE_PIN    , OUTPUT);
  
  digitalWrite(PHLOW_ENABLE_PIN    , LOW);
  digitalWrite(PHHIGH_ENABLE_PIN    , LOW);

  pinMode(EC_A_STEP_PIN, OUTPUT);
  pinMode(EC_B_STEP_PIN, OUTPUT);
  pinMode(EC_C_STEP_PIN, OUTPUT);

  pinMode(EC_A_DIR_PIN, OUTPUT);
  pinMode(EC_B_DIR_PIN, OUTPUT);
  pinMode(EC_C_DIR_PIN, OUTPUT);

  pinMode(EC_A_ENABLE_PIN, OUTPUT);
  pinMode(EC_B_ENABLE_PIN, OUTPUT);
  pinMode(EC_C_ENABLE_PIN, OUTPUT);

  digitalWrite(EC_A_DIR_PIN, LOW);
  digitalWrite(EC_B_DIR_PIN, LOW);
  digitalWrite(EC_C_DIR_PIN, LOW);

  digitalWrite(EC_A_ENABLE_PIN, LOW);
  digitalWrite(EC_B_ENABLE_PIN, LOW);
  digitalWrite(EC_C_ENABLE_PIN, LOW);
  
  //***WaterLevel****
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  pinMode(RORelay, OUTPUT);
  pinMode(BUZZER, OUTPUT);

      //Update Title position
    x=x+scroll_direction;
    if (x > 40) scroll_direction = -1;
    if (x < 1) scroll_direction = 1;
  //u8g.setFont(u8g_font_helvR08);
  u8g2.setFont(fontName);
  //u8g.setFont(u8g_font_04b_03r);
  u8g2.firstPage();
  do {
//    u8g2.setColorIndex(1);
//    nav.out[0].setCursor(0,0);
//    u8g2.drawStr( 2+x, 10, "RRD GLCD TEST");
    nav.out[0].print(F("Welcome To Cenaura"));
    nav.out[0].setCursor(0,3);
    nav.out[0].print(F("Booting..."));

  } while(u8g2.nextPage());
  delay(1000);
}

bool AutoSwitchFun;

void loop() {
//  testfun();
  DateTime now = rtc.now();
  rtc.begin();
//  sprintf(t, "%02d:%02d:%02d %02d/%02d/%02d",  now.hour(), now.minute(), now.second(), now.day(), now.month(), now.year());
//  Serial.print(F("Date/Time: "));
//  Serial.println(t);
  ///Switching funtion
   if (autostart == true) {

    if(SPstart=false){
    Serial.println("ph autostart...........");  
    float pHAutoSP =  pHsp;
    float pHHys =  phSetHysteris;
     Serial.println(pHAutoSP);  
     Serial.println(pHHys);
    pHHysteris(pHAutoSP, pHHys);
    }
    if(AutoSwitchFun=false){
      Serial.println("Ec autostart...........");  
    float ECAutoSP =  ECsp;
    float ECHys =  ECSetHysteris;
     Serial.println(ECAutoSP);  
     Serial.println(ECHys);
     ECHysteris(ECAutoSP, ECHys ); 
      }
//    float ECAutoSP =  ECsp;
//    ECHysteris( ECAutoSP);
  }
  //PH Calibration
  if (SPstart == true) {
    Serial.println("PH calibration start...........");
       float pHcalibration =  pHSetpoint;
       float pHHys =  phSetHysteris;
//        Serial.println(pHcalibration);  
//        Serial.println(pHHys);
       pHHysteris(pHcalibration , pHHys );
//   getshift();
  }
  if (EcCalStart == true) {
    Serial.println("EC calibration start...........");
    float ECcalibration =  ECsetvalue;
    float ECHys =  ECSetHysteris;
       ECHysteris(ECcalibration, ECHys ); 
  }

  if (TankM == true) {
    getNutrientTL();
  }
 if (ScheduleAuto == true) {
   Serial.print(F("  Schedule Auto ON  "));  
    scheduleFun();
    bool Resetfun = false;
  }

  //scheduleFun();
   //Update Title position
    x=x+scroll_direction;
    if (x > 40) scroll_direction = -1;
    if (x < 1) scroll_direction = 1;   
  //dateandtime();
  //getNutrientTL();
   

  //getSDdata();
  //Sensor Reading
  {
  getNutrientPH();
  getNutrientTDS();
  getNutrientEC();
  getNutrientCF();
  getNutrientTemperature();
  }
  
  digitalWrite(LEDPIN, ledCtrl);
  digitalWrite (BUZZER, ledCtrl);


  
  nav.doInput();
  //nav.poll();
  if (nav.changed(1)) {//only draw if menu changed for gfx device
    //because this code clears the screen, if always called then screen will blink
    u8g2.firstPage();
    do nav.doOutput(); while(u8g2.nextPage());
  }
  delay(500);//simulate other tasks delay
}

void getSDdata() {
   getNutrientTemperature();
  
  DateTime now = rtc.now();
  rtc.begin();
  sprintf(t, "%02d:%02d:%02d %02d/%02d/%02d",  now.hour(), now.minute(), now.second(), now.day(), now.month(), now.year());
  //Serial.print(F("Date/Time: "));
  Serial.println("     ");
  Serial.print(t);
  
//  Serial.println("     ");
//  Serial.print(now.day());
//  Serial.print("/");
//  Serial.print(now.month());
//  Serial.print("/");
//  Serial.print(now.year());
//  Serial.print("     ");
//  Serial.print(now.hour());
//  Serial.print(":");
//  Serial.print(now.minute());
//  Serial.print(":");
//  Serial.print(now.second());
  Serial.print("      ");
  Serial.print(nutrientTemperature);
  Serial.print("      ");
  Serial.print(nutrientPH);
  Serial.print("      ");
  Serial.print(tdsValue);
 
  sdcard_file = SD.open("data.txt", FILE_WRITE);
  if (sdcard_file) {  
    sdcard_file.print("      ");  
    sdcard_file.print(t);
    sdcard_file.print("      ");
    sdcard_file.print(nutrientTemperature);
    sdcard_file.print("      ");
    sdcard_file.println(nutrientPH);
    sdcard_file.println("      ");
    sdcard_file.println(tdsValue);
    
    sdcard_file.close(); // close the file
    
  }
  // if the file didn't open, print an error:
  else {
    Serial.println(F("error opening test.txt"));
  }
  delay(3000);
}

void datajsonsetup() {
  
  
  // put your setup code here, to run once:
const size_t capacity = 9*JSON_ARRAY_SIZE(4) + 3*JSON_OBJECT_SIZE(2) + 4*JSON_OBJECT_SIZE(3) + 60;
DynamicJsonDocument doc(capacity);

const char* json = "{\"tomato\":{\"time\":1351824120,\"day1\":{\"slot1\":[28.5,6.5,750,300],\"slot2\":[25,6.5,750,300],\"slot3\":[27.5,6.5,750,300]}},\"onion\":{\"time\":1351824120,\"day1\":{\"slot1\":[27.5,6.2,650,750],\"slot2\":[26,6.3,645,850],\"slot3\":[28.5,6.4,680,650]}},\"lettuce\":{\"time\":1351824120,\"day1\":{\"slot1\":[28.5,6.5,750,300],\"slot2\":[25,6.5,750,300],\"slot3\":[27.5,6.5,750,300]}}}";

deserializeJson(doc, json);

long tomato_time = doc["tomato"]["time"]; // 1351824120

JsonObject tomato_day1 = doc["tomato"]["day1"];

JsonArray tomato_day1_slot1 = tomato_day1["slot1"];
float tomato_day1_slot1_0 = tomato_day1_slot1[0]; // 28.5
float tomato_day1_slot1_1 = tomato_day1_slot1[1]; // 6.5
int tomato_day1_slot1_2 = tomato_day1_slot1[2]; // 750
int tomato_day1_slot1_3 = tomato_day1_slot1[3]; // 300

JsonArray tomato_day1_slot2 = tomato_day1["slot2"];
int tomato_day1_slot2_0 = tomato_day1_slot2[0]; // 25
float tomato_day1_slot2_1 = tomato_day1_slot2[1]; // 6.5
int tomato_day1_slot2_2 = tomato_day1_slot2[2]; // 750
int tomato_day1_slot2_3 = tomato_day1_slot2[3]; // 300

JsonArray tomato_day1_slot3 = tomato_day1["slot3"];
float tomato_day1_slot3_0 = tomato_day1_slot3[0]; // 27.5
float tomato_day1_slot3_1 = tomato_day1_slot3[1]; // 6.5
int tomato_day1_slot3_2 = tomato_day1_slot3[2]; // 750
int tomato_day1_slot3_3 = tomato_day1_slot3[3]; // 300

long onion_time = doc["onion"]["time"]; // 1351824120

JsonObject onion_day1 = doc["onion"]["day1"];

JsonArray onion_day1_slot1 = onion_day1["slot1"];
float onion_day1_slot1_0 = onion_day1_slot1[0]; // 27.5
float onion_day1_slot1_1 = onion_day1_slot1[1]; // 6.2
int onion_day1_slot1_2 = onion_day1_slot1[2]; // 650
int onion_day1_slot1_3 = onion_day1_slot1[3]; // 750

JsonArray onion_day1_slot2 = onion_day1["slot2"];
int onion_day1_slot2_0 = onion_day1_slot2[0]; // 26
float onion_day1_slot2_1 = onion_day1_slot2[1]; // 6.3
int onion_day1_slot2_2 = onion_day1_slot2[2]; // 645
int onion_day1_slot2_3 = onion_day1_slot2[3]; // 850

JsonArray onion_day1_slot3 = onion_day1["slot3"];
float onion_day1_slot3_0 = onion_day1_slot3[0]; // 28.5
float onion_day1_slot3_1 = onion_day1_slot3[1]; // 6.4
int onion_day1_slot3_2 = onion_day1_slot3[2]; // 680
int onion_day1_slot3_3 = onion_day1_slot3[3]; // 650

long lettuce_time = doc["lettuce"]["time"]; // 1351824120

JsonObject lettuce_day1 = doc["lettuce"]["day1"];

JsonArray lettuce_day1_slot1 = lettuce_day1["slot1"];
float lettuce_day1_slot1_0 = lettuce_day1_slot1[0]; // 28.5
float lettuce_day1_slot1_1 = lettuce_day1_slot1[1]; // 6.5
int lettuce_day1_slot1_2 = lettuce_day1_slot1[2]; // 750
int lettuce_day1_slot1_3 = lettuce_day1_slot1[3]; // 300

JsonArray lettuce_day1_slot2 = lettuce_day1["slot2"];
int lettuce_day1_slot2_0 = lettuce_day1_slot2[0]; // 25
float lettuce_day1_slot2_1 = lettuce_day1_slot2[1]; // 6.5
int lettuce_day1_slot2_2 = lettuce_day1_slot2[2]; // 750
int lettuce_day1_slot2_3 = lettuce_day1_slot2[3]; // 300

JsonArray lettuce_day1_slot3 = lettuce_day1["slot3"];
float lettuce_day1_slot3_0 = lettuce_day1_slot3[0]; // 27.5
float lettuce_day1_slot3_1 = lettuce_day1_slot3[1]; // 6.5
int lettuce_day1_slot3_2 = lettuce_day1_slot3[2]; // 750
int lettuce_day1_slot3_3 = lettuce_day1_slot3[3]; // 300


//Serial.print (lettuce_day1_slot3_3);

serializeJsonPretty(onion_day1, Serial);
}

// Read water temperature 
float getNutrientTemperature(){
  // Read DALLAS
  // call sensors.requestTemperatures() to issue a global temperature
  // request to all devices on the bus
  sensors.requestTemperatures(); // Send the command to get temperatures
  float ntValue = (sensors.getTempCByIndex(0)); // Why "byIndex"?
  // You can have more than one IC on the same bus.
  // 0 refers to the first IC on the wire
  ntValue = roundDecimalPoint(ntValue,1);
  Fahrenheit=sensors.toFahrenheit(ntValue);

  nutrientTemperature = ntValue;
//  Serial.print(nutrientTemperature, 1);
//    Serial.println("C");
//    Serial.print(Fahrenheit, 1);
//    Serial.println("F");
  return ntValue;
}

//Read ph value from water
float getNutrientPH(){
  for(int i=0;i<10;i++)                            //Get 10 sample value from the sensor for smooth the value
  { 
    buf[i]=analogRead(PH_PIN);
    delay(10);
  }
  for(int i=0;i<9;i++)                           //sort the analog from small to large
  {
    for(int j=i+1;j<10;j++)
    {
      if(buf[i]>buf[j])
      {
        temp=buf[i];
        buf[i]=buf[j];
        buf[j]=temp;
      }
    }
  }
  avgValue=0;
  for(int i=2;i<8;i++)                           //take the average value of 6 center sample
    avgValue+=buf[i];
  float phValue=(float)avgValue*5.0/1024/6;      //convert the analog into millivolt
  phValue=3.5*phValue;                           //convert the millivolt into pH value
  phValue = roundDecimalPoint(phValue,2);
//  Serial.print("    pH:");  
//  Serial.println(phValue,2);
  nutrientPH = phValue;
  return phValue;
}

//Calculate TDS value
//float getNutrientTDS(){
//    //    The ppm 700 scale is based on measuring the KCl or potassium chloride content of a solution. 
//    //    The PPM 500 is based on measuring the NaCl or sodium chloride content of a solution and is also referred 
//    //    to as TDS - total dissolved solids. Individual nutrient ions have different electrical effects! 
//    //    The true ppm of a solution can only be determined by a chemical analysis. PPMcannot be accurately measured by a CF or EC meter.
//    //    PPM500 is the used in US and is also referred as TDS. Since we use gravity sensor, we use this to calculate EC, CF, PPM640(EU) and PPM700(Australia)
//    gravityTds.setTemperature(nutrientTemperature);  // set the temperature to obtain temperature compensated TDS
//    gravityTds.update();  //sample and calculate
//    int tdsValue = gravityTds.getTdsValue();  // then get the value
//    nutrientTDS = tdsValue;
//    Serial.print(nutrientTDS,0);
//    Serial.println("ppm");
//    return tdsValue;
//}

float getNutrientTDS(){
    //    The ppm 700 scale is based on measuring the KCl or potassium chloride content of a solution. 
    //    The PPM 500 is based on measuring the NaCl or sodium chloride content of a solution and is also referred 
    //    to as TDS - total dissolved solids. Individual nutrient ions have different electrical effects! 
    //    The true ppm of a solution can only be determined by a chemical analysis. PPMcannot be accurately measured by a CF or EC meter.
    //    PPM500 is the used in US and is also referred as TDS. Since we use gravity sensor, we use this to calculate EC, CF, PPM640(EU) and PPM700(Australia)
    static unsigned long analogSampleTimepoint = millis();
 if(millis()-analogSampleTimepoint > 40U) //every 40 milliseconds,read the analog value from the ADC
 {
 analogSampleTimepoint = millis();
 analogBuffer[analogBufferIndex] = analogRead(TdsSensorPin); //readthe analog value and store into the buffer
 analogBufferIndex++;
 if(analogBufferIndex == SCOUNT)
 analogBufferIndex = 0;
 }
 static unsigned long printTimepoint = millis();
 if(millis()-printTimepoint > 800U)
 {
 printTimepoint = millis();
 for(copyIndex=0;copyIndex<SCOUNT;copyIndex++)
 analogBufferTemp[copyIndex]= analogBuffer[copyIndex];
 averageVoltage = getMedianNum(analogBufferTemp,SCOUNT) * (float)VREF
/ 1024.0; // read the analog value more stable by the median filtering algorithm, and convert to voltage value
 float compensationCoefficient=1.0+0.02*(temperature-25.0); //temperature compensation formula: fFinalResult(25^C) = fFinalResult(current)/(1.0+0.02*(fTP-25.0));
 float compensationVolatge=averageVoltage/compensationCoefficient; //temperature compensation
 tdsValue=(133.42*compensationVolatge*compensationVolatge*compensationVolatge - 255.86*compensationVolatge*compensationVolatge + 857.39*compensationVolatge)*0.5; //convert voltage value to tds value
  nutrientTDS = tdsValue;

 //Serial.print("voltage:");
// Serial.print(averageVoltage,2);
 //Serial.print("V ");
// Serial.print("TDS Value:");
// Serial.print(tdsValue,0);
// Serial.println("ppm");
 }
 return tdsValue; 
}

int getMedianNum(int bArray[], int iFilterLen)
{
 int bTab[iFilterLen];
 for (byte i = 0; i<iFilterLen; i++)
 bTab[i] = bArray[i];
 int i, j, bTemp;
 for (j = 0; j < iFilterLen - 1; j++)
 {
 for (i = 0; i < iFilterLen - j - 1; i++)
 {
 if (bTab[i] > bTab[i + 1])
 {
 bTemp = bTab[i];
 bTab[i] = bTab[i + 1];
 bTab[i + 1] = bTemp;
 }
 }
 }
 if ((iFilterLen & 1) > 0)
 bTemp = bTab[(iFilterLen - 1) / 2];
 else
 bTemp = (bTab[iFilterLen / 2] + bTab[iFilterLen / 2 - 1]) / 2;
 return bTemp;
}

float getNutrientEC(){
  float ecValue = nutrientTDS/500;
  ecValue = roundDecimalPoint(ecValue,1);
  nutrientEC = ecValue;
//  Serial.print("EC Value:");
//  Serial.print(nutrientEC,2);
  return ecValue;
}

float getNutrientCF(){
  float cfValue = nutrientEC*10;
  nutrientCF = cfValue;
//  Serial.print("CF Value:");
//  Serial.println(nutrientCF,0);
  return cfValue;
}

// Doser pH
void pHHysteris(float pHcalibration, float pHAutoSP) {
 //  Serial.println(pHcalibration);  
   Serial.println("pHHysteris");  
   Serial.println(pHAutoSP);  
   Serial.println(pHcalibration);
  
   pHcalibration = pHSetpoint;
    static unsigned long samplingTime = millis();
    static unsigned long printTime = millis();
    static float pHValue,
    voltage;
   if (millis() - samplingTime > samplingInterval) {
        pHArray[pHArrayIndex++] = analogRead(PH_PIN);
        if (pHArrayIndex == ArrayLenth)
            pHArrayIndex = 0;
        voltage = avergearray(pHArray, ArrayLenth) * 5.0 / 1024;
        pHValue = 3.5 * voltage + Offset;
        samplingTime = millis();
    }
    pH = roundDecimalPoint(pHValue, 2);

    HysterisMin = (pHSetpoint - phSetHysteris);
    HysterisPlus = (pHSetpoint + phSetHysteris);

    if (pH == pHSetpoint) {
        unsigned long currentMillis = millis();
        if (currentMillis - previousMillis > pinTime) {
            previousMillis = currentMillis;
            
            digitalWrite(PHHIGH_DIR_PIN, LOW);
            digitalWrite(PHLOW_DIR_PIN, LOW);
            for(int X = 0; X < 200; X++) {  
              digitalWrite(PHLOW_STEP_PIN,LOW);
              digitalWrite(PHHIGH_STEP_PIN,LOW);
              delayMicroseconds(500);
            AutoSwitchFun=false;
                       
            }                      
            } 
            Serial.println("ph + and - are OFF");
            phDown = 0;
            phUp = 0;
            pinTime = pinLowTime;
        }


    if (pH >= HysterisMin && pH <= HysterisPlus) {
        unsigned long currentMillis = millis();
        if (currentMillis - previousMillis > pinTime) {
            previousMillis = currentMillis;
            
            digitalWrite(PHHIGH_DIR_PIN, LOW);
            digitalWrite(PHLOW_DIR_PIN, LOW);
            for(int X = 0; X < 200; X++) {  
              digitalWrite(PHLOW_STEP_PIN,LOW);
              digitalWrite(PHHIGH_STEP_PIN,LOW);
              delayMicroseconds(500);            
            }
          Serial.println("ph + and - are LOW (hysteria correction)");
            phUp = 0;
            phDown = 0;
            pinTime = pinLowTime;
        }
    }

    if (pH < HysterisMin) {
        unsigned long currentMillis = millis();
        if (currentMillis - previousMillis > pinTime) {
            previousMillis = currentMillis;
            
            digitalWrite(PHHIGH_DIR_PIN, LOW);
            //digitalWrite(PHLOW_DIR_PIN, LOW);
            for(int X = 0; X < 200; X++) {  
              //digitalWrite(PHLOW_STEP_PIN, HIGH);
              digitalWrite(PHHIGH_STEP_PIN, HIGH);
              delayMicroseconds(500);
              digitalWrite(PHHIGH_STEP_PIN, LOW);
              delayMicroseconds(500);
             }                            
            Serial.println("ph + pin is HIGH");
            phUp = 1;
            phDown = 0;
            pinTime = pinHighTime;
        }
    }

    if (pH >= HysterisMin && pH < pHSetpoint) {
        unsigned long currentMillis = millis();
        if (currentMillis - previousMillis > pinTime) {
            previousMillis = currentMillis;
            digitalWrite(PHHIGH_DIR_PIN, LOW);
            //digitalWrite(PHLOW_DIR_PIN, LOW);
            for(int X = 0; X < 200; X++) {  
              //digitalWrite(PHLOW_STEP_PIN, HIGH);
              digitalWrite(PHHIGH_STEP_PIN, HIGH);
              delayMicroseconds(500);
              digitalWrite(PHHIGH_STEP_PIN, LOW);
              delayMicroseconds(500);              
           }
           Serial.println("ph + pin is HIGH");
            phUp = 1;
            phDown = 0;
            pinTime = pinHighTime;
        }
    }

    if (pH > HysterisPlus) {
        unsigned long currentMillis = millis();
        if (currentMillis - previousMillis > pinTime) {
            previousMillis = currentMillis;
            digitalWrite(PHLOW_DIR_PIN, LOW);
            //digitalWrite(PHLOW_DIR_PIN, LOW);
            for(int X = 0; X < 200; X++) {  
              digitalWrite(PHLOW_STEP_PIN, HIGH);
              //digitalWrite(PHLOW_STEP_PIN, LOW);
              delayMicroseconds(500);
              digitalWrite(PHLOW_STEP_PIN, LOW);
              delayMicroseconds(500);
            }
            Serial.println("ph - pin is HIGH");
            phDown = 1;
            phUp = 0;
            pinTime = pinLowTime;
        }
    }

    if (pH <= HysterisPlus && pH > pHSetpoint) {
        unsigned long currentMillis = millis();
        if (currentMillis - previousMillis > pinTime) {
            previousMillis = currentMillis;
            
            //digitalWrite(PHHIGH_DIR_PIN, LOW);
            digitalWrite(PHLOW_DIR_PIN, LOW);
            for(int X = 0; X < 200; X++) {  
              digitalWrite(PHLOW_STEP_PIN, HIGH);
              //digitalWrite(PHHIGH_STEP_PIN, LOW);
              delayMicroseconds(500);
              digitalWrite(PHLOW_STEP_PIN, LOW);
              delayMicroseconds(500);
              
            }
            Serial.println("ph - pin is HIGH");
            phUp = 0;
            phDown = 1;
            pinTime = pinLowTime;
        }
    }
//    phset();
//    phsetHys();
//    Serial.print("Setpoint = ");
//    Serial.println(pHSetpoint);
//    Serial.print("Hysteris = ");
//    Serial.println(phSetHysteris);
//    Serial.print("pH = ");
//    Serial.println(pH);
    
    delay(1000);
    return pH;
}

void ECHysteris(float ECcalibration , float ECHys){
  Serial.println("EC Calibration");  
   Serial.println(ECcalibration);  
   Serial.println(ECHys);
  ECSetpoint=ECcalibration;
  
  ECHys=ECSetHysteris;
  //temperature = readTemperature();  //add your temperature sensor and read it
//    gravityTds.setTemperature(nutrientTemperature);  // set the temperature and execute temperature compensation
//    gravityTds.update();  //sample and calculate
//    tdsValue = gravityTds.getTdsValue();  // then get the value
    
    getNutrientTDS();
    EC = roundDecimalPoint(ecValue, 0);

    ECHysterisMin = (ECSetpoint - ECSetHysteris);
    ECHysterisPlus = (ECSetpoint + ECSetHysteris);

    if (EC == ECSetpoint) {
        unsigned long currentMillis = millis();
        if (currentMillis - ECpreviousMillis > ECpinTime) {
            ECpreviousMillis = currentMillis;
            
            digitalWrite(EC_A_DIR_PIN, LOW);
            //digitalWrite(EC_B_DIR_PIN, LOW);
            //digitalWrite(EC_A_DIR_PIN, LOW);
            for(int X = 0; X < 200; X++) {  
              //digitalWrite(PHLOW_STEP_PIN, HIGH);
              digitalWrite(EC_A_STEP_PIN, HIGH);
              digitalWrite(EC_B_STEP_PIN, HIGH);
              //digitalWrite(EC_C_STEP_PIN, HIGH);
              delayMicroseconds(500);
              digitalWrite(EC_A_STEP_PIN, HIGH);
              digitalWrite(EC_B_STEP_PIN, HIGH);
              //digitalWrite(EC_C_STEP_PIN, HIGH);
              delayMicroseconds(500);
              
              
            }
            
            Serial.println("A B C is Low ");
            ECDown = 0;
            ECUp = 0;
            ECpinTime = ECpinLowTime;
            AutoSwitchFun=false;
        }
    }

    if (EC >= ECHysterisMin && EC <= ECHysterisPlus) {
        unsigned long currentMillis = millis();
        if (currentMillis - previousMillis > pinTime) {
            previousMillis = currentMillis;
            digitalWrite(EC_A_DIR_PIN, LOW);
            //digitalWrite(EC_B_DIR_PIN, LOW);
            //digitalWrite(EC_A_DIR_PIN, LOW);
            for(int X = 0; X < 200; X++) {  
              
              digitalWrite(EC_A_STEP_PIN, HIGH);
              digitalWrite(EC_B_STEP_PIN, HIGH);
              //digitalWrite(EC_C_STEP_PIN, HIGH);
              delayMicroseconds(500);
              digitalWrite(EC_A_STEP_PIN, HIGH);
              digitalWrite(EC_B_STEP_PIN, HIGH);
              //digitalWrite(EC_C_STEP_PIN, HIGH);
              delayMicroseconds(500);
              
            }      
            Serial.println("EC + and - are LOW (hysteria correction)");
            ECUp = 0;
            ECDown = 0;
            ECpinTime = ECpinLowTime;
        }
    }

    if (EC < ECHysterisMin) {
        unsigned long currentMillis = millis();
        if (currentMillis - previousMillis > pinTime) {
            previousMillis = currentMillis;
            digitalWrite(EC_A_DIR_PIN, LOW);
            //digitalWrite(EC_B_DIR_PIN, LOW);
            //digitalWrite(EC_A_DIR_PIN, LOW);
            for(int X = 0; X < 200; X++) {  
              
              digitalWrite(EC_A_STEP_PIN, HIGH);
              digitalWrite(EC_B_STEP_PIN, HIGH);
              //digitalWrite(EC_C_STEP_PIN, HIGH);
              delayMicroseconds(500);
              digitalWrite(EC_A_STEP_PIN, HIGH);
              digitalWrite(EC_B_STEP_PIN, HIGH);
              //digitalWrite(EC_C_STEP_PIN, HIGH);
              delayMicroseconds(500);
              
            }                

            Serial.println("ph + pin is HIGH");
            ECUp = 1;
            ECDown = 0;
            ECpinTime = ECpinHighTime;
        }
    }

    if (EC >= ECHysterisMin && EC < ECSetpoint) {
        unsigned long currentMillis = millis();
        if (currentMillis - previousMillis > pinTime) {
            previousMillis = currentMillis;
            digitalWrite(EC_A_DIR_PIN, LOW);
            //digitalWrite(EC_B_DIR_PIN, LOW);
            //digitalWrite(EC_A_DIR_PIN, LOW);
            for(int X = 0; X < 200; X++) {  
              
              digitalWrite(EC_A_STEP_PIN, HIGH);
              digitalWrite(EC_B_STEP_PIN, HIGH);
              //digitalWrite(EC_C_DIR_PIN, HIGH);
              delayMicroseconds(500);
              digitalWrite(EC_A_STEP_PIN, HIGH);
              digitalWrite(EC_B_STEP_PIN, HIGH);
              //digitalWrite(EC_C_STEP_PIN, HIGH);
              delayMicroseconds(500);
              
            }
            Serial.println("ph + pin is HIGH");
            ECUp = 1;
            ECDown = 0;
            ECpinTime = ECpinHighTime;
        }
    }

    if (EC > ECHysterisPlus) {
        unsigned long currentMillis = millis();
        if (currentMillis - previousMillis > pinTime) {
            previousMillis = currentMillis;
            digitalWrite(EC_A_DIR_PIN, LOW);
            //digitalWrite(EC_B_DIR_PIN, LOW);
            //digitalWrite(EC_A_DIR_PIN, LOW);
            for(int X = 0; X < 200; X++) {  
              
              digitalWrite(EC_A_STEP_PIN, HIGH);
              digitalWrite(EC_B_STEP_PIN, HIGH);
              //digitalWrite(EC_C_STEP_PIN, HIGH);
              delayMicroseconds(500);
              digitalWrite(EC_A_STEP_PIN, HIGH);
              digitalWrite(EC_B_STEP_PIN, HIGH);
              //digitalWrite(EC_C_DIR_PIN, HIGH);
              delayMicroseconds(500);
              
            }
            Serial.println("B And C pin is HIGH");
            ECDown = 1;
            ECUp = 0;
            ECpinTime = ECpinLowTime;
        }
    }

    if (EC <= ECHysterisPlus && EC > ECSetpoint) {
        unsigned long currentMillis = millis();
        if (currentMillis - previousMillis > pinTime) {
            previousMillis = currentMillis;
            digitalWrite(EC_A_DIR_PIN, LOW);
            //digitalWrite(EC_B_STEP_PIN, LOW);
            //digitalWrite(EC_A_STEP_PIN, LOW);
            for(int X = 0; X < 200; X++) {  
              //digitalWrite(PHLOW_STEP_PIN, HIGH);
              digitalWrite(EC_A_STEP_PIN, HIGH);
              digitalWrite(EC_B_STEP_PIN, HIGH);
              //digitalWrite(EC_C_STEP_PIN, HIGH);
              delayMicroseconds(500);
              digitalWrite(EC_A_STEP_PIN, HIGH);
              digitalWrite(EC_B_STEP_PIN, HIGH);
              //digitalWrite(EC_C_STEP_PIN, HIGH);
              delayMicroseconds(500);
              
            }  
            
            Serial.println("A B C PUMP ON");
            ECUp = 0;
            ECDown = 1;
            ECpinTime = ECpinLowTime;
        }
    }
    Serial.print("EC Setpoint = ");
    Serial.println(ECSetpoint);
    Serial.print("EC Hysteris = ");
    Serial.println(ECSetHysteris);
    
    Serial.print(EC,0);
    Serial.println("ppm");
    
    delay(5000);
    
  }



void pHAuto(float autosetphvalue) {
  float pHAutoSetValue =autosetphvalue;
  Serial.println("pHAuto start");
    static unsigned long samplingTime = millis();
    static unsigned long printTime = millis();
    static float pHValue, voltage;
    
   if (millis() - samplingTime > samplingInterval) {
        pHArray[pHArrayIndex++] = analogRead(PH_PIN);
        if (pHArrayIndex == ArrayLenth)
            pHArrayIndex = 0;
        voltage = avergearray(pHArray, ArrayLenth) * 5.0 / 1024;
        pHValue = 3.5 * voltage + Offset;
        samplingTime = millis();
    }
    pH = roundDecimalPoint(pHValue, 2);
    Serial.println("raj");  
     Serial.println(pHAutoSetValue);  
    HysterisMin = (pHAutoSetValue - phSetHysteris);
    HysterisPlus = (pHAutoSetValue + phSetHysteris);

    if (pH == pHAutoSetValue) {
        unsigned long currentMillis = millis();
        if (currentMillis - previousMillis > pinTime) {
            previousMillis = currentMillis;
            
            digitalWrite(PHHIGH_DIR_PIN, LOW);
            digitalWrite(PHLOW_DIR_PIN, LOW);
            for(int X = 0; X < 200; X++) {  
              digitalWrite(PHLOW_STEP_PIN,LOW);
              digitalWrite(PHHIGH_STEP_PIN,LOW);
              delayMicroseconds(500);   
              Resetfun == true;          
            }                      
            } 
            Serial.println("ph + and - are OFF");
            phDown = 0;
            phUp = 0;
            pinTime = pinLowTime;
        }


    if (pH >= HysterisMin && pH <= HysterisPlus) {
        unsigned long currentMillis = millis();
        if (currentMillis - previousMillis > pinTime) {
            previousMillis = currentMillis;
            
            digitalWrite(PHHIGH_DIR_PIN, LOW);
            digitalWrite(PHLOW_DIR_PIN, LOW);
            for(int X = 0; X < 200; X++) {  
              digitalWrite(PHLOW_STEP_PIN,LOW);
              digitalWrite(PHHIGH_STEP_PIN,LOW);
              delayMicroseconds(500);            
            }
          Serial.println("ph + and - are LOW (hysteria correction)");
            phUp = 0;
            phDown = 0;
            pinTime = pinLowTime;
        }
    }

    if (pH < HysterisMin) {
        unsigned long currentMillis = millis();
        if (currentMillis - previousMillis > pinTime) {
            previousMillis = currentMillis;
            
            digitalWrite(PHHIGH_DIR_PIN, LOW);
            //digitalWrite(PHLOW_DIR_PIN, LOW);
            for(int X = 0; X < 200; X++) {  
              //digitalWrite(PHLOW_STEP_PIN, HIGH);
              digitalWrite(PHHIGH_STEP_PIN, HIGH);
              delayMicroseconds(500);
              digitalWrite(PHHIGH_STEP_PIN, LOW);
              delayMicroseconds(500);
             }                            
            Serial.println("ph + pin is HIGH");
            phUp = 1;
            phDown = 0;
            pinTime = pinHighTime;
        }
    }

    if (pH >= HysterisMin && pH < pHAutoSetValue) {
        unsigned long currentMillis = millis();
        if (currentMillis - previousMillis > pinTime) {
            previousMillis = currentMillis;
            digitalWrite(PHHIGH_DIR_PIN, LOW);
            //digitalWrite(PHLOW_DIR_PIN, LOW);
            for(int X = 0; X < 200; X++) {  
              //digitalWrite(PHLOW_STEP_PIN, HIGH);
              digitalWrite(PHHIGH_STEP_PIN, HIGH);
              delayMicroseconds(500);
              digitalWrite(PHHIGH_STEP_PIN, LOW);
              delayMicroseconds(500);              
           }
           Serial.println("ph + pin is HIGH");
            phUp = 1;
            phDown = 0;
            pinTime = pinHighTime;
        }
    }

    if (pH > HysterisPlus) {
        unsigned long currentMillis = millis();
        if (currentMillis - previousMillis > pinTime) {
            previousMillis = currentMillis;
            digitalWrite(PHLOW_DIR_PIN, LOW);
            //digitalWrite(PHLOW_DIR_PIN, LOW);
            for(int X = 0; X < 200; X++) {  
              digitalWrite(PHLOW_STEP_PIN, HIGH);
              //digitalWrite(PHLOW_STEP_PIN, LOW);
              delayMicroseconds(500);
              digitalWrite(PHLOW_STEP_PIN, LOW);
              delayMicroseconds(500);
            }
            Serial.println("ph - pin is HIGH");
            phDown = 1;
            phUp = 0;
            pinTime = pinLowTime;
        }
    }

    if (pH <= HysterisPlus && pH > pHAutoSetValue) {
        unsigned long currentMillis = millis();
        if (currentMillis - previousMillis > pinTime) {
            previousMillis = currentMillis;
            
            //digitalWrite(PHHIGH_DIR_PIN, LOW);
            digitalWrite(PHLOW_DIR_PIN, LOW);
            for(int X = 0; X < 200; X++) {  
              digitalWrite(PHLOW_STEP_PIN, HIGH);
              //digitalWrite(PHHIGH_STEP_PIN, LOW);
              delayMicroseconds(500);
              digitalWrite(PHLOW_STEP_PIN, LOW);
              delayMicroseconds(500);
              
            }
            Serial.println("ph - pin is HIGH");
            phUp = 0;
            phDown = 1;
            pinTime = pinLowTime;
        }
    }
//    phset();
//    phsetHys();
    Serial.print("Setpoint = ");
    Serial.println(pHAutoSetValue);
    Serial.print("Hysteris = ");
    Serial.println(phSetHysteris);
    Serial.print("pH = ");
    Serial.println(pH);
    
    delay(1000);
    return pH;
}

void ECAuto(float autosetecvalue){
  float ECAutoSetpoint =autosetecvalue;
  //temperature = readTemperature();  //add your temperature sensor and read it
//    gravityTds.setTemperature(nutrientTemperature);  // set the temperature and execute temperature compensation
//    gravityTds.update();  //sample and calculate
//    tdsValue = gravityTds.getTdsValue();  // then get the value
    
    getNutrientTDS();
    EC = roundDecimalPoint(ecValue, 0);

    ECHysterisMin = (ECAutoSetpoint - ECSetHysteris);
    ECHysterisPlus = (ECAutoSetpoint + ECSetHysteris);

    if (EC == ECAutoSetpoint) {
        unsigned long currentMillis = millis();
        if (currentMillis - ECpreviousMillis > ECpinTime) {
            ECpreviousMillis = currentMillis;
            
            digitalWrite(EC_A_DIR_PIN, LOW);
            //digitalWrite(EC_B_DIR_PIN, LOW);
            //digitalWrite(EC_A_DIR_PIN, LOW);
            for(int X = 0; X < 200; X++) {  
              //digitalWrite(PHLOW_STEP_PIN, HIGH);
              digitalWrite(EC_A_STEP_PIN, HIGH);
              digitalWrite(EC_B_STEP_PIN, HIGH);
              //digitalWrite(EC_C_STEP_PIN, HIGH);
              delayMicroseconds(500);
              digitalWrite(EC_A_STEP_PIN, HIGH);
              digitalWrite(EC_B_STEP_PIN, HIGH);
              //digitalWrite(EC_C_STEP_PIN, HIGH);
              delayMicroseconds(500);
              Resetfun == true;
              
            }
            
            Serial.println("A B C is Low ");
            ECDown = 0;
            ECUp = 0;
            ECpinTime = ECpinLowTime;
        }
    }

    if (EC >= ECHysterisMin && EC <= ECHysterisPlus) {
        unsigned long currentMillis = millis();
        if (currentMillis - previousMillis > pinTime) {
            previousMillis = currentMillis;
            digitalWrite(EC_A_DIR_PIN, LOW);
            //digitalWrite(EC_B_DIR_PIN, LOW);
            //digitalWrite(EC_A_DIR_PIN, LOW);
            for(int X = 0; X < 200; X++) {  
              
              digitalWrite(EC_A_STEP_PIN, HIGH);
              digitalWrite(EC_B_STEP_PIN, HIGH);
              //digitalWrite(EC_C_STEP_PIN, HIGH);
              delayMicroseconds(500);
              digitalWrite(EC_A_STEP_PIN, HIGH);
              digitalWrite(EC_B_STEP_PIN, HIGH);
              //digitalWrite(EC_C_STEP_PIN, HIGH);
              delayMicroseconds(500);
              
            }      
            Serial.println("EC + and - are LOW (hysteria correction)");
            ECUp = 0;
            ECDown = 0;
            ECpinTime = ECpinLowTime;
        }
    }

    if (EC < ECHysterisMin) {
        unsigned long currentMillis = millis();
        if (currentMillis - previousMillis > pinTime) {
            previousMillis = currentMillis;
            digitalWrite(EC_A_DIR_PIN, LOW);
            //digitalWrite(EC_B_DIR_PIN, LOW);
            //digitalWrite(EC_A_DIR_PIN, LOW);
            for(int X = 0; X < 200; X++) {  
              
              digitalWrite(EC_A_STEP_PIN, HIGH);
              digitalWrite(EC_B_STEP_PIN, HIGH);
              //digitalWrite(EC_C_STEP_PIN, HIGH);
              delayMicroseconds(500);
              digitalWrite(EC_A_STEP_PIN, HIGH);
              digitalWrite(EC_B_STEP_PIN, HIGH);
              //digitalWrite(EC_C_STEP_PIN, HIGH);
              delayMicroseconds(500);
              
            }                

            Serial.println("ph + pin is HIGH");
            ECUp = 1;
            ECDown = 0;
            ECpinTime = ECpinHighTime;
        }
    }

    if (EC >= ECHysterisMin && EC < ECAutoSetpoint) {
        unsigned long currentMillis = millis();
        if (currentMillis - previousMillis > pinTime) {
            previousMillis = currentMillis;
            digitalWrite(EC_A_DIR_PIN, LOW);
            //digitalWrite(EC_B_DIR_PIN, LOW);
            //digitalWrite(EC_A_DIR_PIN, LOW);
            for(int X = 0; X < 200; X++) {  
              
              digitalWrite(EC_A_STEP_PIN, HIGH);
              digitalWrite(EC_B_STEP_PIN, HIGH);
              //digitalWrite(EC_C_DIR_PIN, HIGH);
              delayMicroseconds(500);
              digitalWrite(EC_A_STEP_PIN, HIGH);
              digitalWrite(EC_B_STEP_PIN, HIGH);
              //digitalWrite(EC_C_STEP_PIN, HIGH);
              delayMicroseconds(500);
              
            }
            Serial.println("ph + pin is HIGH");
            ECUp = 1;
            ECDown = 0;
            ECpinTime = ECpinHighTime;
        }
    }

    if (EC > ECHysterisPlus) {
        unsigned long currentMillis = millis();
        if (currentMillis - previousMillis > pinTime) {
            previousMillis = currentMillis;
            digitalWrite(EC_A_DIR_PIN, LOW);
            //digitalWrite(EC_B_DIR_PIN, LOW);
            //digitalWrite(EC_A_DIR_PIN, LOW);
            for(int X = 0; X < 200; X++) {  
              
              digitalWrite(EC_A_STEP_PIN, HIGH);
              digitalWrite(EC_B_STEP_PIN, HIGH);
              //digitalWrite(EC_C_STEP_PIN, HIGH);
              delayMicroseconds(500);
              digitalWrite(EC_A_STEP_PIN, HIGH);
              digitalWrite(EC_B_STEP_PIN, HIGH);
              //digitalWrite(EC_C_DIR_PIN, HIGH);
              delayMicroseconds(500);
              
            }
            Serial.println("B And C pin is HIGH");
            ECDown = 1;
            ECUp = 0;
            ECpinTime = ECpinLowTime;
        }
    }

    if (EC <= ECHysterisPlus && EC > ECAutoSetpoint) {
        unsigned long currentMillis = millis();
        if (currentMillis - previousMillis > pinTime) {
            previousMillis = currentMillis;
            digitalWrite(EC_A_DIR_PIN, LOW);
            //digitalWrite(EC_B_STEP_PIN, LOW);
            //digitalWrite(EC_A_STEP_PIN, LOW);
            for(int X = 0; X < 200; X++) {  
              //digitalWrite(PHLOW_STEP_PIN, HIGH);
              digitalWrite(EC_A_STEP_PIN, HIGH);
              digitalWrite(EC_B_STEP_PIN, HIGH);
              //digitalWrite(EC_C_STEP_PIN, HIGH);
              delayMicroseconds(500);
              digitalWrite(EC_A_STEP_PIN, HIGH);
              digitalWrite(EC_B_STEP_PIN, HIGH);
              //digitalWrite(EC_C_STEP_PIN, HIGH);
              delayMicroseconds(500);
              
            }   
            
            Serial.println("A B C PUMP ON");
            ECUp = 0;
            ECDown = 1;
            ECpinTime = ECpinLowTime;
        }
    }
    
    Serial.print("EC Setpoint = ");
    Serial.println(ECAutoSetpoint);
    Serial.print("EC Hysteris = ");
    Serial.println(ECSetHysteris);
    
    Serial.print(ecValue,0);
    Serial.println("d/ms");
    
    delay(2000);
    
  }

int hourupg;
int minupg;

boolean Reset=false;
void dateandtime(void) {
DateTime now = rtc.now();
hourupg=now.hour();
minupg=now.minute();

}

//void testfun(){
//  float pHAutoSetValue = pHs1;
//  pHAuto(pHAutoSetValue);
//  Serial.println("testfun");
//  Serial.println(pHAutoSetValue);
//  delay(100);
//  }
//
//  static byte trigger = 1;
//  dateandtime();
//  if(hourupg == S1Hr && minupg == S1Min && Resetfun == false){
//     //Serial.print(hourupg);
//     //Serial.print(S1Hr);
//   
// if (trigger++) {
//          float pHAutoSetValue =  pHs1;
//     
//     float ECAutoSetValue =  ECs1;
//     digitalWrite(BUZZER, HIGH);
//     delay(1000);
//     digitalWrite(BUZZER, LOW);
//     delay(2000);
//     
//
//     
//      pHAuto(pHAutoSetValue );
//      Serial.println(pHs1);
//      //ecautodose
//      
//      float ECAuto(ECAutoSetValue );
//      Serial.print(ECs1);
//    Serial.println("Shift 1 Progressing");
//      trigger = 0;
//    }
//  }
//  else {
//    trigger = 1;
// 
//  
//          }

void scheduleFun() {

  static byte trigger = 1;
  dateandtime();
  if(hourupg >= S1Hr && minupg >= S1Min && Resetfun == false){
     //Serial.print(hourupg);p
     //Serial.print(S1Hr);
   
 //for (trigger++) {
     float pHAutoSetValue =  pHs1;
     
     float ECAutoSetValue =  ECs1;
     digitalWrite(BUZZER, HIGH);
     delay(1000);
     digitalWrite(BUZZER, LOW);
     delay(2000);
     

     
      pHAuto(pHAutoSetValue );
      Serial.println(pHs1);
      //ecautodose
      
      float ECAuto(ECAutoSetValue );
      Serial.print(ECs1);
      Serial.println("Shift 1 Progressing");
     // trigger = 0;
    
  }
  else {
    trigger = 1;
 
  
          }
  
     
      //phautodose
//      float pHAutoSetValue =  pHs1;
//      pHAuto(pHAutoSetValue );
//      Serial.println(pHs1);
//      //ecautodose
//      float ECAutoSetValue =  ECs1;
//      float ECAuto(ECAutoSetValue );
//      Serial.print(ECs1);
//    Serial.println("Shift 1 Progressing");
    
    if(hourupg==S2Hr && minupg==S2Min){

      
      float pHAutoSetValue =  pHs2;
       pHAuto(pHAutoSetValue );
      
      float ECAutoSetValue =  ECs2;
      float ECAuto(ECAutoSetValue );

      Serial.print("Shift 2 Progressing");
      }
    if(hourupg==S3Hr && minupg==S3Min){
      
      float pHAutoSetValue =  pHs3;
      pHAuto(pHAutoSetValue );

      float ECAutoSetValue =  ECs3;
      float ECAuto(ECAutoSetValue );
      Serial.print("Shift 3 Progressing");
      
      }
      if(hourupg==S4Hr && minupg==S4Min){
     
      float pHAutoSetValue =  pHs4;
      pHAuto(pHAutoSetValue );

      float ECAutoSetValue =  ECs4;
      float ECAuto(ECAutoSetValue );
      Serial.print("Shift 4 Progressing");
      
      }
      if(hourupg==S5Hr && minupg==S5Min){
     
      float pHAutoSetValue =  pHs5;
      pHAuto(pHAutoSetValue );

      float ECAutoSetValue =  ECs5;
       ECAuto(ECAutoSetValue );
      Serial.print("Shift 5 Progressing");
      
      }
 }




float roundDecimalPoint( float in_value, int decimal_place )
{
  float multiplier = powf( 10.0f, decimal_place );
  in_value = roundf( in_value * multiplier ) / multiplier;
  return in_value;
}

double avergearray(int * arr, int number) {
    int i;
    int max,
    min;
    double avg;
    long amount = 0;
    if (number <= 0) {
        Serial.println("Error number for the array to avraging!/n");
        return 0;
    }
    if (number < 5) { //less than 5, calculated directly statistics
        for (i = 0; i < number; i++) {
            amount += arr[i];
        }
        avg = amount / number;
        return avg;
    } else {
        if (arr[0] < arr[1]) {
            min = arr[0];
            max = arr[1];
        } else {
            min = arr[1];
            max = arr[0];
        }
        for (i = 2; i < number; i++) {
            if (arr[i] < min) {
                amount += min; //arr<min
                min = arr[i];
            } else {
                if (arr[i] > max) {
                    amount += max; //arr>max
                    max = arr[i];
                } else {
                    amount += arr[i]; //min<=arr<=max
                }
            } //if
        } //for
        avg = (double)amount / (number - 2);
    } //if
    return avg;
}


long waterconlevel;
void getNutrientTL()
{
digitalWrite(trigPin, LOW);
delayMicroseconds (2);
digitalWrite(trigPin, HIGH);
delayMicroseconds (10);
digitalWrite(trigPin, LOW);


duration = pulseIn(echoPin, HIGH);
distance = duration*0.034/2;
volume = pi*16*distance; //change based on the volume for the tank
waterHeight = tankHeight-distance;
waterQuantity = totalVolume-volume-offset;
waterconlevel = tankHeight-waterHeight;

Serial.print("Tank is: ");
Serial.print(distance);
Serial.println(" Cm");
Serial.print("Water level [cm]: ");
Serial.println(waterHeight);
Serial.print("Water Quantity [ml]: ");
Serial.println(waterQuantity);

delay(100);
if(distance==waterconlevel && duration==0)
 {
     digitalWrite(RORelay, LOW);
     digitalWrite(BUZZER, HIGH);
     
     Serial.print("Water Tank Full Motor Turned OFF");
     delay(1000);
     digitalWrite(BUZZER, LOW);
     delay(3000);
     Wtemp=1;
 }
 
//  else if(distance<12 && duration==1)
// {
//     digitalWrite(RORelay, LOW); 
//     Serial.println("Water Tank Full Motor Turned OFF ");
//     delay(2000);
// }
 
 else if(distance==tankHeight)
 {
   digitalWrite(RORelay, HIGH);
   Serial.println("LOW Water Level Motor Turned ON");
   delay(2000);
   Wtemp=0;
 }
 return distance;
 
}
