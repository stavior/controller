#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
     
const char* ssid = "Temporary Name";
const char* password = "nothing123";

ESP8266WebServer server(80);
MDNSResponder mdns;
 String webPage = "";
void setup(void){
  Serial.begin(115200);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }
  webPage += "<h1>robot sterilizer</h1>";
  webPage += "<p>front:<a href=\"F\"><button>ON</button></a>&nbsp;</p>";
  webPage += "<p>back:<a href=\"B\"><button>ON</button></a>&nbsp;</p>";
  webPage += "<p>right:<a href=\"R\"><button>ON</button></a>&nbsp;</p>";
  webPage += "<p>left:<a href=\"L\"><button>ON</button></a>&nbsp;</p>";
  webPage += "<p>stop:<a href=\"S\"><button>ON</button></a>&nbsp;</p>";
  WiFi.begin(ssid, password);
  Serial.println("");
     

  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to ");  //  "Подключились к "
  Serial.println(ssid);
  Serial.print("IP address: ");  //  "IP-адрес: "
  Serial.println(WiFi.localIP());

  if (mdns.begin("esp8266", WiFi.localIP())) {
    Serial.println("MDNS responder started");
  }
     
    server.on("/", [](){
    server.send(200, "text/html", webPage);
  });
  
    server.on("/F", [](){
    server.send(200, "text/html", webPage);
    Serial.println("f");
    delay(1000);
  });
  
    server.on("/S", [](){
    server.send(200, "text/html", webPage);
    Serial.println("z");  
    delay(1000);
  });
//led 2
    server.on("/B", [](){
    server.send(200, "text/html", webPage);
    Serial.println("w");
    delay(1000);
  });
  
    server.on("/R", [](){
    server.send(200, "text/html", webPage);
    Serial.println("q");
    delay(1000);
  });
    server.on("/L", [](){
    server.send(200, "text/html", webPage);
    Serial.println("k");
    delay(1000);
  });
    server.on("/Led?????", [](){
    server.send(200, "text/html", webPage);
    Serial.println("[OFF]");
    delay(1000);
  });
  
  server.begin();
  Serial.println("HTTP server started");
  
}
     
void loop(void){
  server.handleClient();
}
