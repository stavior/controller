#include <Wire.h>  
#include <MPU6050_tockn.h>                   
#include <LiquidCrystal_I2C.h>       

LiquidCrystal_I2C lcd(0x3F,20,4); 

//wheels pin configartion 
#define wheel1 2
#define wheel2 3

//wheels speed pin configartion 
#define speed1 4
#define speed2 5

//sensors pin configartion 
#define RIR 8
#define LIR 9
#define MIR 10

#define trigPin 22
#define echoPin 24

#define trigPin1 26
#define echoPin1 27
#define MINI 12

#define BREAK 13
#define UVLIGHT 25
 
//RC recevier pin configartion 
int channel_2 = 6;  // pin with ~ symbol
int channel_4 = 7;  // pin with ~ symbol
int channel_5 = 46;
int channel_6 = 45;

// Define variables: 
int pwm = 0;
int value1,value2,value3,value4;
long duration; 
int distance;
long duration2; 
int distance2;
MPU6050 mpu6050(Wire);

long timer = 0;

//Initial Speed of Motor
int initial_motor_speed = 140;

// PID Constants
float Kp = 25;
float Ki = 0;
float Kd = 15;

float error = 0, P = 0, I = 0, D = 0, PID_value = 0;
float previous_error = 0, previous_I = 0;
int flag = 0;

void setup() {  // put your setup code here, to run once: 
Serial.begin(115200);
Wire.begin();
mpu6050.begin();
mpu6050.calcGyroOffsets(true);

lcd.init();  
pinMode(wheel1,OUTPUT);
pinMode(wheel2,OUTPUT);
pinMode(speed1,OUTPUT);
pinMode(speed2,OUTPUT);
pinMode(BREAK,OUTPUT);
digitalWrite(BREAK,HIGH);

analogWrite(speed1,0);
analogWrite(speed2,0);

pinMode(RIR,INPUT);
pinMode(LIR,INPUT);
//pinMode(MIR,INPUT);
pinMode(MINI,INPUT);

pinMode(UVLIGHT,OUTPUT);

digitalWrite(UVLIGHT,HIGH);

pinMode(trigPin,OUTPUT);
pinMode(echoPin,INPUT);
pinMode(trigPin1,OUTPUT);
pinMode(echoPin1,INPUT);

pinMode(channel_2, INPUT);
pinMode(channel_4, INPUT);
pinMode(channel_5, INPUT);
pinMode(channel_6, INPUT);
  lcd.backlight();                
  lcd.setCursor(0,0);           
  lcd.print("WELCOME TO CENAURA");
  lcd.setCursor(3,1);             
  lcd.print("ROBOT STERILIZER!");
  lcd.setCursor(3,2);            
  lcd.print("VISIT OUR WEBSITE");
  lcd.setCursor(2,3);             
  lcd.print("www.cenaura.com");
  mpu6050.update();
  delay(2000); 
}
void loop() { // put your main code here, to run repeatedly: 

value3 = pulseIn(channel_5, HIGH);
Serial.print("channel_5 \t");
Serial.println(value3);

value4 = pulseIn(channel_6, HIGH);
Serial.print("channel_6 \t");
Serial.println(value4);

digitalWrite(trigPin,LOW); // Clear the trigPin by setting it LOW:
delayMicroseconds(5);

digitalWrite(trigPin, HIGH);
delayMicroseconds(10);// Trigger the sensor by setting the trigPin high for 10 microseconds:
digitalWrite(trigPin, LOW);

duration = pulseIn(echoPin, HIGH); // Read the echoPin. pulseIn() returns the duration (length of the pulse) in microseconds:

distance = duration*0.034/2; // Calculate the distance:
Serial.print("Distance = ");
Serial.print(distance);
Serial.println(" cm");

digitalWrite(trigPin1,LOW); // Clear the trigPin by setting it LOW:
delayMicroseconds(5);

digitalWrite(trigPin1, HIGH);
delayMicroseconds(10);// Trigger the sensor by setting the trigPin high for 10 microseconds:
digitalWrite(trigPin1, LOW);

duration2 = pulseIn(echoPin1, HIGH); // Read the echoPin. pulseIn() returns the duration (length of the pulse) in microseconds:
distance2 = duration2*0.034/2; // Calculate the distance:
Serial.print("Distance2 = ");
Serial.print(distance2);
Serial.println(" cm");

Serial.println(digitalRead(RIR));
Serial.println(digitalRead(LIR));
Serial.println(digitalRead(MINI)); 
 
if(((digitalRead(RIR)==0) || (digitalRead(LIR)==0) ||(digitalRead(MINI)== 0))||((distance>=22 && distance<30) || (distance2>=22 && distance2<30)))//
{ 
sen();
}
else
{
 if(value4>900 && value4<1399)
{
  lcd.clear();
  lcd.setCursor(0,0);           
  lcd.print("variable speed drive");
  Serial.println("variable speed drive");
  robotdirection();
}  
if(value4>1500 && value4<2000)
{
  lcd.clear();
  lcd.setCursor(0,0);           
  lcd.print("constant speed drive");
  Serial.println("constatnt speed driving");
  uvmode(); 
}
if(value3>=1400)
{
  uvon();
}
else
{
  uvoff();
} 
}
}
void robotdirection()
{
value1 = pulseIn(channel_2, HIGH);
Serial.print("channel_2 \t");
Serial.println(value1);

value2 = pulseIn(channel_4, HIGH);
Serial.print("channel_4 \t");
Serial.println(value2);  
 
if((value1 > 1530) && (value1 < 2000))
{
Serial.println("BACKWARD");
pwm = map(value1, 1530, 2000, 10, 80); 
backward();
}
else if((value1 > 980) && (value1 < 1400))
{
Serial.println("FORDWARD");
pwm = map(value1, 1400, 1000, 10,80); 
forward();
}
if((value2 > 1530) && (value2 < 2000))
{
Serial.println("RIGHT");
pwm = map(value2, 1530, 2000, 0, 20);
right();
}
else if((value2 > 980) && (value2 < 1400))
{
Serial.println("LEFT");
pwm = map(value2, 1400, 1000, 0, 20);
left();  
}
if((value1 > 1400) && (value1 < 1500) && (value2 > 1400) && (value2 < 1500))
{
 stop1(); 
}
}
void sen()
{
digitalWrite(BREAK,LOW);
stop1();

value1 = pulseIn(channel_2, HIGH);
Serial.print("channel_2 \t");
Serial.println(value1);

value2 = pulseIn(channel_4, HIGH);
Serial.print("channel_4 \t");
Serial.println(value2); 
if((value1 > 980) && (value1 < 1400))
{
Serial.println("BACKWARD");
pwm = map(value1, 1400, 1000, 10,80); 
forward();
}
if((value1 > 1400) && (value1 < 1500) && (value2 > 1400) && (value2 < 1500))
{ 
 digitalWrite(BREAK,LOW);
 stop1();
}   
}
void forward()
{
  lcd.clear();
  lcd.setCursor(0,0);           
  lcd.print("FORWARD DIRECTION");
Serial.println("FORWARD");
digitalWrite(BREAK,HIGH);
digitalWrite(wheel1,LOW);
digitalWrite(wheel2,HIGH);
analogWrite(speed1,pwm);
analogWrite(speed2,pwm); 
positionreading(); 
}
void backward()
{
  lcd.clear();
  lcd.setCursor(0,0);           
  lcd.print("BACKWARD DIRECTION");
Serial.println("BACKWARD");
digitalWrite(BREAK,HIGH);
digitalWrite(wheel1,HIGH);
digitalWrite(wheel2,LOW);
analogWrite(speed1,pwm);
analogWrite(speed2,pwm); 
positionreading();
}
void right()
{
  lcd.clear();
  lcd.setCursor(0,0);           
  lcd.print("RIGHT DIRECTION");
Serial.println("RIGHT");
digitalWrite(BREAK,HIGH);
digitalWrite(wheel1,HIGH);
digitalWrite(wheel2,HIGH);
analogWrite(speed1,pwm);
analogWrite(speed2,pwm); 
delay(500);
digitalWrite(BREAK,LOW);
analogWrite(speed1,0);
analogWrite(speed2,0);  
}
void left()
{
  lcd.clear();
  lcd.setCursor(0,0);           
  lcd.print("LEFT DIRECTION");
Serial.println("LEFT");
digitalWrite(BREAK,HIGH);
digitalWrite(wheel1,LOW);
digitalWrite(wheel2,LOW);
analogWrite(speed1,pwm);
analogWrite(speed2,pwm);
delay(500);
digitalWrite(BREAK,LOW);
analogWrite(speed1,0);
analogWrite(speed2,0);  
}
void stop1()
{
  lcd.clear();
  lcd.setCursor(0,0);           
  lcd.print("ROBOT STOP");
Serial.println("ROBOT STOP");
digitalWrite(BREAK,LOW);
analogWrite(speed1,0);
analogWrite(speed2,0);  
}
void uvon()
{
  Serial.println("uv light on");
  digitalWrite(UVLIGHT,LOW);
}
void uvoff()
{
  Serial.println("uv light off");
  digitalWrite(UVLIGHT,HIGH);
}
void uvmode()
{
value1 = pulseIn(channel_2, HIGH);
Serial.print("channel_2 \t");
Serial.println(value1);

value2 = pulseIn(channel_4, HIGH);
Serial.print("channel_4 \t");
Serial.println(value2);

if((value1 > 1530) && (value1 < 2000))
{
Serial.println("BACKWARD");
digitalWrite(BREAK,HIGH);
digitalWrite(wheel1,HIGH);
digitalWrite(wheel2,LOW);
analogWrite(speed1,20);
analogWrite(speed2,20);

}
if((value1 > 980) && (value1 < 1400))
{
Serial.println("FORDWARD");
digitalWrite(BREAK,HIGH);
 digitalWrite(wheel1,LOW);
digitalWrite(wheel2,HIGH);
analogWrite(speed1,20);
analogWrite(speed2,20); 
}
if((value2 > 1530) && (value2 < 2000))
{
Serial.println("RIGHT");
digitalWrite(BREAK,HIGH);
digitalWrite(wheel1,HIGH);
digitalWrite(wheel2,HIGH);
analogWrite(speed1,20);
analogWrite(speed2,20); 
delay(500);
digitalWrite(BREAK,LOW);
analogWrite(speed1,0);
analogWrite(speed2,0);  
}
if((value2 > 980) && (value2 < 1400))
{
Serial.println("LEFT");
digitalWrite(BREAK,HIGH);
digitalWrite(wheel1,LOW);
digitalWrite(wheel2,LOW);
analogWrite(speed1,20);
analogWrite(speed2,20); 
delay(500);
digitalWrite(BREAK,LOW);
analogWrite(speed1,0);
analogWrite(speed2,0);   
}
if((value1 > 1400) && (value1 < 1500) && (value2 > 1400) && (value2 < 1500))
{
digitalWrite(BREAK,LOW);
analogWrite(speed1,0);
analogWrite(speed2,0);
} 
}
void positionreading()
{
  if(millis() - timer > 1000){
    
    Serial.println("=======================================================");
    Serial.print("temp : ");Serial.println(mpu6050.getTemp());
    Serial.print("accX : ");Serial.print(mpu6050.getAccX());
    Serial.print("\taccY : ");Serial.print(mpu6050.getAccY());
    Serial.print("\taccZ : ");Serial.println(mpu6050.getAccZ());
  
    Serial.print("gyroX : ");Serial.print(mpu6050.getGyroX());
    Serial.print("\tgyroY : ");Serial.print(mpu6050.getGyroY());
    Serial.print("\tgyroZ : ");Serial.println(mpu6050.getGyroZ());
  
    Serial.print("accAngleX : ");Serial.print(mpu6050.getAccAngleX());
    Serial.print("\taccAngleY : ");Serial.println(mpu6050.getAccAngleY());
  
    Serial.print("gyroAngleX : ");Serial.print(mpu6050.getGyroAngleX());
    Serial.print("\tgyroAngleY : ");Serial.print(mpu6050.getGyroAngleY());
    Serial.print("\tgyroAngleZ : ");Serial.println(mpu6050.getGyroAngleZ());
    
    Serial.print("angleX : ");Serial.print(mpu6050.getAngleX());
    Serial.print("\tangleY : ");Serial.print(mpu6050.getAngleY());
    Serial.print("\tangleZ : ");Serial.println(mpu6050.getAngleZ());
    Serial.println("=======================================================\n");
    timer = millis();
  if ()
    error = 3;
  else if ()
    error = 2;
  else if ()
    error = 1;
  else if ()
    error = 0;
  else if ()
    error = -1;
  else if ()
    error = -2;
  else if ()
    error = -3;
  else if () // Turn robot left side
    error = 100;
  else if (mpu6050.getAccAngleX()<2.0) // Turn robot right side
    error = 101;
  else if () // Make U turn
    error = 102;
  else if () // Turn left side or stop
    error = 103;  
  }
}
void errorchecking()
{
  positionreading();
  Serial.print(error);
  if (error == 100) {               // Make left turn untill it detects straight path
    //Serial.print("\t");
    //Serial.println("Left");
    do {
       positionreading();
      analogWrite(ENA, 110); //Left Motor Speed
      analogWrite(ENB, 90); //Right Motor Speed
      sharpLeftTurn();
    } while (error != 0);

  } else if (error == 101) {          // Make right turn in case of it detects only right path (it will go into forward direction in case of staright and right "|--")
                                      // untill it detects straight path.
    //Serial.print("\t");
    //Serial.println("Right");
    analogWrite(ENA, 110); //Left Motor Speed
    analogWrite(ENB, 90); //Right Motor Speed
    forward();
    delay(200);
    stop_bot();
     positionreading();
    if (error == 102) {
      do {
        analogWrite(ENA, 110); //Left Motor Speed
        analogWrite(ENB, 90); //Right Motor Speed
        sharpRightTurn();
        positionreading();
      } while (error != 0);
    }
  } else if (error == 102) {        // Make left turn untill it detects straight path
    //Serial.print("\t");
    //Serial.println("Sharp Left Turn");
    do {
      analogWrite(ENA, 110); //Left Motor Speed
      analogWrite(ENB, 90); //Right Motor Speed
      sharpLeftTurn();
       positionreading();
      if (error == 0) {
        stop_bot();
        delay(200);
      }
    } while (error != 0);
  } else if (error == 103) {        // Make left turn untill it detects straight path or stop if dead end reached.
    if (flag == 0) {
      analogWrite(ENA, 110); //Left Motor Speed
      analogWrite(ENB, 90); //Right Motor Speed
      forward();
      delay(200);
      stop_bot();
       positionreading();
      if (error == 103) {     /**** Dead End Reached, Stop! ****/
        stop_bot();
        flag = 1;
      } else {        /**** Move Left ****/
        analogWrite(ENA, 110); //Left Motor Speed
        analogWrite(ENB, 90); //Right Motor Speed
        sharpLeftTurn();
        delay(200);
        do {
          //Serial.print("\t");
          //Serial.println("Left Here");
           positionreading();
          analogWrite(ENA, 110); //Left Motor Speed
          analogWrite(ENB, 90); //Right Motor Speed
          sharpLeftTurn();
        } while (error != 0);
      }
    }
  } else {
    calculate_pid();
    motor_control();
  }
} 
void calculate_pid()
{
  P = error;
  I = I + previous_I;
  D = error - previous_error;

  PID_value = (Kp * P) + (Ki * I) + (Kd * D);

  previous_I = I;
  previous_error = error;
}
void motor_control()
{
  // Calculating the effective motor speed:
  int left_motor_speed = initial_motor_speed - PID_value;
  int right_motor_speed = initial_motor_speed + PID_value;

  // The motor speed should not exceed the max PWM value
  left_motor_speed = constrain(left_motor_speed, 0, 255);
  right_motor_speed = constrain(right_motor_speed, 0, 255);

  /*Serial.print(PID_value);
    Serial.print("\t");
    Serial.print(left_motor_speed);
    Serial.print("\t");
    Serial.println(right_motor_speed);*/

  analogWrite(ENA, left_motor_speed); //Left Motor Speed
  analogWrite(ENB, right_motor_speed - 30); //Right Motor Speed

  //following lines of code are to make the bot move forward
  forward();
}
