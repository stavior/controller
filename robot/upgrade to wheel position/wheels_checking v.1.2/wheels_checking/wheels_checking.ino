#include <Wire.h>                      
#include <LiquidCrystal_I2C.h> 
#include <VL53L0X.h>
      
LiquidCrystal_I2C lcd(0x3F,20,4); 

//wheels pin configartion 
#define wheel1 2
#define wheel2 3

//wheels speed pin configartion 
#define speed1 4
#define speed2 5
//sensors pin configartion 
#define RIR 8
#define LIR 9
#define MIR 10

#define trigPin 22
#define echoPin 24

#define trigPin1 26
#define echoPin1 27

#define trigPin2 28
#define echoPin2 29

#define trigPin3 30
#define echoPin3 31

#define BREAK 13
#define UVLIGHT 25

//RC recevier pin configartion 
int channel_2 = 6;  // pin with ~ symbol
int channel_4 = 7;  // pin with ~ symbol
int channel_5 = 46;
int channel_6 = 45;

// Define variables: 
int pwm = 0;
int value1,value2,value3,value4;

long duration,duration1,duration2,duration3; 
int distance,distance1,distance2,distance3;
 
VL53L0X sensor;
int reading;

void setup() {  // put your setup code here, to run once: 
Serial.begin(115200);
lcd.init(); 
Wire.begin();
sensor.setTimeout(500);
  if (!sensor.init())
  {
    Serial.println("Failed to detect and initialize sensor!");
    while (1) {}
  }
sensor.startContinuous();
    
pinMode(wheel1,OUTPUT);
pinMode(wheel2,OUTPUT);
pinMode(speed1,OUTPUT);
pinMode(speed2,OUTPUT);
pinMode(BREAK,OUTPUT);
pinMode(RIR,INPUT);
pinMode(LIR,INPUT);
//pinMode(MIR,INPUT);
pinMode(UVLIGHT,OUTPUT);
pinMode(trigPin,OUTPUT);
pinMode(echoPin,INPUT);
pinMode(trigPin1,OUTPUT);
pinMode(echoPin1,INPUT);
pinMode(trigPin2,OUTPUT);
pinMode(echoPin2,INPUT);
pinMode(trigPin3,OUTPUT);
pinMode(echoPin3,INPUT);

pinMode(channel_2, INPUT);
pinMode(channel_4, INPUT);
pinMode(channel_5, INPUT);
pinMode(channel_6, INPUT);

digitalWrite(BREAK,HIGH);
analogWrite(speed1,0);
analogWrite(speed2,0);
digitalWrite(UVLIGHT,LOW);

  lcd.backlight();                
  lcd.setCursor(0,0);           
  lcd.print("WELCOME TO CENAURA");
  lcd.setCursor(3,1);             
  lcd.print("ROBOT STERILIZER!");
  lcd.setCursor(3,2);            
  lcd.print("VISIT OUR WEBSITE");
  lcd.setCursor(2,3);             
  lcd.print("www.cenaura.com");
  delay(2000); 
}
void loop() { // put your main code here, to run repeatedly: 

value3 = pulseIn(channel_5, HIGH);
Serial.print("channel_5 \t");
Serial.println(value3);

value4 = pulseIn(channel_6, HIGH);
Serial.print("channel_6 \t");
Serial.println(value4);

if(value3>=1400)
{
  Serial.println("uv light on");
  digitalWrite(UVLIGHT,HIGH);
}
else
{
  Serial.println("uv light off");
  digitalWrite(UVLIGHT,LOW);
} 

digitalWrite(trigPin,LOW); // Clear the trigPin by setting it LOW:
delayMicroseconds(5);
digitalWrite(trigPin, HIGH);
delayMicroseconds(10);// Trigger the sensor by setting the trigPin high for 10 microseconds:
digitalWrite(trigPin, LOW);
duration = pulseIn(echoPin, HIGH); // Read the echoPin. pulseIn() returns the duration (length of the pulse) in microseconds:
distance = duration*0.034/2; // Calculate the distance:
Serial.print("Distance = ");
Serial.print(distance);
Serial.println(" cm");


digitalWrite(trigPin1,LOW); // Clear the trigPin by setting it LOW:
delayMicroseconds(5);
digitalWrite(trigPin1, HIGH);
delayMicroseconds(10);// Trigger the sensor by setting the trigPin high for 10 microseconds:
digitalWrite(trigPin1, LOW);
duration1 = pulseIn(echoPin1, HIGH); // Read the echoPin. pulseIn() returns the duration (length of the pulse) in microseconds:
distance1 = duration1*0.034/2; // Calculate the distance:
Serial.print("Distance1 = ");
Serial.print(distance1);
Serial.println(" cm");



Serial.println(digitalRead(RIR));
Serial.println(digitalRead(LIR));
reading=sensor.readRangeContinuousMillimeters();
  Serial.print(reading);
  
if (sensor.timeoutOccurred()) { Serial.print(" TIMEOUT"); } 

if(((digitalRead(RIR)==0) || (digitalRead(LIR)==0) ||(reading<=40))||((distance>=22 && distance<30) || (distance1>=22 && distance1<30)))//
{ 
analogWrite(speed1,0);
analogWrite(speed2,0); 
digitalWrite(BREAK,LOW);

value1 = pulseIn(channel_2, HIGH);
Serial.print("channel_2 \t");
Serial.println(value1);

value2 = pulseIn(channel_4, HIGH);
Serial.print("channel_4 \t");
Serial.println(value2); 

if((value1 > 980) && (value1 < 1400))
{
Serial.println("BACKWARD");
digitalWrite(BREAK,HIGH);
digitalWrite(wheel1,LOW);
digitalWrite(wheel2,HIGH);
analogWrite(speed1,20);
analogWrite(speed2,20); 
}
if((value1 > 1400) && (value1 < 1500) && (value2 > 1400) && (value2 < 1500))
{ 
 digitalWrite(BREAK,LOW);
 analogWrite(speed1,0);
 analogWrite(speed2,0);  
}
}
else
{
 if(value4>900 && value4<1399)
{
  lcd.clear();
  lcd.setCursor(0,0);           
  lcd.print("variable speed drive");
  Serial.println("variable speed drive");
  robotdirection();
}  
if(value4>1500 && value4<2000)
{
  lcd.clear();
  lcd.setCursor(0,0);           
  lcd.print("constant speed drive");
  Serial.println("constatnt speed driving");
  uvmode(); 
}
}
}

void robotdirection()
{
value1 = pulseIn(channel_2, HIGH);
Serial.print("channel_2 \t");
Serial.println(value1);

value2 = pulseIn(channel_4, HIGH);
Serial.print("channel_4 \t");
Serial.println(value2);  
 
if((value1 > 1530) && (value1 < 2000))
{
Serial.println("BACKWARD");
pwm = map(value1, 1530, 2000, 10, 80); 
  lcd.clear();
  lcd.setCursor(0,0);           
  lcd.print("BACKWARD DIRECTION");
Serial.println("BACKWARD");
digitalWrite(BREAK,HIGH);
digitalWrite(wheel1,HIGH);
digitalWrite(wheel2,LOW);
analogWrite(speed1,pwm);
analogWrite(speed2,pwm);
}
if((value1 > 980) && (value1 < 1400))
{
   sensorreading();
Serial.println("FORDWARD");
pwm = map(value1, 1400, 1000, 10,80); 
  lcd.clear();
  lcd.setCursor(0,0);           
  lcd.print("FORWARD DIRECTION");
Serial.println("FORWARD");
digitalWrite(BREAK,HIGH);
digitalWrite(wheel1,LOW);
digitalWrite(wheel2,HIGH);
analogWrite(speed1,pwm);
analogWrite(speed2,pwm);
}
if((value2 > 1530) && (value2 < 2000))
{
Serial.println("RIGHT");
pwm = map(value2, 1530, 2000, 0, 20);
  lcd.clear();
  lcd.setCursor(0,0);           
  lcd.print("RIGHT DIRECTION");
Serial.println("RIGHT");
digitalWrite(BREAK,HIGH);
digitalWrite(wheel1,HIGH);
digitalWrite(wheel2,HIGH);
analogWrite(speed1,pwm);
analogWrite(speed2,pwm); 
delay(500);
digitalWrite(BREAK,LOW);
analogWrite(speed1,0);
analogWrite(speed2,0);  
}
if((value2 > 980) && (value2 < 1400))
{
Serial.println("LEFT");
pwm = map(value2, 1400, 1000, 0, 20);
  lcd.clear();
  lcd.setCursor(0,0);           
  lcd.print("LEFT DIRECTION");
Serial.println("LEFT");
digitalWrite(BREAK,HIGH);
digitalWrite(wheel1,LOW);
digitalWrite(wheel2,LOW);
analogWrite(speed1,pwm);
analogWrite(speed2,pwm);
delay(500);
digitalWrite(BREAK,LOW);
analogWrite(speed1,0);
analogWrite(speed2,0);  
}
if((value1 > 1400) && (value1 < 1500) && (value2 > 1400) && (value2 < 1500))
{
  lcd.clear();
  lcd.setCursor(0,0);           
  lcd.print("ROBOT STOP");
Serial.println("ROBOT STOP");
digitalWrite(BREAK,LOW);
analogWrite(speed1,0);
analogWrite(speed2,0);  
}
}
void uvmode()
{
value1 = pulseIn(channel_2, HIGH);
Serial.print("channel_2 \t");
Serial.println(value1);

value2 = pulseIn(channel_4, HIGH);
Serial.print("channel_4 \t");
Serial.println(value2);

if((value1 > 1530) && (value1 < 2000))
{
Serial.println("BACKWARD");
digitalWrite(BREAK,HIGH);
digitalWrite(wheel1,HIGH);
digitalWrite(wheel2,LOW);
analogWrite(speed1,20);
analogWrite(speed2,20);
}
if((value1 > 980) && (value1 < 1400))
{
  sensorreading();
Serial.println("FORDWARD");
digitalWrite(BREAK,HIGH);
 digitalWrite(wheel1,LOW);
digitalWrite(wheel2,HIGH);
analogWrite(speed1,20);
analogWrite(speed2,20); 
}
if((value2 > 1530) && (value2 < 2000))
{
Serial.println("RIGHT");
digitalWrite(BREAK,HIGH);
digitalWrite(wheel1,HIGH);
digitalWrite(wheel2,HIGH);
analogWrite(speed1,20);
analogWrite(speed2,20); 
delay(500);
digitalWrite(BREAK,LOW);
analogWrite(speed1,0);
analogWrite(speed2,0);  
}
if((value2 > 980) && (value2 < 1400))
{
Serial.println("LEFT");
digitalWrite(BREAK,HIGH);
digitalWrite(wheel1,LOW);
digitalWrite(wheel2,LOW);
analogWrite(speed1,20);
analogWrite(speed2,20); 
delay(500);
digitalWrite(BREAK,LOW);
analogWrite(speed1,0);
analogWrite(speed2,0);   
}
if((value1 > 1400) && (value1 < 1500) && (value2 > 1400) && (value2 < 1500))
{
digitalWrite(BREAK,LOW);
analogWrite(speed1,0);
analogWrite(speed2,0);
} 
}
void  sensorreading()
{
 digitalWrite(trigPin2,LOW); // Clear the trigPin by setting it LOW:
delayMicroseconds(5);
digitalWrite(trigPin2, HIGH);
delayMicroseconds(10);// Trigger the sensor by setting the trigPin high for 10 microseconds:
digitalWrite(trigPin2, LOW);
duration2 = pulseIn(echoPin2, HIGH); // Read the echoPin. pulseIn() returns the duration (length of the pulse) in microseconds:
distance2 = duration2*0.034/2; // Calculate the distance:
Serial.print("Distance2 = ");
Serial.print(distance2);
Serial.println(" cm");

digitalWrite(trigPin3,LOW); // Clear the trigPin by setting it LOW:
delayMicroseconds(5);
digitalWrite(trigPin3, HIGH);
delayMicroseconds(10);// Trigger the sensor by setting the trigPin high for 10 microseconds:
digitalWrite(trigPin3, LOW);
duration3 = pulseIn(echoPin3, HIGH); // Read the echoPin. pulseIn() returns the duration (length of the pulse) in microseconds:
distance3 = duration3*0.034/2; // Calculate the distance:
Serial.print("Distance3 = ");
Serial.print(distance3);
Serial.println(" cm");
if(((distance2>=22 && distance2<30) || (distance3>=22 && distance3<30)))
{
digitalWrite(BREAK,LOW);
analogWrite(speed1,0);
analogWrite(speed2,0);    
}
}
