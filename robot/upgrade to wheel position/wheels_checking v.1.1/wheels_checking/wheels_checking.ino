#include <Wire.h>                      
#include <LiquidCrystal_I2C.h>       
LiquidCrystal_I2C lcd(0x3F,20,4); 

//wheels pin configartion 
#define wheel1 2
#define wheel2 3

//wheels speed pin configartion 
#define speed1 4
#define speed2 5

#define trigPin 22
#define echoPin 24

#define trigPin1 26
#define echoPin1 27
#define MINI 12

#define BREAK 13

#define UVLIGHT 25

//RC recevier pin configartion 
int channel_2 = 6;  // pin with ~ symbol
int channel_4 = 7;  // pin with ~ symbol
int channel_5 = 46;
int channel_6 = 45;

// Define variables: 
int pwm = 0;
int value1,value2,value3,value4;
long duration; 
int distance;
long duration2; 
int distance2;

void isr()         
{
digitalWrite(BREAK,LOW);

value1 = pulseIn(channel_2, HIGH);
//Serial.print("channel_2 \t");
//Serial.println(value1);

value2 = pulseIn(channel_4, HIGH);
//Serial.print("channel_4 \t");
//Serial.println(value2); 
Serial.println("ISR");

if((value1 > 980) && (value1 < 1400))
{
Serial.println("BACKWARD");
lcd.clear();
lcd.setCursor(0,0);           
lcd.print("BACKWARD DIRECTION");
Serial.println("FORWARD");
digitalWrite(BREAK,HIGH);

digitalWrite(wheel1,LOW);
digitalWrite(wheel2,HIGH);
analogWrite(speed1,20);
analogWrite(speed2,20); 
}
if((value1 > 1400) && (value1 < 1500) && (value2 > 1400) && (value2 < 1500))
{ 
 digitalWrite(BREAK,LOW);
 analogWrite(speed1,0);
 analogWrite(speed2,0); 
}   
}
void uvon()
{
  Serial.println("uv light on");
  digitalWrite(UVLIGHT,HIGH);
}
void uvoff()
{
  Serial.println("uv light off");
  digitalWrite(UVLIGHT,LOW);
}


void setup() {  // put your setup code here, to run once: 
Serial.begin(115200);
lcd.init();
attachInterrupt(2,isr,RISING);
attachInterrupt(3,isr,RISING);
attachInterrupt(4,isr,RISING);
  
pinMode(wheel1,OUTPUT);
pinMode(wheel2,OUTPUT);
pinMode(speed1,OUTPUT);
pinMode(speed2,OUTPUT);
pinMode(BREAK,OUTPUT);

digitalWrite(BREAK,HIGH);

analogWrite(speed1,0);
analogWrite(speed2,0);

pinMode(UVLIGHT,OUTPUT);
digitalWrite(UVLIGHT,LOW);

pinMode(trigPin,OUTPUT);
pinMode(echoPin,INPUT);

pinMode(trigPin1,OUTPUT);
pinMode(echoPin1,INPUT);

pinMode(channel_2, INPUT);
pinMode(channel_4, INPUT);
pinMode(channel_5, INPUT);
pinMode(channel_6, INPUT);
  lcd.backlight();                
  lcd.setCursor(0,0);           
  lcd.print("WELCOME TO CENAURA");
  lcd.setCursor(3,1);             
  lcd.print("ROBOT STERILIZER!");
  lcd.setCursor(3,2);            
  lcd.print("VISIT OUR WEBSITE");
  lcd.setCursor(2,3);             
  lcd.print("www.cenaura.com");
  delay(2000); 
}
void loop() { // put your main code here, to run repeatedly: 

//attachInterrupt(2,isr,FALLING);
//attachInterrupt(3,isr,FALLING);
//attachInterrupt(4,isr,FALLING);

value3 = pulseIn(channel_5, HIGH);
Serial.print("channel_5 \t");
Serial.println(value3);

value4 = pulseIn(channel_6, HIGH);
Serial.print("channel_6 \t");
Serial.println(value4);
if(value3>=1400)
{
  uvon();
}
else
{
  uvoff();
} 

digitalWrite(trigPin,LOW); // Clear the trigPin by setting it LOW:
delayMicroseconds(5);

digitalWrite(trigPin, HIGH);
delayMicroseconds(10);// Trigger the sensor by setting the trigPin high for 10 microseconds:
digitalWrite(trigPin, LOW);
duration = pulseIn(echoPin, HIGH); // Read the echoPin. pulseIn() returns the duration (length of the pulse) in microseconds:
distance = duration*0.034/2; // Calculate the distance:
Serial.print("Distance = ");
Serial.print(distance);
Serial.println(" cm");

digitalWrite(trigPin1,LOW); // Clear the trigPin by setting it LOW:
delayMicroseconds(5);

digitalWrite(trigPin1, HIGH);
delayMicroseconds(10);// Trigger the sensor by setting the trigPin high for 10 microseconds:
digitalWrite(trigPin1, LOW);

duration2 = pulseIn(echoPin1, HIGH); // Read the echoPin. pulseIn() returns the duration (length of the pulse) in microseconds:
distance2 = duration2*0.034/2; // Calculate the distance:
Serial.print("Distance2 = ");
Serial.print(distance2);
Serial.println(" cm");

if(value4>900 && value4<1399)
{
  lcd.clear();
  lcd.setCursor(0,0);           
  lcd.print("variable speed drive");
  Serial.println("variable speed drive");
  robotdirection();
}  
if(value4>1500 && value4<2000)
{
  lcd.clear();
  lcd.setCursor(0,0);           
  lcd.print("constant speed drive");
  Serial.println("constatnt speed driving");
  uvmode(); 
}
}

void robotdirection()
{
value1 = pulseIn(channel_2, HIGH);
Serial.print("channel_2 \t");
Serial.println(value1);

value2 = pulseIn(channel_4, HIGH);
Serial.print("channel_4 \t");
Serial.println(value2);  
 
if((value1 > 1530) && (value1 < 2000))
{
Serial.println("BACKWARD");
pwm = map(value1, 1530, 2000, 10, 80); 
  lcd.clear();
  lcd.setCursor(0,0);           
  lcd.print("BACKWARD DIRECTION");
Serial.println("BACKWARD");
digitalWrite(BREAK,HIGH);
digitalWrite(wheel1,HIGH);
digitalWrite(wheel2,LOW);
analogWrite(speed1,pwm);
analogWrite(speed2,pwm);
}
else if((value1 > 980) && (value1 < 1400))
{
Serial.println("FORDWARD");
pwm = map(value1, 1400, 1000, 10,80); 
  lcd.clear();
  lcd.setCursor(0,0);           
  lcd.print("FORWARD DIRECTION");
Serial.println("FORWARD");
digitalWrite(BREAK,HIGH);
digitalWrite(wheel1,LOW);
digitalWrite(wheel2,HIGH);
analogWrite(speed1,pwm);
analogWrite(speed2,pwm); 
}
if((value2 > 1530) && (value2 < 2000))
{
Serial.println("RIGHT");
pwm = map(value2, 1530, 2000, 0, 20);
lcd.clear();
  lcd.setCursor(0,0);           
  lcd.print("RIGHT DIRECTION");
Serial.println("RIGHT");
digitalWrite(BREAK,HIGH);
digitalWrite(wheel1,HIGH);
digitalWrite(wheel2,HIGH);
analogWrite(speed1,pwm);
analogWrite(speed2,pwm); 
delay(500);
digitalWrite(BREAK,LOW);
analogWrite(speed1,0);
analogWrite(speed2,0);
}
else if((value2 > 980) && (value2 < 1400))
{
Serial.println("LEFT");
pwm = map(value2, 1400, 1000, 0, 20);
lcd.clear();
  lcd.setCursor(0,0);           
  lcd.print("LEFT DIRECTION");
Serial.println("LEFT");
digitalWrite(BREAK,HIGH);
digitalWrite(wheel1,LOW);
digitalWrite(wheel2,LOW);
analogWrite(speed1,pwm);
analogWrite(speed2,pwm);
delay(500);
digitalWrite(BREAK,LOW);
analogWrite(speed1,0);
analogWrite(speed2,0); 
}
if((value1 > 1400) && (value1 < 1500) && (value2 > 1400) && (value2 < 1500))
{
  lcd.clear();
  lcd.setCursor(0,0);           
  lcd.print("ROBOT STOP");
Serial.println("ROBOT STOP");
digitalWrite(BREAK,LOW);
analogWrite(speed1,0);
analogWrite(speed2,0);
}
}
void uvmode()
{
value1 = pulseIn(channel_2, HIGH);
Serial.print("channel_2 \t");
Serial.println(value1);

value2 = pulseIn(channel_4, HIGH);
Serial.print("channel_4 \t");
Serial.println(value2);

if((value1 > 1530) && (value1 < 2000))
{
Serial.println("BACKWARD");
digitalWrite(BREAK,HIGH);
digitalWrite(wheel1,HIGH);
digitalWrite(wheel2,LOW);
analogWrite(speed1,20);
analogWrite(speed2,20);

}
if((value1 > 980) && (value1 < 1400))
{
Serial.println("FORDWARD");
digitalWrite(BREAK,HIGH);
 digitalWrite(wheel1,LOW);
digitalWrite(wheel2,HIGH);
analogWrite(speed1,20);
analogWrite(speed2,20); 
}
if((value2 > 1530) && (value2 < 2000))
{
Serial.println("RIGHT");
digitalWrite(BREAK,HIGH);
digitalWrite(wheel1,HIGH);
digitalWrite(wheel2,HIGH);
analogWrite(speed1,20);
analogWrite(speed2,20); 
delay(500);
digitalWrite(BREAK,LOW);
analogWrite(speed1,0);
analogWrite(speed2,0);  
}
if((value2 > 980) && (value2 < 1400))
{
Serial.println("LEFT");
digitalWrite(BREAK,HIGH);
digitalWrite(wheel1,LOW);
digitalWrite(wheel2,LOW);
analogWrite(speed1,20);
analogWrite(speed2,20); 
delay(500);
digitalWrite(BREAK,LOW);
analogWrite(speed1,0);
analogWrite(speed2,0);   
}
if((value1 > 1400) && (value1 < 1500) && (value2 > 1400) && (value2 < 1500))
{
digitalWrite(BREAK,LOW);
analogWrite(speed1,0);
analogWrite(speed2,0);
} 
}
