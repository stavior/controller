#include <QTRSensors.h>

// This example is designed for use with eight RC QTR sensors. These
// reflectance sensors should be connected to digital pins 3 to 10. The
// sensors' emitter control pin (CTRL or LEDON) can optionally be connected to
// digital pin 2, or you can leave it disconnected and remove the call to
// setEmitterPin().
//
// The main loop of the example reads the raw sensor values (uncalibrated). You
// can test this by taping a piece of 3/4" black electrical tape to a piece of
// white paper and sliding the sensor across it. It prints the sensor values to
// the serial monitor as numbers from 0 (maximum reflectance) to 2500 (minimum
// reflectance; this is the default RC timeout, which can be changed with
// setTimeout()).

QTRSensors qtr;

const uint8_t SensorCount = 8;
uint16_t sensorValues[SensorCount];
void setup()
{
  // configure the sensors
  qtr.setTypeRC();
  qtr.setSensorPins((const uint8_t[]){3, 4, 5, 6, 7, 8, 9, 10}, SensorCount);
//  qtr.setEmitterPin(2);

  Serial.begin(9600);
}

void loop()
{
  // read raw sensor values
  qtr.read(sensorValues);

  // print the sensor values as numbers from 0 to 2500, where 0 means maximum
  // reflectance and 2500 means minimum reflectance
//  for (uint8_t i = 0; i < SensorCount; i++)
//  {
//    Serial.print(sensorValues[i]);
//    Serial.print('\t');
//  }
  Serial.print("IR1:");
  Serial.print(sensorValues[0]);
  Serial.print('\t');
  Serial.print("IR2:");
  Serial.print(sensorValues[1]);
  Serial.print('\t');
  Serial.print("IR3:");
  Serial.print(sensorValues[2]);
  Serial.print('\t');
  Serial.print("IR4:");
  Serial.print(sensorValues[3]);
  Serial.print('\t');
  Serial.print("IR5:");
  Serial.print(sensorValues[4]);
  Serial.print('\t');
  Serial.print("IR6:");
  Serial.print(sensorValues[5]);
  Serial.print('\t');
  Serial.print("IR7:");
  Serial.print(sensorValues[6]);
  Serial.print('\t');
  Serial.print("IR8:");
  Serial.print(sensorValues[7]);
  Serial.println();
if(sensorValues[0]<500 && sensorValues[1]<500 && sensorValues[2]<500 && sensorValues[3]>1800 && sensorValues[4]>1800 && sensorValues[5]<500 && sensorValues[6]<500 && sensorValues[7]<500) 
{
  Serial.println("FORWARD");
}
else if(sensorValues[0]<500 && sensorValues[1]<500 && sensorValues[2]<500 && sensorValues[3]<500 && sensorValues[4]<500 && sensorValues[5]>1800 && sensorValues[6]>1800 && sensorValues[7]<500) 
{
  Serial.println("RIGHT");
}
else if(sensorValues[0]<500 && sensorValues[1]<500 && sensorValues[2]<500 && sensorValues[3]<500 && sensorValues[4]<500 && sensorValues[5]<500 && sensorValues[6]>1800 && sensorValues[7]>1800) 
{
  Serial.println("RIGHT");
}
else if(sensorValues[0]<500 && sensorValues[1]>1800 && sensorValues[2]>1800 && sensorValues[3]<500 && sensorValues[4]<500 && sensorValues[5]<500 && sensorValues[6]<500 && sensorValues[7]<500) 
{
  Serial.println("LEFT");
}
else if(sensorValues[0]>1800 && sensorValues[1]>1800 && sensorValues[2]>1800 && sensorValues[3]<500 && sensorValues[4]<500 && sensorValues[5]<500 && sensorValues[6]<500 && sensorValues[7]<500) 
{
  Serial.println("LEFT");
}
//else
//{
// Serial.println("STOP"); 
//}
  delay(250);
}
