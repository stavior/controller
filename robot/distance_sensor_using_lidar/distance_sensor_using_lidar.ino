
#include <Wire.h>
#include <VL53L0X.h>

VL53L0X sensor;

int reading;
void setup()
{
  Serial.begin(9600);
  Wire.begin();

  sensor.setTimeout(500);
  if (!sensor.init())
  {
    Serial.println("Failed to detect and initialize sensor!");
    while (1) {}
  }

  // Start continuous back-to-back mode (take readings as
  // fast as possible).  To use continuous timed mode
  // instead, provide a desired inter-measurement period in
  // ms (e.g. sensor.startContinuous(100)).
  sensor.startContinuous();
}

void loop()
{
  reading=sensor.readRangeContinuousMillimeters();
  Serial.print(reading);
  if(reading<=40)
    Serial.println("Obtect nera by robot");
  else
    Serial.println("Obtect far by robot");
    
  if (sensor.timeoutOccurred()) { Serial.print(" TIMEOUT"); }

  Serial.println();
  delay(1000);
}
