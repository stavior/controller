#include <DFRobot_TFmini.h>
SoftwareSerial mySerial(12, 13); // RX, TX
DFRobot_TFmini  TFmini;
#define OUTPIN 3
uint16_t distance1,strength;
void setup() {
  // put your setup code here, to run once:
Serial.begin(115200);
TFmini.begin(mySerial);
pinMode(OUTPIN,OUTPUT);
digitalWrite(OUTPIN,HIGH);
}

void loop() {
  // put your main code here, to run repeatedly:
if(TFmini.measure()){                      //Measure Distance and get signal strength
        distance1 = TFmini.getDistance();       //Get distance data
        strength = TFmini.getStrength();       //Get signal strength data
        Serial.print("TFMINI DISTNCE = ");
        Serial.print(distance1);
        Serial.println("cm");
        Serial.print("Strength = ");
        Serial.println(strength);
        delay(500);
    }
if(distance1>0 && distance1<40)
{
  digitalWrite(OUTPIN,LOW);
  //Serial.println("objectdetected");
}
else
{
 digitalWrite(OUTPIN,HIGH);
  //Serial.println("object not detected"); 
}

}
