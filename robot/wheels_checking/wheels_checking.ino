#include <Wire.h>                      
#include <LiquidCrystal_I2C.h>       
LiquidCrystal_I2C lcd(0x3F,20,4); 

//wheels pin configartion 
#define wheel1 2
#define wheel2 3

//wheels speed pin configartion 
#define speed1 4
#define speed2 5

//sensors pin configartion 
#define RIR 8
#define LIR 9
#define MIR 10

#define trigPin 22
#define echoPin 24

#define trigPin1 26
#define echoPin1 27

#define trigPin2 28
#define echoPin2 29

#define trigPin3 30
#define echoPin3 31

#define MINI 12
#define BREAK 13

#define UVLIGHT 25

//RC recevier pin configartion 
int channel_2 = 6;  // pin with ~ symbol
int channel_4 = 7;  // pin with ~ symbol
int channel_5 = 46;
int channel_6 = 45;

// Define variables: 
int pwm = 0;
int value1,value2,value3,value4;
long duration; 
int distance;
long duration1; 
int distance1;
long duration2; 
int distance2;
long duration3; 
int distance3;

void setup() {  // put your setup code here, to run once: 
Serial.begin(115200);
lcd.init();  
pinMode(wheel1,OUTPUT);
pinMode(wheel2,OUTPUT);
pinMode(speed1,OUTPUT);
pinMode(speed2,OUTPUT);
pinMode(BREAK,OUTPUT);
digitalWrite(BREAK,HIGH);

analogWrite(speed1,0);
analogWrite(speed2,0);

pinMode(RIR,INPUT);
pinMode(LIR,INPUT);
//pinMode(MIR,INPUT);
pinMode(MINI,INPUT);

pinMode(UVLIGHT,OUTPUT);

digitalWrite(UVLIGHT,LOW);

pinMode(trigPin,OUTPUT);
pinMode(echoPin,INPUT);

pinMode(trigPin1,OUTPUT);
pinMode(echoPin1,INPUT);

pinMode(trigPin2,OUTPUT);
pinMode(echoPin2,INPUT);

pinMode(trigPin3,OUTPUT);
pinMode(echoPin3,INPUT);

pinMode(channel_2, INPUT);
pinMode(channel_4, INPUT);
pinMode(channel_5, INPUT);
pinMode(channel_6, INPUT);
  lcd.backlight();                
  lcd.setCursor(0,0);           
  lcd.print("WELCOME TO CENAURA");
  lcd.setCursor(3,1);             
  lcd.print("ROBOT STERILIZER!");
  lcd.setCursor(3,2);            
  lcd.print("VISIT OUR WEBSITE");
  lcd.setCursor(2,3);             
  lcd.print("www.cenaura.com");
  delay(2000); 
}
void loop() { // put your main code here, to run repeatedly: 

value3 = pulseIn(channel_5, HIGH);
Serial.print("channel_5 \t");
Serial.println(value3);

value4 = pulseIn(channel_6, HIGH);
Serial.print("channel_6 \t");
Serial.println(value4);
if(value3>=1400)
{
  uvon();
}
else
{
  uvoff();
}
 




Serial.println(digitalRead(RIR));
Serial.println(digitalRead(LIR));
Serial.println(digitalRead(MINI)); 
 
if(((digitalRead(RIR)==0) || (digitalRead(LIR)==0) ||(digitalRead(MINI)== 0))||((distance>=22 && distance<30) || (distance2>=22 && distance2<30)))//
{ 
sen();
}
else
{
 if(value4>900 && value4<1399)
{
  lcd.clear();
  lcd.setCursor(0,0);           
  lcd.print("variable speed drive");
  Serial.println("variable speed drive");
  robotdirection();
}  
if(value4>1500 && value4<2000)
{
  lcd.clear();
  lcd.setCursor(0,0);           
  lcd.print("constant speed drive");
  Serial.println("constatnt speed driving");
  uvmode(); 
}
}
}

void robotdirection()
{
value1 = pulseIn(channel_2, HIGH);
Serial.print("channel_2 \t");
Serial.println(value1);

value2 = pulseIn(channel_4, HIGH);
Serial.print("channel_4 \t");
Serial.println(value2);  
 
if((value1 > 1530) && (value1 < 2000))
{
Serial.println("BACKWARD");
pwm = map(value1, 1530, 2000, 10, 80); 
backward();
}
else if((value1 > 980) && (value1 < 1400))
{
Serial.println("FORDWARD");
pwm = map(value1, 1400, 1000, 10,80); 
forward();
}
if((value2 > 1530) && (value2 < 2000))
{
Serial.println("RIGHT");
pwm = map(value2, 1530, 2000, 0, 20);
right();
}
else if((value2 > 980) && (value2 < 1400))
{
Serial.println("LEFT");
pwm = map(value2, 1400, 1000, 0, 20);
left();  
}
if((value1 > 1400) && (value1 < 1500) && (value2 > 1400) && (value2 < 1500))
{
 stop1(); 
}
}
void sen()
{
digitalWrite(BREAK,LOW);
stop1();

value1 = pulseIn(channel_2, HIGH);
Serial.print("channel_2 \t");
Serial.println(value1);

value2 = pulseIn(channel_4, HIGH);
Serial.print("channel_4 \t");
Serial.println(value2); 
if((value1 > 980) && (value1 < 1400))
{
Serial.println("BACKWARD");
pwm = map(value1, 1400, 1000, 10,80); 
forward();
}
if((value1 > 1400) && (value1 < 1500) && (value2 > 1400) && (value2 < 1500))
{ 
 digitalWrite(BREAK,LOW);
 stop1();
}   
}
void forward()
{
  backsensorchecking();
  lcd.clear();
  lcd.setCursor(0,0);           
  lcd.print("FORWARD DIRECTION");
Serial.println("FORWARD");
digitalWrite(BREAK,HIGH);
digitalWrite(wheel1,LOW);
digitalWrite(wheel2,HIGH);
analogWrite(speed1,pwm);
analogWrite(speed2,pwm);  
}
void backward()
{
  frontsensorchecking();
  lcd.clear();
  lcd.setCursor(0,0);           
  lcd.print("BACKWARD DIRECTION");
Serial.println("BACKWARD");
digitalWrite(BREAK,HIGH);
digitalWrite(wheel1,HIGH);
digitalWrite(wheel2,LOW);
analogWrite(speed1,pwm);
analogWrite(speed2,pwm); 
}
void right()
{
  lcd.clear();
  lcd.setCursor(0,0);           
  lcd.print("RIGHT DIRECTION");
Serial.println("RIGHT");
digitalWrite(BREAK,HIGH);
digitalWrite(wheel1,HIGH);
digitalWrite(wheel2,HIGH);
analogWrite(speed1,pwm);
analogWrite(speed2,pwm); 
delay(500);
digitalWrite(BREAK,LOW);
analogWrite(speed1,0);
analogWrite(speed2,0);  
}
void left()
{
  lcd.clear();
  lcd.setCursor(0,0);           
  lcd.print("LEFT DIRECTION");
Serial.println("LEFT");
digitalWrite(BREAK,HIGH);
digitalWrite(wheel1,LOW);
digitalWrite(wheel2,LOW);
analogWrite(speed1,pwm);
analogWrite(speed2,pwm);
delay(500);
digitalWrite(BREAK,LOW);
analogWrite(speed1,0);
analogWrite(speed2,0);  
}
void stop1()
{
  lcd.clear();
  lcd.setCursor(0,0);           
  lcd.print("ROBOT STOP");
Serial.println("ROBOT STOP");
digitalWrite(BREAK,LOW);
analogWrite(speed1,0);
analogWrite(speed2,0);  
}
void uvon()
{
  Serial.println("uv light on");
  digitalWrite(UVLIGHT,HIGH);
}
void uvoff()
{
  Serial.println("uv light off");
  digitalWrite(UVLIGHT,LOW);
}
void uvmode()
{
value1 = pulseIn(channel_2, HIGH);
Serial.print("channel_2 \t");
Serial.println(value1);

value2 = pulseIn(channel_4, HIGH);
Serial.print("channel_4 \t");
Serial.println(value2);

if((value1 > 1530) && (value1 < 2000))
{
  frontsensorchecking();
Serial.println("BACKWARD");
digitalWrite(BREAK,HIGH);
digitalWrite(wheel1,HIGH);
digitalWrite(wheel2,LOW);
analogWrite(speed1,20);
analogWrite(speed2,20);

}
if((value1 > 980) && (value1 < 1400))
{
  backsensorchecking();
Serial.println("FORDWARD");
digitalWrite(BREAK,HIGH);
 digitalWrite(wheel1,LOW);
digitalWrite(wheel2,HIGH);
analogWrite(speed1,20);
analogWrite(speed2,20); 
}
if((value2 > 1530) && (value2 < 2000))
{
Serial.println("RIGHT");
digitalWrite(BREAK,HIGH);
digitalWrite(wheel1,HIGH);
digitalWrite(wheel2,HIGH);
analogWrite(speed1,20);
analogWrite(speed2,20); 
delay(500);
digitalWrite(BREAK,LOW);
analogWrite(speed1,0);
analogWrite(speed2,0);  
}
if((value2 > 980) && (value2 < 1400))
{
Serial.println("LEFT");
digitalWrite(BREAK,HIGH);
digitalWrite(wheel1,LOW);
digitalWrite(wheel2,LOW);
analogWrite(speed1,20);
analogWrite(speed2,20); 
delay(500);
digitalWrite(BREAK,LOW);
analogWrite(speed1,0);
analogWrite(speed2,0);   
}
if((value1 > 1400) && (value1 < 1500) && (value2 > 1400) && (value2 < 1500))
{
digitalWrite(BREAK,LOW);
analogWrite(speed1,0);
analogWrite(speed2,0);
} 
}
void frontsensorchecking()
{
  digitalWrite(trigPin,LOW); // Clear the trigPin by setting it LOW:
delayMicroseconds(5);
digitalWrite(trigPin, HIGH);
delayMicroseconds(10);// Trigger the sensor by setting the trigPin high for 10 microseconds:
digitalWrite(trigPin, LOW);
duration = pulseIn(echoPin, HIGH); // Read the echoPin. pulseIn() returns the duration (length of the pulse) in microseconds:
distance = duration*0.034/2; // Calculate the distance:
Serial.print("Distance = ");
Serial.print(distance);
Serial.println(" cm");

digitalWrite(trigPin1,LOW); // Clear the trigPin by setting it LOW:
delayMicroseconds(5);
digitalWrite(trigPin1, HIGH);
delayMicroseconds(10);// Trigger the sensor by setting the trigPin high for 10 microseconds:
digitalWrite(trigPin1, LOW);
duration1 = pulseIn(echoPin1, HIGH); // Read the echoPin. pulseIn() returns the duration (length of the pulse) in microseconds:
distance1 = duration1*0.034/2; // Calculate the distance:
Serial.print("Distance2 = ");
Serial.print(distance1);
Serial.println(" cm");
}
void backsensorchecking()
{
 digitalWrite(trigPin2,LOW); // Clear the trigPin by setting it LOW:
delayMicroseconds(5);
digitalWrite(trigPin2, HIGH);
delayMicroseconds(10);// Trigger the sensor by setting the trigPin high for 10 microseconds:
digitalWrite(trigPin2, LOW);
duration2 = pulseIn(echoPin2, HIGH); // Read the echoPin. pulseIn() returns the duration (length of the pulse) in microseconds:
distance2 = duration2*0.034/2; // Calculate the distance:
Serial.print("Distance 3 = ");
Serial.print(distance2);
Serial.println(" cm");

digitalWrite(trigPin3,LOW); // Clear the trigPin by setting it LOW:
delayMicroseconds(5);
digitalWrite(trigPin3, HIGH);
delayMicroseconds(10);// Trigger the sensor by setting the trigPin high for 10 microseconds:
digitalWrite(trigPin3, LOW);
duration3 = pulseIn(echoPin3, HIGH); // Read the echoPin. pulseIn() returns the duration (length of the pulse) in microseconds:
distance3 = duration3*0.034/2; // Calculate the distance:
Serial.print("Distance 4 = ");
Serial.print(distance3);
Serial.println(" cm"); 
}
