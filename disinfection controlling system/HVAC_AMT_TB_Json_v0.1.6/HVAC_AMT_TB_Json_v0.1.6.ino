/*/
 * HVAC Arduino MEGA with ESP8266 Board Code... 
 * Serial communication to esp8266
 * ArduinoJson
 * 
 * 
 */

#include <Arduino.h>
#include <menu.h>
#include <menuIO/u8g2Out.h>
#include <menuIO/encoderIn.h>
#include <menuIO/keyIn.h>
#include <menuIO/chainStream.h>
#include <menuIO/serialOut.h>
#include <menuIO/serialIn.h>
#include <menuIO/U8GLibOut.h>
#include <TimerOne.h>
#include <ClickEncoder.h>
#include <menuIO/clickEncoderIn.h>
#include <MemoryFree.h>
#include <TaskScheduler.h>
#include <ArduinoJson.h>
#include <RTClib.h>
#include <RTClib.h>
#include <Time.h>
#include <TimeLib.h>

char t[32];
#include "Countimer.h"

int totallife=9000;

using namespace Menu;

String sta1="OFF",sta2="OFF",sta3="OFF",sta4="OFF",sta5="OFF",sta6="OFF",sta7="OFF",sta8="OFF";
byte s1=0,s2=0,s3=0,s4=0,s5=0,s6=0,s7=0,s8=0;
byte ds=0;//
String modestatus="MANUAL";
String doorstatus="DISABLE";
#define LEDPIN LED_BUILTIN

// rotary encoder pins
#define encA    33   //31
#define encB    31   //33
#define encBtn  35   //35

#define rled 22
#define gled 24
#define bled 26

//EPROM
#include <EEPROMex.h>

//Define
#define BUZZER 37
#define doorsen 10
int doorreading;
//RELAY
#define UVLIGHT1 9  //PUMPA 3 
#define UVLIGHT2 8  //PUMPB 2
#define UVLIGHT3 7 //AIRPUMP  18 //14
#define UVLIGHT4 6 //SOLVALVE 19 //15
#define UVLIGHT5 5 //LEDRED 42
#define UVLIGHT6 4 //LEDGREEN 44
#define UVLIGHT7 3 //LEDBLUE 58
#define UVLIGHT8 2  //LEDBLUE 58


float  voltage1;
float current1;
float  voltage2;
float current2;

float  voltage3;
float current3;
float  voltage4;
float current4;

float CS;

#define DEBUG 1                                // change value to 1 to enable debuging using serial monitor  
#define SERIAL_PORT_SPEED 115200                //9600, 19200, 38400, 57600, 115200
int counter1on=0,counter1off=0,counter2on=0,counter2off=0,counter3on=0,counter3off=0,counter4on=0,counter4off=0,counter5on=0,counter5off=0,counter6on=0,counter6off=0,counter7on=0,counter7off=0,counter8on=0,counter8off=0;
int counter1,counter2,counter3,counter4,counter5,counter6,counter7,counter8;
//****************************************** Water Level US Sensor **************************************************************
float offset = 245;
//***************************cuurent sesnor**************************************************************************************
#define VIN0 A0 // define the Arduino pin A0 as voltage input (V in)
#define VIN1 A1 // define the Arduino pin A0 as voltage input (V in)
#define VIN2 A2 // define the Arduino pin A0 as voltage input (V in)
#define VIN3 A3 // define the Arduino pin A0 as voltage input (V in)
#define VIN4 A4 // define the Arduino pin A0 as voltage input (V in)
#define VIN5 A5 // define the Arduino pin A0 as voltage input (V in)
#define VIN6 A6 // define the Arduino pin A0 as voltage input (V in)
#define VIN7 A7 // define the Arduino pin A0 as voltage input (V in)
const float VCC   = 5.0;// supply voltage is from 4.5 to 5.5V. Normally 5V.
const int model = 2;   // enter the model number (see below)

float cutOffLimit = 1.01;// set the current which below that value, doesn't matter. Or set 0.5

/*
          "ACS712ELCTR-05B-T",// for model use 0
          "ACS712ELCTR-20A-T",// for model use 1
          "ACS712ELCTR-30A-T"// for model use 2  
sensitivity array is holding the sensitivy of the  ACS712
current sensors. Do not change. All values are from page 5  of data sheet          
*/
float sensitivity[] ={
          0.185,// for ACS712ELCTR-05B-T
          0.100,// for ACS712ELCTR-20A-T
          0.066// for ACS712ELCTR-30A-T
     
         }; 


const float QOV =   0.5 * VCC;// set quiescent Output voltage of 0.5V
float voltage;// internal variable for voltage

//********************UVC ALERT**********************
//********************************** ENCODER*******************
ClickEncoder clickEncoder(encA, encB, encBtn, 2);
ClickEncoderStream encStream(clickEncoder, 1);
MENU_INPUTS(in, &encStream);
void timerIsr() {
  clickEncoder.service();
}
//********************Display Configuration **************************
#include <SPI.h>
#define USE_SWSPI
#define U8_DC 17
#define U8_CS 16
#define U8_RST 23
#define fontName  u8g2_font_5x7_tf // u8g2_font_synchronizer_nbp_tf           
#define offsetX 0
#define offsetY 0
#define U8_Width 128
#define U8_Height 64
int x = 0;
int scroll_direction = 1;               //Direction of title scroll, 1 right, -1 left//Offset postion of title

U8G2_ST7920_128X64_F_SW_SPI u8g2(U8G2_R0, /* clock=*/ 23 /* A4 */ , /* data=*/ 17 /* A2 */, /* CS=*/ 16 /* A3 */, /* reset=*/ U8X8_PIN_NONE);

RTC_DS3231 rtc; //for date time
char daysOfTheWeek[7][12] = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};
int hourupg;
int minupg;
uint8_t secslive;
int Monthlive;
int Datelive;
int Yearlive;
int lamp1on,lamp2on,lamp3on,lamp4on,lamp5on,lamp6on,lamp7on,lamp8on;
int lamp1off,lamp2off,lamp3off,lamp4off,lamp5off,lamp6off,lamp7off,lamp8off;
// define menu colors --------------------------------------------------------
//each color is in the format:
//  {{disabled normal,disabled selected},{enabled normal,enabled selected, enabled editing}}
// this is a monochromatic color table
const colorDef<uint8_t> colors[6] MEMMODE = {
  {{0, 0}, {0, 1, 1}}, //bgColor
  {{1, 1}, {1, 0, 0}}, //fgColor
  {{1, 1}, {1, 0, 0}}, //valColor
  {{1, 1}, {1, 0, 0}}, //unitColor
  {{0, 1}, {0, 0, 1}}, //cursorColor
  {{1, 1}, {1, 0, 0}}, //titleColor
};
// *********************************************************************
// Task Scheduler
// *********************************************************************
// objects
Scheduler r;

// Callback methods prototypes
void Task_Serial_Blink_Example();
void dateandtime();
void sending();
// Tasks
Task t0(1000,  TASK_FOREVER, &dateandtime, &r);
//Task TBdata(4000,  TASK_FOREVER, &senddata, &r);
//Task TBdata1(6000,  TASK_FOREVER, &senddata1, &r);
Task TBdata2(40000,  TASK_FOREVER, &Room2data, &r);
Task TBdata3(80000,  TASK_FOREVER, &Room3data, &r);

result doAlert(eventMask e, prompt &item);
result showEvent(eventMask e, navNode& nav, prompt& item) {
  Serial.print("event: ");
  Serial.println(e);
  return proceed;
}
result action1(eventMask e, navNode& nav, prompt &item) {
  Serial.print(e);
  Serial.println(" action1 executed, proceed menu");
  Serial.flush();
  return proceed;
}
result action2(eventMask e, navNode& nav, prompt &item) {
  Serial.print(e);
  Serial.print(" action2 executed, quiting menu");
  return quit;
}
///**********************Relay Control******************************
//********************relay 1**************
int teston2;
int Eepromautostatus2 =304;
bool relay1=teston2;
void autosavefun2(){
  if (teston2==1){
    relay1=true;
    }
    else{
      relay1=false;
      }
  }
TOGGLE(relay1, L1, "     MODULE 1 : ", doNothing, noEvent, noStyle
       , VALUE("On", HIGH, autosaveon2, noEvent)
       , VALUE("Off", LOW, autosaveon2, noEvent)
      );

result autosaveon2(){
  EEPROM.write(Eepromautostatus2, relay1);
  }

void EepromRead2()
{
  teston2 = EEPROM.read(Eepromautostatus2);
 Serial.print ("on");
  Serial.println (teston2);
  return proceed;
}
//********************relay 2**************
int teston3;
int Eepromautostatus3 =306;
bool relay2=teston3;
void autosavefun3(){
  if (teston3==1){
    relay2=true;
    }
    else{
      relay2=false;
      }
  }
TOGGLE(relay2, L2, "     MODULE 2 : ", doNothing, noEvent, noStyle
       , VALUE("On", HIGH, autosaveon3, noEvent)
       , VALUE("Off", LOW, autosaveon3, noEvent)
      );

result autosaveon3(){
  EEPROM.write(Eepromautostatus3, relay2);
  }

void EepromRead3()
{
  teston3 = EEPROM.read(Eepromautostatus3);
 Serial.print ("on");
  Serial.println (teston3);
  return proceed;
}
      
//********************relay 3**************
int teston4;
int Eepromautostatus4 =308;
bool relay3=teston4;
void autosavefun4(){
  if (teston4==1){
    relay3=true;
    }
    else{
      relay3=false;
      }
  }
TOGGLE(relay3, L3, "     MODULE 3 : ", doNothing, noEvent, noStyle
       , VALUE("On", HIGH, autosaveon4, noEvent)
       , VALUE("Off", LOW, autosaveon4, noEvent)
      );

result autosaveon4(){
  EEPROM.write(Eepromautostatus4, relay3);
  }

void EepromRead4()
{
  teston4 = EEPROM.read(Eepromautostatus4);
 Serial.print ("on");
  Serial.println (teston4);
  return proceed;
}

//********************relay 4**************
int teston5;
int Eepromautostatus5 =310;
bool relay4=teston5;
void autosavefun5(){
  if (teston5==1){
    relay4=true;
    }
    else{
      relay4=false;
      }
  }
TOGGLE(relay4, L4, "     MODULE 4 : ", doNothing, noEvent, noStyle
       , VALUE("On", HIGH, autosaveon5, noEvent)
       , VALUE("Off", LOW, autosaveon5, noEvent)
      );

result autosaveon5(){
  EEPROM.write(Eepromautostatus5, relay4);
  }

void EepromRead5()
{
  teston5 = EEPROM.read(Eepromautostatus5);
 Serial.print ("on");
  Serial.println (teston5);
  return proceed;
}
      
//********************relay 5**************
int teston6;
int Eepromautostatus6 =314;
bool relay5=teston6;
void autosavefun6(){
  if (teston6==1){
    relay5=true;
    }
    else{
      relay5=false;
      }
  }
TOGGLE(relay5, L5, "     MODULE 5 : ", doNothing, noEvent, noStyle
       , VALUE("On", HIGH, autosaveon6, noEvent)
       , VALUE("Off", LOW, autosaveon6, noEvent)
      );

result autosaveon6(){
  EEPROM.write(Eepromautostatus6, relay5);
  }

void EepromRead6()
{
  teston6 = EEPROM.read(Eepromautostatus6);
 Serial.print ("on");
  Serial.println (teston6);
  return proceed;
}

//********************relay 6**************
int teston7;
int Eepromautostatus7 =316;
bool relay6=teston7;
void autosavefun7(){
  if (teston7==1){
    relay6=true;
    }
    else{
      relay6=false;
      }
  }
TOGGLE(relay6, L6, "     MODULE 6 : ", doNothing, noEvent, noStyle
       , VALUE("On", HIGH, autosaveon7, noEvent)
       , VALUE("Off", LOW, autosaveon7, noEvent)
      );

result autosaveon7(){
  EEPROM.write(Eepromautostatus7, relay6);
  }

void EepromRead7()
{
  teston7 = EEPROM.read(Eepromautostatus7);
 Serial.print ("on");
  Serial.println (teston7);
  return proceed;
}
      
//********************relay 7**************
int teston8;
int Eepromautostatus8 =318;
bool relay7=teston8;
void autosavefun8(){
  if (teston8==1){
    relay7=true;
    }
    else{
      relay7=false;
      }
  }
TOGGLE(relay7, L7, "     MODULE 7 : ", doNothing, noEvent, noStyle
       , VALUE("On", HIGH, autosaveon8, noEvent)
       , VALUE("Off", LOW, autosaveon8, noEvent)
      );

result autosaveon8(){
  EEPROM.write(Eepromautostatus8, relay7);
  }

void EepromRead8()
{
  teston8 = EEPROM.read(Eepromautostatus8);
 Serial.print ("on");
  Serial.println (teston8);
  return proceed;
}

//********************relay 8**************
int teston9;
int Eepromautostatus9 =320;
bool relay8=teston9;
void autosavefun9(){
  if (teston9==1){
    relay8=true;
    }
    else{
      relay8=false;
      }
  }
TOGGLE(relay8, L8, "     MODULE 8 : ", doNothing, noEvent, noStyle
       , VALUE("On", HIGH, autosaveon9, noEvent)
       , VALUE("Off", LOW, autosaveon9, noEvent)
      );

result autosaveon9(){
  EEPROM.write(Eepromautostatus9, relay8);
  }

void EepromRead9()
{
  teston9 = EEPROM.read(Eepromautostatus9);
 Serial.print ("on");
  Serial.println (teston9);
  return proceed;
}
//******************************************************************
MENU(MANmenu, " MANUAL", showEvent, enterEvent, wrapStyle
, OP(" ZONE ", doNothing, noEvent)     
     , SUBMENU(L1)
     , SUBMENU(L2)
     , SUBMENU(L3)
     , SUBMENU(L4)
     , SUBMENU(L5)
     , SUBMENU(L6)
     , SUBMENU(L7)
     , SUBMENU(L8)
, EXIT("<Back")
    );      

///****************************************************************    
//customizing a prompt look!
//by extending the prompt class
class altPrompt:public prompt {
public:
  altPrompt(constMEM promptShadow& p):prompt(p) {}
  Used printTo(navRoot &root,bool sel,menuOut& out, idx_t idx,idx_t len,idx_t) override {
    return out.printRaw(F("special prompt!"),len);;
  }
};
//*****************************ZONE ONE AUTO ************************************
int LAMP1SThr;
int LAMP1STmin;
int LAMP1SPhr;
int LAMP1SPmin;

int LAMP2SThr;
int LAMP2STmin;
int LAMP2SPhr;
int LAMP2SPmin;

int LAMP3SThr;
int LAMP3STmin;
int LAMP3SPhr;
int LAMP3SPmin;

int LAMP4SThr;
int LAMP4STmin;
int LAMP4SPhr;
int LAMP4SPmin;

int LAMP5SThr;
int LAMP5STmin;
int LAMP5SPhr;
int LAMP5SPmin;

int LAMP6SThr;
int LAMP6STmin;
int LAMP6SPhr;
int LAMP6SPmin;

int LAMP7SThr;
int LAMP7STmin;
int LAMP7SPhr;
int LAMP7SPmin;

int LAMP8SThr;
int LAMP8STmin;
int LAMP8SPhr;
int LAMP8SPmin;

int LAMP1,LAMP2,LAMP3,LAMP4,LAMP5,LAMP6,LAMP7,LAMP8;
int LAMP11,LAMP22,LAMP33,LAMP44,LAMP55,LAMP66,LAMP77,LAMP88;

int EepromLAMP1SThr = 200;
int EepromLAMP1STmin =202;
int EepromLAMP1SPhr =204;
int EepromLAMP1SPmin= 206;

int EepromLAMP2SThr = 208;
int EepromLAMP2STmin =210;
int EepromLAMP2SPhr =212;
int EepromLAMP2SPmin= 214;

int EepromLAMP3SThr = 216;
int EepromLAMP3STmin =214;
int EepromLAMP3SPhr =218;
int EepromLAMP3SPmin= 220;

int EepromLAMP4SThr = 222;
int EepromLAMP4STmin =224;
int EepromLAMP4SPhr =226;
int EepromLAMP4SPmin= 228;

int EepromLAMP5SThr = 230;
int EepromLAMP5STmin =232;
int EepromLAMP5SPhr =234;
int EepromLAMP5SPmin= 236;

int EepromLAMP6SThr = 238;
int EepromLAMP6STmin =240;
int EepromLAMP6SPhr =242;
int EepromLAMP6SPmin= 244;

int EepromLAMP7SThr = 246;
int EepromLAMP7STmin =248;
int EepromLAMP7SPhr =250;
int EepromLAMP7SPmin= 252;

int EepromLAMP8SThr = 254;
int EepromLAMP8STmin =256;
int EepromLAMP8SPhr =258;
int EepromLAMP8SPmin= 260;

int EepromLAMP1on = 262;
int EepromLAMP2on =264;
int EepromLAMP3on =266;
int EepromLAMP4on = 268;
int EepromLAMP5on = 270;
int EepromLAMP6on =272;
int EepromLAMP7on =274;
int EepromLAMP8on = 276;

//int Eepromautostatus =278;
char PUMPAONtime;
char PUMPAOFFtime;
//**********************save lamp life ********************************************
result LAMP1saveontime() {
   LAMP1=LAMP11+LAMP1; 
  EEPROM.write(EepromLAMP1on, LAMP1);
  return proceed;
}
void EepromReadLAMP1save ()
{
 LAMP11 = EEPROM.read(EepromLAMP1on);
 //LAMP11=LAMP11/60;
  return proceed;
}
result LAMP2saveontime() {
   LAMP2=LAMP22+LAMP2;  
  EEPROM.write(EepromLAMP2on, LAMP2);
  return proceed;
}
void EepromReadLAMP2save ()
{
  LAMP22 = EEPROM.read(EepromLAMP2on);
  //LAMP22=LAMP22/60;
  return proceed;
}
result LAMP3saveontime() {
    LAMP3=LAMP33+LAMP3; 
  EEPROM.write(EepromLAMP3on, LAMP3);
  return proceed;
}
void EepromReadLAMP3save ()
{
  LAMP33 = EEPROM.read(EepromLAMP3on);
  //LAMP33=LAMP33/60;
  return proceed;
}
result LAMP4saveontime() {
    LAMP4=LAMP44+LAMP4; 
  EEPROM.write(EepromLAMP4on, LAMP4);
  return proceed;
}
void EepromReadLAMP4save ()
{
  LAMP44 = EEPROM.read(EepromLAMP4on);
 // LAMP44=LAMP44/60;
  return proceed;
}
result LAMP5saveontime() {
    LAMP5=LAMP55+LAMP5; 
  EEPROM.write(EepromLAMP5on, LAMP5);
  return proceed;
}
void EepromReadLAMP5save ()
{
  LAMP55 = EEPROM.read(EepromLAMP5on);
  //LAMP55=LAMP55/60;
  return proceed;
}
result LAMP6saveontime() {
    LAMP6=LAMP66+LAMP6; 
  EEPROM.write(EepromLAMP6on, LAMP6);
  return proceed;
}
void EepromReadLAMP6save ()
{
  LAMP66 = EEPROM.read(EepromLAMP6on);
  //LAMP66=LAMP66/60;
  return proceed;
}
result LAMP7saveontime() {
    LAMP7=LAMP77+LAMP7; 
  EEPROM.write(EepromLAMP7on, LAMP7);
  return proceed;
}
void EepromReadLAMP7save ()
{
  LAMP77 = EEPROM.read(EepromLAMP7on);
  //LAMP77=LAMP77/60;
  return proceed;
}
result LAMP8saveontime() {
    LAMP8=LAMP88+LAMP8; 
  EEPROM.write(EepromLAMP8on, LAMP8);
  return proceed;
}
void EepromReadLAMP8save ()
{
  LAMP88 = EEPROM.read(EepromLAMP8on);
  //LAMP88=LAMP88/60;
  return proceed;
}


//***SAVE TIME********************************************************///
result LAMP1savetime() {
  EEPROM.write(EepromLAMP1SThr, LAMP1SThr);
  EEPROM.write(EepromLAMP1STmin,LAMP1STmin);
  return proceed;
}
result LAMP1savedue() {
  EEPROM.write(EepromLAMP1SPhr, LAMP1SPhr);
  EEPROM.write(EepromLAMP1SPmin,LAMP1SPmin);
  return proceed;
}
void EepromReadLAMP1()
{
  LAMP1SThr = EEPROM.read(EepromLAMP1SThr);
  LAMP1STmin = EEPROM.read(EepromLAMP1STmin);
  LAMP1SPhr = EEPROM.read(EepromLAMP1SPhr);
  LAMP1SPmin = EEPROM.read(EepromLAMP1SPmin);
  return proceed;
}
//*****************************************************************
result LAMP2savetime() {
  EEPROM.write(EepromLAMP2SThr, LAMP2SThr);
  EEPROM.write(EepromLAMP2STmin, LAMP2STmin);

  return proceed;
}
result LAMP2savedue() {

  EEPROM.write(EepromLAMP2SPhr, LAMP2SPhr);
  EEPROM.write(EepromLAMP2SPmin,LAMP2SPmin);
  return proceed;
}
void EepromReadLAMP2()
{
  LAMP2SThr = EEPROM.read(EepromLAMP2SThr);
  LAMP2STmin = EEPROM.read(EepromLAMP2STmin);
  LAMP2SPhr = EEPROM.read(EepromLAMP2SPhr);
  LAMP2SPmin = EEPROM.read(EepromLAMP2SPmin);
  return proceed;
}
//*****************************************************************
result LAMP3savetime() {
  EEPROM.write(EepromLAMP3SThr, LAMP3SThr);
  EEPROM.write(EepromLAMP3STmin, LAMP3STmin);

  return proceed;
}
result LAMP3savedue() {

  EEPROM.write(EepromLAMP3SPhr,LAMP3SPhr);
  EEPROM.write(EepromLAMP3SPmin, LAMP3SPmin);
  return proceed;
}
void EepromReadLAMP3()
{
  LAMP3SThr = EEPROM.read(EepromLAMP3SThr);
  LAMP3STmin = EEPROM.read(EepromLAMP3STmin);
  LAMP3SPhr = EEPROM.read(EepromLAMP3SPhr);
  LAMP3SPmin = EEPROM.read(EepromLAMP3SPmin);
  return proceed;
}
//*****************************************************************
result LAMP4savetime() {
  EEPROM.write(EepromLAMP4SThr, LAMP4SThr);
  EEPROM.write(EepromLAMP4STmin, LAMP4STmin);

  return proceed;
}
result LAMP4savedue() {

  EEPROM.write(EepromLAMP4SPhr,LAMP4SPhr);
  EEPROM.write(EepromLAMP4SPmin,LAMP4SPmin);
  return proceed;
}
void EepromReadLAMP4()
{
  LAMP4SThr = EEPROM.read(EepromLAMP4SThr);
  LAMP4STmin = EEPROM.read(EepromLAMP4STmin);
  LAMP4SPhr = EEPROM.read(EepromLAMP4SPhr);
  LAMP4SPmin = EEPROM.read(EepromLAMP4SPmin);
  return proceed;
}
//*****************************************************************
result LAMP5savetime() {
  EEPROM.write(EepromLAMP5SThr, LAMP5SThr);
  EEPROM.write(EepromLAMP5STmin, LAMP5STmin);

  return proceed;
}
result LAMP5savedue() {

  EEPROM.write(EepromLAMP5SPhr,LAMP5SPhr);
  EEPROM.write(EepromLAMP5SPmin, LAMP5SPmin);
  return proceed;
}
void EepromReadLAMP5()
{
  LAMP5SThr = EEPROM.read(EepromLAMP5SThr);
  LAMP5STmin = EEPROM.read(EepromLAMP5STmin);
  LAMP5SPhr = EEPROM.read(EepromLAMP5SPhr);
  LAMP5SPmin = EEPROM.read(EepromLAMP5SPmin);
  return proceed;
}
//*****************************************************************
result LAMP6savetime() {
  EEPROM.write(EepromLAMP6SThr, LAMP6SThr);
  EEPROM.write(EepromLAMP6STmin, LAMP6STmin);

  return proceed;
}
result LAMP6savedue() {

  EEPROM.write(EepromLAMP6SPhr,LAMP6SPhr);
  EEPROM.write(EepromLAMP6SPmin, LAMP6SPmin);
  return proceed;
}
void EepromReadLAMP6()
{
  LAMP6SThr = EEPROM.read(EepromLAMP6SThr);
  LAMP6STmin = EEPROM.read(EepromLAMP6STmin);
  LAMP6SPhr = EEPROM.read(EepromLAMP6SPhr);
  LAMP6SPmin = EEPROM.read(EepromLAMP6SPmin);
  return proceed;
}
//*****************************************************************
result LAMP7savetime() {
  EEPROM.write(EepromLAMP7SThr, LAMP7SThr);
  EEPROM.write(EepromLAMP7STmin,LAMP7STmin);
  return proceed;
}
result LAMP7savedue() {
  EEPROM.write(EepromLAMP7SPhr,LAMP7SPhr);
  EEPROM.write(EepromLAMP7SPmin, LAMP7SPmin);
  return proceed;
}
void EepromReadLAMP7()
{
  LAMP7SThr = EEPROM.read(EepromLAMP7SThr);
  LAMP7STmin = EEPROM.read(EepromLAMP7STmin);
  LAMP7SPhr = EEPROM.read(EepromLAMP7SPhr);
  LAMP7SPmin = EEPROM.read(EepromLAMP7SPmin);
  return proceed;
}
//*****************************************************************
result LAMP8savetime() {
  EEPROM.write(EepromLAMP8SThr, LAMP8SThr);
  EEPROM.write(EepromLAMP8STmin, LAMP8STmin);
  return proceed;
}
result LAMP8savedue() {

  EEPROM.write(EepromLAMP8SPhr,LAMP8SPhr);
  EEPROM.write(EepromLAMP8SPmin, LAMP8SPmin);
  return proceed;
}
void EepromReadLAMP8()
{
  LAMP8SThr = EEPROM.read(EepromLAMP8SThr);
  LAMP8STmin = EEPROM.read(EepromLAMP8STmin);
  LAMP8SPhr = EEPROM.read(EepromLAMP8SPhr);
  LAMP8SPmin = EEPROM.read(EepromLAMP8SPmin);
  return proceed;
}
//******************************************Time Date Setting**************************************************************
int t1;
int t2;
int h1;
int h2;
altMENU(menu, timeMenu, " TIME ", doNothing, anyEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(hourupg, "", ":", 0, 23, 1, 0, doNothing, anyEvent, noStyle)
        , FIELD(minupg, "", ":", 0, 59, 10, 1, doNothing, anyEvent, wrapStyle)
        , FIELD(secslive, "", "", 0, 59, 10, 1, doNothing, noEvent, wrapStyle)
        
       );

altMENU(menu, dateMenu, " DATE ", doNothing, anyEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(Datelive, "", ":", 0, 31, 1, 0, doNothing, anyEvent, noStyle)
        , FIELD(Monthlive, "", ":", 0, 12, 10, 1, doNothing, anyEvent, wrapStyle)
        , FIELD(Yearlive, "", "", 0, 2050, 10, 1, doNothing, anyEvent, wrapStyle)
       );

altMENU(menu, LAMP1SET1, " ON ", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(LAMP1SThr, ":", "  HH", 0, 23, 10, 1, LAMP1savetime, enterEvent, wrapStyle)
        , FIELD(LAMP1STmin, "", "  MIN", 0, 59, 10, 1, LAMP1savetime, enterEvent, wrapStyle) 
       );
       
altMENU(menu, LAMP1SET2, " OFF", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(LAMP1SPhr, ":", "  HH", 0, 23, 10, 1, LAMP1savedue, enterEvent, wrapStyle)
        , FIELD(LAMP1SPmin, "", "  MIN", 0, 59, 10, 1, LAMP1savedue, enterEvent, wrapStyle)
              );

altMENU(menu, LAMP2SET1, " ON ", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(LAMP2SThr, ":", "  HH", 0, 23, 10, 1, LAMP2savetime, enterEvent, wrapStyle)
        , FIELD(LAMP2STmin, "", "  MIN", 0, 59, 10, 1, LAMP2savetime, enterEvent, wrapStyle) 
       );

altMENU(menu, LAMP2SET2, " OFF", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(LAMP2SPhr, ":", "  HH", 0, 23, 10, 1, LAMP2savedue, enterEvent, wrapStyle)
        , FIELD(LAMP2SPmin, "", "  MIN", 0, 59, 10, 1, LAMP2savedue, enterEvent, wrapStyle)
              );

altMENU(menu, LAMP3SET1, " ON ", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(LAMP3SThr, ":", "  HH", 0, 23, 10, 1, LAMP3savetime, enterEvent, wrapStyle)
        , FIELD(LAMP3STmin, "", "  MIN", 0, 59, 10, 1, LAMP3savetime, enterEvent, wrapStyle) 
       );
       
altMENU(menu, LAMP3SET2, " OFF", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(LAMP3SPhr, ":", "  HH", 0, 23, 10, 1, LAMP3savedue, enterEvent, wrapStyle)
        , FIELD(LAMP3SPmin, "", "  MIN", 0, 59, 10, 1, LAMP3savedue, enterEvent, wrapStyle)
              );
//****************************************************************     
//******************************************************************

altMENU(menu, LAMP4SET1, " ON ", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(LAMP4SThr, ":", "  HH", 0, 23, 10, 1, LAMP4savetime, enterEvent, wrapStyle)
        , FIELD(LAMP4STmin, "", "  MIN", 0, 59, 10, 1, LAMP4savetime, enterEvent, wrapStyle) 
       );
       
altMENU(menu, LAMP4SET2, " OFF", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(LAMP4SPhr, ":", "  HH", 0, 23, 10, 1, LAMP4savedue, enterEvent, wrapStyle)
        , FIELD(LAMP4SPmin, "", "  MIN", 0, 59, 10, 1, LAMP4savedue, enterEvent, wrapStyle)
              );

altMENU(menu, LAMP5SET1, " ON ", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(LAMP5SThr, ":", "  HH", 0, 23, 10, 1, LAMP5savetime, enterEvent, wrapStyle)
        , FIELD(LAMP5STmin, "", "  MIN", 0, 59, 10, 1, LAMP5savetime, enterEvent, wrapStyle) 
       );
       
altMENU(menu, LAMP5SET2, " OFF", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(LAMP5SPhr, ":", "  HH", 0, 23, 10, 1, LAMP5savedue, enterEvent, wrapStyle)
        , FIELD(LAMP5SPmin, "", "  MIN", 0, 59, 10, 1, LAMP5savedue, enterEvent, wrapStyle)
              );

altMENU(menu, LAMP6SET1, " ON ", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(LAMP6SThr, ":", "  HH", 0, 23, 10, 1, LAMP6savetime, enterEvent, wrapStyle)
        , FIELD(LAMP6STmin, "", "  MIN", 0, 59, 10, 1, LAMP6savetime, enterEvent, wrapStyle) 
       );
       
altMENU(menu, LAMP6SET2, " OFF", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(LAMP6SPhr, ":", "  HH", 0, 23, 10, 1, LAMP6savedue, enterEvent, wrapStyle)
        , FIELD(LAMP6SPmin, "", "  MIN", 0, 59, 10, 1, LAMP6savedue, enterEvent, wrapStyle)
              );
//****************************************************************   
//******************************************************************

altMENU(menu, LAMP7SET1, " ON ", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(LAMP7SThr, ":", "  HH", 0, 23, 10, 1, LAMP7savetime, enterEvent, wrapStyle)
        , FIELD(LAMP7STmin, "", "  MIN", 0, 59, 10, 1, LAMP7savetime, enterEvent, wrapStyle) 
       );
       
altMENU(menu, LAMP7SET2, " OFF", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(LAMP7SPhr, ":", "  HH", 0, 23, 10, 1, LAMP7savedue, enterEvent, wrapStyle)
        , FIELD(LAMP7SPmin, "", "  MIN", 0, 59, 10, 1, LAMP7savedue, enterEvent, wrapStyle)
              );

altMENU(menu, LAMP8SET1, " ON ", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(LAMP8SThr, ":", "  HH", 0, 23, 10, 1, LAMP8savetime, enterEvent, wrapStyle)
        , FIELD(LAMP8STmin, "", "  MIN", 0, 59, 10, 1, LAMP8savetime, enterEvent, wrapStyle) 
       );
       
altMENU(menu, LAMP8SET2, " OFF", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(LAMP8SPhr, ":", "  HH", 0, 23, 10, 1, LAMP8savedue, enterEvent, wrapStyle)
        , FIELD(LAMP8SPmin, "", "  MIN", 0, 59, 10, 1, LAMP8savedue, enterEvent, wrapStyle)
              );
//****************************************************************   
MENU(timedateset, " DATE & TIME", showEvent, anyEvent, noStyle

     , SUBMENU(timeMenu)
     , SUBMENU(dateMenu)
     , EXIT("<Back")
    );
MENU(Monitor, " DATE & TIME",showEvent, noEvent, noStyle
     , SUBMENU(dateMenu)
     , SUBMENU(timeMenu)
     , EXIT("<Back")
    );


MENU(selMenu01,"UV MODULE 1",showEvent,anyEvent,noStyle
     , SUBMENU(LAMP1SET1)
     , SUBMENU(LAMP1SET2)
     , OP("CONFIRM"  , EepromReadLAMP1, enterEvent)
     , EXIT("<Back")
);
MENU(selMenu02,"UV MODULE 2",showEvent,anyEvent,noStyle
     , SUBMENU(LAMP2SET1)
     , SUBMENU(LAMP2SET2)
     , OP("CONFIRM"  , EepromReadLAMP2, enterEvent)
     , EXIT("<Back")
);
MENU(selMenu03,"UV MODULE 3",showEvent,anyEvent,noStyle
     , SUBMENU(LAMP3SET1)
     , SUBMENU(LAMP3SET2)
     , OP("CONFIRM"  , EepromReadLAMP3, enterEvent)
     , EXIT("<Back")
);
MENU(selMenu04,"UV MODULE 4",showEvent,anyEvent,noStyle
     , SUBMENU(LAMP4SET1)
     , SUBMENU(LAMP4SET2)
     , OP("CONFIRM"  , EepromReadLAMP4, enterEvent)
     , EXIT("<Back")
);
MENU(selMenu05,"UV MODULE 5",showEvent,anyEvent,noStyle
     , SUBMENU(LAMP5SET1)
     , SUBMENU(LAMP5SET2)
     , OP("CONFIRM"  , EepromReadLAMP5, enterEvent)
     , EXIT("<Back")
);
MENU(selMenu06,"UV MODULE 6",showEvent,anyEvent,noStyle
     , SUBMENU(LAMP6SET1)
     , SUBMENU(LAMP6SET2)
     , OP("CONFIRM"  , EepromReadLAMP6, enterEvent)
     , EXIT("<Back")
);
MENU(selMenu07,"UV MODULE 7",showEvent,anyEvent,noStyle
     , SUBMENU(LAMP7SET1)
     , SUBMENU(LAMP7SET2)
     , OP("CONFIRM"  , EepromReadLAMP7, enterEvent)
     , EXIT("<Back")
);
MENU(selMenu08,"UV MODULE 8",showEvent,anyEvent,noStyle
     , SUBMENU(LAMP8SET1)
     , SUBMENU(LAMP8SET2)
     , OP("CONFIRM"  , EepromReadLAMP8, enterEvent)
     , EXIT("<Back")
);

int teston ;
int Eepromautostatus =300;
bool Autocnt=teston;
void autosavefun(){
  if (teston==1){
    Autocnt=true;
    }
    else{
      Autocnt=false;
      }
  }
TOGGLE(Autocnt, autoOn, "Auto Mode   : ", doNothing, enterEvent, noStyle
       , VALUE("On", HIGH, autosaveon, enterEvent)
       , VALUE("Off", LOW, autosaveon, enterEvent)
      );

result autosaveon(){
  EEPROM.write(Eepromautostatus, Autocnt);
  }

void EepromRead()
{
  teston = EEPROM.read(Eepromautostatus);
 Serial.print ("on");
  Serial.println (teston);
  return proceed;
}
//****************DOOR EEPROM****************
int teston1 ;
int Eepromautostatus1 =302;
bool DOORSET=teston1;
void autosavefun1(){
  if (teston1==1){
    DOORSET=true;
    }
    else{
      DOORSET=false;
      }
  }
TOGGLE(DOORSET, DSET, " DOOR SWITCH: ", doNothing, enterEvent, noStyle
       , VALUE("ENABLE", HIGH, autosaveon1, enterEvent)
       , VALUE("DISABLE", LOW, autosaveon1, enterEvent)
      ); 

result autosaveon1(){
  EEPROM.write(Eepromautostatus1, DOORSET);
  }

void EepromRead1()
{
  teston1 = EEPROM.read(Eepromautostatus1);
 Serial.print ("on");
  Serial.println (teston1);
  return proceed;
}
bool WIFISET;
TOGGLE(WIFISET, wifi, " WIFI SETTINGS: ", doNothing, noEvent, noStyle
       , VALUE("ENABLE", HIGH, doNothing, noEvent)
       , VALUE("DISABLE", LOW, doNothing, noEvent)
      );     
MENU(Automatic," AUTOMATIC",showEvent,anyEvent,wrapStyle
 ,SUBMENU(autoOn)
 ,SUBMENU(selMenu01)
 ,SUBMENU(selMenu02)
 ,SUBMENU(selMenu03)
 ,SUBMENU(selMenu04)
 ,SUBMENU(selMenu05)
 ,SUBMENU(selMenu06)
 ,SUBMENU(selMenu07)
 ,SUBMENU(selMenu08)
 ,EXIT("<Back>")
);
//*********************************************************************************************************

///*********EPROM********************///
result home(menuOut& o, idleEvent e) {
  {
    updateScreen();
  }
  return proceed;
}
//----------------------menu setting--------------------
MENU(Settingsmenu," SETTINGS",showEvent,noEvent,noStyle
  , OP(" INFO", Cinfo, enterEvent)
   //, OP(" DOOR SETTINGS", DSET, enterEvent)
    ,SUBMENU(DSET)
    ,SUBMENU(wifi)
  ,EXIT("<Back")
);
MENU(mode," MODE",showEvent,noEvent,noStyle
  ,SUBMENU(Automatic)
  ,SUBMENU(MANmenu)
  ,EXIT("<Back")
);
MENU(lamprst," MODULE RESET",showEvent,noEvent,noStyle
  , OP(" UVC 1" ,RST1, enterEvent)
  , OP(" UVC 2" ,RST2, enterEvent)
  , OP(" UVC 3" ,RST3, enterEvent)
  , OP(" UVC 4" ,RST4, enterEvent)
  , OP(" UVC 5" ,RST5, enterEvent)
  , OP(" UVC 6" ,RST6, enterEvent)
  , OP(" UVC 7" ,RST7, enterEvent)
  , OP(" UVC 8" ,RST8, enterEvent)
  , OP(" ALL MODULES" ,RST, enterEvent)
  ,EXIT("<Back")
);
MENU(life," LIFESPAN",showEvent,noEvent,noStyle
  , OP(" MODULE LIFE ", LP, enterEvent)
  , SUBMENU(lamprst)
  ,EXIT("<Back")
);
MENU(mainMenu, " DISINFECTION CONTROLLER", doNothing, noEvent, wrapStyle
     , SUBMENU(Monitor)
     , OP(" STATUS", goHome, enterEvent)
     , SUBMENU(life)
     ,SUBMENU(mode)
     ,SUBMENU(Settingsmenu)
  );
serialIn serial(Serial);
//fontY should now account for fontMarginY
#define fontX 3
#define fontY 10
#define MAX_DEPTH 10

MENU_OUTPUTS(out, MAX_DEPTH
             // ,U8GLIB_OUT(u8g2,colors,fontX,fontY,{0,0,128/fontX,64/fontY})
             , U8G2_OUT(u8g2, colors, fontX, fontY, offsetX, offsetY, {0, 0, U8_Width / fontX, U8_Height / fontY})
             , SERIAL_OUT(Serial)
             , NONE
            );

NAVROOT(nav, mainMenu, MAX_DEPTH, in, out);
result alert(menuOut& o, idleEvent e) {
  if (e == idling) {
    o.setCursor(0, 0);
    o.print("alert test");
    o.setCursor(0, 1);
    o.print("press [select]");
    o.setCursor(0, 2);
    o.print("to continue...");
  }
  return proceed;
}
result LP1(menuOut& o, idleEvent e) {
  {
   lamplife();
  }
  return proceed;
}
result RST0(menuOut& o, idleEvent e) {
  {
   RESET();
  }
  return proceed;
}
result RST01(menuOut& o, idleEvent e) {
  {
   RESET1();
  }
  return proceed;
}
result RST02(menuOut& o, idleEvent e) {
  {
   RESET2();
  }
  return proceed;
}
result RST03(menuOut& o, idleEvent e) {
  {
   RESET3();
  }
  return proceed;
}
result RST04(menuOut& o, idleEvent e) {
  {
   RESET4();
  }
  return proceed;
}
result RST05(menuOut& o, idleEvent e) {
  {
   RESET5();
  }
  return proceed;
}
result RST06(menuOut& o, idleEvent e) {
  {
   RESET6();
  }
  return proceed;
}
result RST07(menuOut& o, idleEvent e) {
  {
   RESET7();
  }
  return proceed;
}
result RST08(menuOut& o, idleEvent e) {
  {
   RESET8();
  }
  return proceed;
}
result goHome(menuOut& o, idleEvent e) {
  //   updateScreen();
  nav.idleOn(home);
  return proceed;
}
result LP(eventMask e, prompt &item) {
  nav.idleOn(LP1);
  return proceed;
}
result RST(eventMask e, prompt &item) {
  nav.idleOn(RST0);
  return proceed;
}
result RST1(eventMask e, prompt &item) {
  nav.idleOn(RST01);
  return proceed;
}
result RST2(eventMask e, prompt &item) {
  nav.idleOn(RST02);
  return proceed;
}
result RST3(eventMask e, prompt &item) {
  nav.idleOn(RST03);
  return proceed;
}
result RST4(eventMask e, prompt &item) {
  nav.idleOn(RST04);
  return proceed;
}
result RST5(eventMask e, prompt &item) {
  nav.idleOn(RST05);
  return proceed;
}
result RST6(eventMask e, prompt &item) {
  nav.idleOn(RST06);
  return proceed;
}
result RST7(eventMask e, prompt &item) {
  nav.idleOn(RST07);
  return proceed;
}
result RST8(eventMask e, prompt &item) {
  nav.idleOn(RST08);
  return proceed;
}
result doAlert(eventMask e, prompt &item) {
  nav.idleOn(alert);
  return proceed;
}
result Cinfo(eventMask e, prompt &item) {
  nav.idleOn(controllerinfo);
  return proceed;
}
result DOORinfo(eventMask e, prompt &item) {
  nav.idleOn(Dinfo);
  return proceed;
}


///Jsonpack
void Room2data()
{
 
  //Serial.println("start sending");
//StaticJsonBuffer<1000> jsonBuffer;
 DynamicJsonDocument room2doc(260);

//JsonObject Rm = room2doc.createNestedObject("Rm");
//Rm["D"] =1; //door
JsonObject R1 = room2doc.createNestedObject("R1");
R1["S2"] = s1;//  sta2;
R1["M2"] =LAMP11;// LAMP22; 

R1["IN2"] =s2;// counter1;  //counter2
R1["V2"]  = LAMP22;

R1["C2"] =s3; 
R1["L2"] =LAMP33;
 
R1["L22"] =s4;
R1["R22"] =LAMP44;

R1["Q2"] =s5;
R1["W2"] =LAMP55;
 
R1["E2"] =s6; 
R1["T2"] =LAMP66;

R1["I2"] =s7;
R1["LC1"] =LAMP77; //LIGHT CYCLE

R1["INS1"] =ds; //door status 


serializeJsonPretty(room2doc, Serial3);
serializeJsonPretty(room2doc, Serial);

}
void Room3data()
{
  //Serial.println("start sending");
//StaticJsonBuffer<1000> jsonBuffer;
 DynamicJsonDocument room3doc(250);

JsonObject R2 = room3doc.createNestedObject("R2");
R2["M11"] =  11;//  sta2;
R2["M22"] =22;// LAMP22; 
R2["M33"] =33;// counter1;  //counter2
//R2["M44"] =voltage2;
//R2["M55"] =current2; 
//R2["M66"] =lamp2on; 
//R2["M55"] =lamp2off; 

serializeJsonPretty(room3doc, Serial3);
serializeJsonPretty(room3doc, Serial);

}

void Room4data()
{
  //Serial.println("start sending");
//StaticJsonBuffer<1000> jsonBuffer;
 DynamicJsonDocument room4doc(500);

JsonObject Room3 = room4doc.createNestedObject("Room3");
Room3["S4"] =  210;//  sta2;
Room3["M4"] =220;// LAMP22; 
Room3["IN4"] =230;// counter1;  //counter2
Room3["V4"] =voltage2;
Room3["C4"] =current2; 
Room3["L4"] =lamp2on; 
Room3["L44"] =lamp2off; 

serializeJsonPretty(room4doc, Serial3);
serializeJsonPretty(room4doc, Serial);

}

void Room5data()
{
  //Serial.println("start sending");
//StaticJsonBuffer<1000> jsonBuffer;
 DynamicJsonDocument room5doc(500);

JsonObject Room4 = room5doc.createNestedObject("Room4");
Room4["S5"] =  210;//  sta2;
Room4["M5"] =220;// LAMP22; 
Room4["IN5"] =230;// counter1;  //counter2
Room4["V5"] =voltage2;
Room4["C5"] =current2; 
Room4["L5"] =lamp2on; 
Room4["L55"] =lamp2off; 

serializeJsonPretty(room5doc, Serial3);
serializeJsonPretty(room5doc, Serial);

}
void Room6data()
{
  //Serial.println("start sending");
//StaticJsonBuffer<1000> jsonBuffer;
 DynamicJsonDocument room6doc(500);

JsonObject Room5 = room6doc.createNestedObject("Room5");
Room5["S6"] =  210;//  sta2;
Room5["M6"] =220;// LAMP22; 
Room5["IN6"] =230;// counter1;  //counter2
Room5["V6"] =voltage2;
Room5["C6"] =current2; 
Room5["L6"] =lamp2on; 
Room5["L66"] =lamp2off; 

serializeJsonPretty(room6doc, Serial3);
serializeJsonPretty(room6doc, Serial);

}

void Dinfo(){
  {
    u8g2.drawFrame(0, 0, 128, 64);
    u8g2.drawStr(9, 15, "     UVC ALERT!!!!");
    u8g2.drawStr(3, 25, "HIGH INTENSITY UV LIGHT    ");
    u8g2.drawStr(3, 38, "WEAR PROPER EYE AND SKIN ");
    u8g2.drawStr(9, 48, "     PROTECTION  ");
    u8g2.setCursor(40, 51);
    digitalWrite(BUZZER,HIGH );
  }
  return proceed;
}
result controllerinfo(eventMask e, navNode& nav, prompt &item) {
  DateTime now = rtc.now();
  {
    u8g2.drawFrame(0, 0, 128, 64);
    u8g2.drawFrame(5, 5, 118, 54);
    u8g2.drawStr(9, 15, "     MODEL:UVC-DLC08");
    u8g2.drawStr(25, 35, "    YoM:2021");
    u8g2.drawStr(20, 48, "Product By Cenaura");
    u8g2.setCursor(40, 51);
}
  return proceed;
}
///***************Read all data from memoery******************************
void readalldata() {
  EepromReadLAMP1();
  EepromReadLAMP2();
  EepromReadLAMP3();
  EepromReadLAMP4();
  EepromReadLAMP5();
  EepromReadLAMP6();
  EepromReadLAMP7();
  EepromReadLAMP8();
  EepromRead();
  autosavefun();
  EepromRead1();
  autosavefun1();
  EepromRead2();
  autosavefun2();

  EepromRead3();
  autosavefun3();

  EepromRead4();
  autosavefun4();

  EepromRead5();
  autosavefun5();

  EepromRead6();
  autosavefun6();

  EepromRead7();
  autosavefun7();

  EepromRead8();
  autosavefun8();

  EepromRead9();
  autosavefun9();
  return proceed;
}
void readlife() {
  EepromReadLAMP1save ();
  EepromReadLAMP2save ();
  EepromReadLAMP3save ();
  EepromReadLAMP4save ();
  EepromReadLAMP5save ();
  EepromReadLAMP6save ();
  EepromReadLAMP7save ();
  EepromReadLAMP8save ();
  return proceed;
}
//when menu is suspended
result idle(menuOut& o, idleEvent e) {
  o.clear();
  switch (e) {
    case idleStart: o.println("suspending menu!"); break;
    case idling: o.println("suspended..."); break;
    case idleEnd: o.println("resuming menu."); break;
  }
  return proceed;
}
void updateScreen() {
  DateTime now = rtc.now();
  hourupg = now.hour();
  minupg = now.minute();
  secslive = now.second();
  Datelive = now.day();
  Monthlive = now.month();
  Yearlive = now.year();
    u8g2.drawFrame(0, 0, 128, 64);
    u8g2.drawFrame(0, 0, 128, 12);
    u8g2.drawFrame(0, 0, 128, 27);
u8g2.drawStr(16, 10, "      MODULE    ");
u8g2.drawStr(0,24, " D/T:");
    u8g2.setCursor(28, 24);
    u8g2.print( now.day());
    u8g2.drawStr(38, 24, "/");
    u8g2.setCursor(44, 24);
    u8g2.print( now.month());
    u8g2.drawStr(49, 24, "/");
    u8g2.setCursor(55, 24);
    u8g2.print( now.year());
    u8g2.setCursor(82 , 24); 
    u8g2.print( now.hour());
    u8g2.drawStr(92, 24, ":"); 
    u8g2.setCursor(97, 24);
    u8g2.print( now.minute());
    u8g2.drawStr(108, 24, ":"); 
    u8g2.setCursor(113, 24);
    u8g2.print( now.second()); 
     u8g2.drawStr(3,35,"UVC 1:");
     u8g2.setCursor(33,35);
     u8g2.print(sta1);
     u8g2.drawStr(3,45,"UVC 2:");
     u8g2.setCursor(33,45);
     u8g2.print(sta2);
     u8g2.drawStr(3,55,"UVC 3:");
     u8g2.setCursor(33,55);
     u8g2.print(sta3);

u8g2.drawStr(3,64,"UVC 4:");
u8g2.setCursor(33,64);
u8g2.print(sta4);
     
     u8g2.drawStr(68,35,"UVC 5:");
     u8g2.setCursor(100,35);
     u8g2.print(sta5);
     u8g2.drawStr(68,45,"UVC 6:");
     u8g2.setCursor(100,45);
     u8g2.print(sta6);
     u8g2.drawStr(68,55,"UVC 7:");
     u8g2.setCursor(100,55);
     u8g2.print(sta7);
     u8g2.drawStr(68,64,"UVC 8:");
     u8g2.setCursor(100,64);
     u8g2.print(sta8);
}
void lamplife()
{
    u8g2.drawFrame(0, 0, 128, 64);
    u8g2.drawFrame(0, 0, 128, 12);
    u8g2.drawFrame(0, 0, 128, 27);
    u8g2.drawStr(16, 10, "    MODULE LIFE    ");
    u8g2.drawStr(0,24, " HOURS:");
    u8g2.setCursor(28, 24);
    u8g2.drawStr(38,24,"9000HRS");
    u8g2.setCursor(82 , 24); 
     u8g2.drawStr(3,35,"UVC 1:");
     u8g2.setCursor(33,35);
     u8g2.print(LAMP11);
     u8g2.drawStr(3,45,"UVC 2:");
     u8g2.setCursor(33,45);
     u8g2.print(LAMP22);
     u8g2.drawStr(3,55,"UVC 3:");
     u8g2.setCursor(33,55);
     u8g2.print(LAMP33);

u8g2.drawStr(3,64,"UVC 4:");
u8g2.setCursor(33,64);
u8g2.print(LAMP44);
     
     u8g2.drawStr(70,35,"UVC 5:");
     u8g2.setCursor(100,35);
     u8g2.print(LAMP55);
     u8g2.drawStr(70,45,"UVC 6:");
     u8g2.setCursor(100,45);
     u8g2.print(LAMP66);
     u8g2.drawStr(70,55,"UVC 7:");
     u8g2.setCursor(100,55);
     u8g2.print(LAMP77);
     u8g2.drawStr(70,64,"UVC 8:");
     u8g2.setCursor(100,64);
     u8g2.print(LAMP88);  
}
void  dateandtime() {
  DateTime now = rtc.now();
  hourupg = now.hour();
  minupg = now.minute();
  secslive = now.second();
  Datelive = now.day();
  Monthlive = now.month();
  Yearlive = now.year();

}
int averageAnalogRead(int pinToRead)
{
  byte numberOfReadings = 8;
  unsigned int runningValue = 0; 
 
  for(int x = 0 ; x < numberOfReadings ; x++)
    runningValue += analogRead(pinToRead);
  runningValue /= numberOfReadings;
 
  return(runningValue);
}
 
float mapfloat(float x, float in_min, float in_max, float out_min, float out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}
//***********************************************************************
void setup() {
  pinMode(LEDPIN, OUTPUT);
  pinMode(UVLIGHT1,OUTPUT);
  pinMode(UVLIGHT2,OUTPUT);
  pinMode(UVLIGHT3,OUTPUT);
  pinMode(UVLIGHT4,OUTPUT);
  pinMode(UVLIGHT5,OUTPUT);
  pinMode(UVLIGHT6,OUTPUT);
  pinMode(UVLIGHT7,OUTPUT);
  pinMode(UVLIGHT8,OUTPUT);
  pinMode(doorsen,INPUT_PULLUP);
  pinMode(VIN0,INPUT);
  pinMode(VIN1,INPUT);
  pinMode(VIN2,INPUT);
  pinMode(VIN3,INPUT);
  pinMode(VIN4,INPUT);
  pinMode(VIN5,INPUT);
  pinMode(VIN6,INPUT);
  pinMode(VIN7,INPUT);
   
   pinMode(rled,OUTPUT);
   pinMode(gled,OUTPUT);
   pinMode(bled,OUTPUT);
   
  digitalWrite(UVLIGHT1,HIGH);
  digitalWrite(UVLIGHT2,HIGH);
  digitalWrite(UVLIGHT3,HIGH);
  digitalWrite(UVLIGHT4,HIGH);
  digitalWrite(UVLIGHT5,HIGH);
  digitalWrite(UVLIGHT6,HIGH);
  digitalWrite(UVLIGHT7,HIGH);
  digitalWrite(UVLIGHT8,HIGH);
   
   Serial.begin(9600);
  Serial3.begin(9600);
  while (!Serial);
  Serial.println("STAVIOR...........");
  Serial.flush();
  gledon();
  DateTime now = rtc.now();
  rtc.begin();
// rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
  sprintf(t, "%02d:%02d:%02d %02d/%02d/%02d",  now.hour(), now.minute(), now.second(), now.day(), now.month(), now.year());
  Serial.print(F("Date/Time: "));
  Serial.println(t);
   nav.showTitle = true;
  u8g2.setFont(fontName);
  u8g2.drawStr(0, 0, "WELCOME TO STAVIOR");
  u8g2.drawStr(10, 10, "BOOTING.....");
  Timer1.initialize(1000);
  Timer1.attachInterrupt(timerIsr);
  SPI.begin();
  u8g2.begin();
  readalldata();
  readlife();
  pinMode(encBtn, INPUT_PULLUP);
  pinMode(BUZZER, OUTPUT);
   digitalWrite (BUZZER, LOW);
  Monitor[0].enabled=disabledStatus;
  Monitor[1].enabled=disabledStatus;
  Monitor[2].enabled=disabledStatus;
  Monitor[3].enabled=disabledStatus;
  Monitor[4].enabled=disabledStatus;
  Monitor[5].enabled=disabledStatus;
  Monitor[6].enabled=disabledStatus;
// Enable  Read time Task
  t0.enable(); //Time
//  TBdata.enable(); //data
//  TBdata1.enable(); //data
  TBdata2.enable(); //data
//  TBdata3.enable(); //data
//  TBdata4.enable(); //data
//  TBdata5.enable(); //data
//  TBdata6.enable(); //data
//  TBdata7.enable(); //data
//  TBdata8.enable(); //data
  nav.idleTask = idle; //point a function to be used when menu is suspended
  u8g2.firstPage();
  do {
    nav.out[0].setCursor(0, 0);
    nav.out[0].print(F("    DISINFECTION LOGIC "));
    nav.out[0].setCursor(0, 1);
    nav.out[0].print(F("        CONTROLLER  "));
    nav.out[0].setCursor(0, 2);
    nav.out[0].print(F("    MODEL:UVC-DLC08"));
    nav.out[0].setCursor(0, 4);
    nav.out[0].print(F("    www.stavior.com"));
    nav.out[0].setCursor(0, 5);
    nav.out[0].print(F("        Booting..."));
  } while (u8g2.nextPage());
  delay(1500);
}
void loop() {
  
 r.execute();
if(DOORSET==true)
{
doorstatus="ENABLE";
ds=1;
doorreading=digitalRead(doorsen);
if(doorreading==LOW)
{
bledon();
nav.idleOn(Dinfo);
  return proceed;
Serial.print("DOOR OPEN");
}
}
if(Autocnt==true)
{
 scheduleon(); 
 modestatus="SCHEDULE";     
}
 if(Autocnt==false)
{
  modestatus="MANUAL";
if(relay1==HIGH)
    L1ON();        
if(relay1==LOW)
    L1OFF();        
if(relay2==HIGH)
    L2ON();        
if(relay2==LOW)
    L2OFF();   
if(relay3==HIGH)
    L3ON();        
if(relay3==LOW)
    L3OFF();   
if(relay4==HIGH)
    L4ON();        
if(relay4==LOW)
    L4OFF(); 
if(relay5==HIGH)
    L5ON();        
if(relay5==LOW)
    L5OFF(); 
if(relay6==HIGH)
    L6ON();        
if(relay6==LOW)
    L6OFF(); 
if(relay7==HIGH)
    L7ON();        
if(relay7==LOW)
    L7OFF(); 
if(relay8==HIGH)
    L8ON();        
if(relay8==LOW)
    L8OFF();
 }  
  DateTime now = rtc.now();
  rtc.begin();
  //rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
 sprintf(t, "%02d:%02d:%02d %02d/%02d/%02d",  now.hour(), now.minute(), now.second(), now.day(), now.month(), now.year());
   digitalWrite (BUZZER, LOW);
  nav.doInput();
  if (nav.changed(1)) {//only draw if menu changed for gfx device
    //because this code clears the screen, if always called then screen will blink
    u8g2.firstPage();
    do nav.doOutput(); while (u8g2.nextPage());
  }
  delay(200);//simulate other tasks delay
}

void scheduleon()
{
if(hourupg==LAMP1SThr && minupg==LAMP1STmin)
L1ON();
if(hourupg==LAMP1SPhr && minupg==LAMP1SPmin)
L1OFF();
if(hourupg==LAMP2SThr && minupg==LAMP2STmin)
L2ON();
if(hourupg==LAMP2SPhr && minupg==LAMP2SPmin)
L2OFF();
if(hourupg==LAMP3SThr && minupg==LAMP3STmin)
L3ON();
if(hourupg==LAMP3SPhr && minupg==LAMP3SPmin)
L3OFF();
if(hourupg==LAMP4SThr && minupg==LAMP4STmin)
L4ON();
if(hourupg==LAMP4SPhr && minupg==LAMP4SPmin)
L4OFF();
if(hourupg==LAMP5SThr && minupg==LAMP5STmin)
L5ON();
if(hourupg==LAMP5SPhr && minupg==LAMP5SPmin)
L5OFF();
if(hourupg==LAMP6SThr && minupg==LAMP6STmin)
L6ON();
if(hourupg==LAMP6SPhr && minupg==LAMP6SPmin)
L6OFF();
if(hourupg==LAMP7SThr && minupg==LAMP7STmin)
L7ON();
if(hourupg==LAMP7SPhr && minupg==LAMP7SPmin)
L7OFF();
if(hourupg==LAMP8SThr && minupg==LAMP8STmin)
L8ON();
if(hourupg==LAMP8SPhr && minupg==LAMP8SPmin)
L8OFF();
}
void L1ON()
{
  s1=1;
  rledon();
  sensorreadings();
  DateTime now = rtc.now();
  if(counter1on==0)
  {
  lamp1on=now.hour();
  Serial.print("1 on time:");
  Serial.println(lamp1on);
  counter1off=1;
  }
  counter1on=1;
  sta1="ON";
  digitalWrite(UVLIGHT1,LOW);
  
}
void L1OFF()
{
  s1=0;
  gledon();
  DateTime now = rtc.now();
  if(counter1off==1)
  {
  lamp1off=now.hour();
  Serial.print("1 off time:");
 Serial.println(lamp1off);
  counter1on=0;
 counter1off=0;
 lamplife1();
  }
  sta1="OFF";
   digitalWrite(UVLIGHT1,HIGH); 
      
}
void L2ON()
{
  s2=1;
  rledon();
  sensorreadings();
  DateTime now = rtc.now();
  if(counter2on==0)
  {
  lamp2on=now.hour();
  Serial.print("2 on time:");
  Serial.println(lamp2on);
  counter2off=1;
  }
  counter2on=1;
  sta2="ON";
   digitalWrite(UVLIGHT2,LOW);
}
void L2OFF()
{
  s2=0;
  gledon();
  DateTime now = rtc.now();
  if(counter2off==1)
  {
  lamp2off=now.hour();
  Serial.print("2 off time:");
 Serial.println(lamp2off);
  counter2on=0;
 counter2off=0;
 lamplife2();
  }
  sta2="OFF";
   digitalWrite(UVLIGHT2,HIGH);    
}
void L3ON()
{
  s3=1;
  rledon();
  sensorreadings();
  DateTime now = rtc.now();
  if(counter3on==0)
  {
  lamp3on=now.hour();
  Serial.print("3 on time:");
  Serial.println(lamp3on);
  counter3off=1;
  }
  counter3on=1;
  sta3="ON";
   digitalWrite(UVLIGHT3,LOW);
}
void L3OFF()
{
  s3=0;
  gledon();
  DateTime now = rtc.now();
  if(counter3off==1)
  {
  lamp3off=now.hour();
  Serial.print("3 off time:");
 Serial.println(lamp3off);
  counter3on=0;
 counter3off=0;
 lamplife3();
  }
  sta3="OFF";
   digitalWrite(UVLIGHT3,HIGH);
}
void L4ON()
{
  s4=1;
  rledon();
  sensorreadings();
  DateTime now = rtc.now();
  if(counter4on==0)
  {
  lamp4on=now.hour();
  Serial.print("4 on time:");
  Serial.println(lamp4on);
  counter4off=1;
  }
  counter4on=1;
  sta4="ON";
   digitalWrite(UVLIGHT4,LOW);
}
void L4OFF()
{
  s4=0;
gledon();
  DateTime now = rtc.now();
  if(counter4off==1)
  {
  lamp4off=now.hour();
  Serial.print("4 off time:");
 Serial.println(lamp4off);
  counter4on=0;
 counter4off=0;
 lamplife4();
  }
  sta4="OFF";
 digitalWrite(UVLIGHT4,HIGH);
}
void L5ON()
{
  s5=1;
  rledon();
  sensorreadings();
  DateTime now = rtc.now();
  if(counter5on==0)
  {
  lamp5on=now.hour();
  Serial.print("5 on time:");
  Serial.println(lamp5on);
  counter5off=1;
  }
  counter5on=1;
  sta5="ON";
   digitalWrite(UVLIGHT5,LOW);
}
void L5OFF()
{
  s5=0;
  gledon();
  DateTime now = rtc.now();
  if(counter5off==1)
  {
  lamp5off=now.hour();
  Serial.print("5 off time:");
 Serial.println(lamp5off);
  counter5on=0;
 counter5off=0;
 lamplife5();
  }
  sta5="OFF";
   digitalWrite(UVLIGHT5,HIGH);
}
void L6ON()
{
  s6=1;
  rledon();
  sensorreadings();
  DateTime now = rtc.now();
  if(counter6on==0)
  {
  lamp6on=now.hour();
  Serial.print("6 on time:");
  Serial.println(lamp6on);
  counter6off=1;
  }
  counter6on=1;
  sta6="ON";
   digitalWrite(UVLIGHT6,LOW);
}
void L6OFF()
{
  s6=0;
  gledon();
  DateTime now = rtc.now();
  if(counter6off==1)
  {
  lamp6off=now.hour();
  Serial.print("6 off time:");
 Serial.println(lamp6off);
  counter6on=0;
 counter6off=0;
 lamplife6();
  }
  sta6="OFF";
   digitalWrite(UVLIGHT6,HIGH);
}
void L7ON()
{
  s7=1;
  rledon();
  sensorreadings();
  DateTime now = rtc.now();
  if(counter7on==0)
  {
  lamp7on=now.hour();
  Serial.print("7 on time:");
  Serial.println(lamp7on);
  counter7off=1;
  }
  counter7on=1;
  sta7="ON";
   digitalWrite(UVLIGHT7,LOW);
}
void L7OFF()
{
  s7=0;
  gledon();
  DateTime now = rtc.now();
  if(counter7off==1)
  {
  lamp7off=now.hour();
  Serial.print("7 off time:");
 Serial.println(lamp7off);
  counter7on=0;
 counter7off=0;
 lamplife7();
  }
  sta7="OFF";
   digitalWrite(UVLIGHT7,HIGH);
}
void L8ON()
{
  s8=1;
  rledon();
  sensorreadings();
  DateTime now = rtc.now();
  if(counter8on==0)
  {
  lamp8on=now.hour();
  Serial.print("8 on time:");
  Serial.println(lamp8on);
  counter8off=1;
  }
  counter8on=1;
  sta8="ON";
   digitalWrite(UVLIGHT8,LOW);
}
void L8OFF()
{
  s8=0;
  gledon();
  DateTime now = rtc.now();
  if(counter8off==1)
  {
  lamp8off=now.hour();
  Serial.print("8 off time:");
 Serial.println(lamp8off);
  counter8on=0;
 counter8off=0;
 lamplife8();
  }
  sta8="OFF";
   digitalWrite(UVLIGHT8,HIGH);
}

void sensorreadings()
{
 //float voltage_raw1 =   (analogRead(VIN0)/  1023)*5000; 
float voltage_raw1 =   (5.0 / 1023.0)* analogRead(VIN0);// Read the voltage from sensor
float  voltage1 =  voltage_raw1 - QOV + 0.012 ;// 0.000 is a value to make voltage zero when there is no current
float current1 = voltage1 / sensitivity[model];

float voltage_raw2 =   (5.0 / 1023.0)* analogRead(VIN1);// Read the voltage from sensor
float  voltage2 =  voltage_raw2 - QOV + 0.012 ;// 0.000 is a value to make voltage zero when there is no current
float current2 = voltage2 / sensitivity[model];

float voltage_raw3 =   (5.0 / 1023.0)* analogRead(VIN2);// Read the voltage from sensor
float  voltage3 =  voltage_raw3 - QOV + 0.012 ;// 0.000 is a value to make voltage zero when there is no current
  float current3 = voltage3 / sensitivity[model];
  
float voltage_raw4 =   (5.0 / 1023.0)* analogRead(VIN3);// Read the voltage from sensor
float  voltage4 =  voltage_raw4 - QOV + 0.012 ;// 0.000 is a value to make voltage zero when there is no current
  float current4 = voltage4 / sensitivity[model];
  
float voltage_raw5 =   (5.0 / 1023.0)* analogRead(VIN4);// Read the voltage from sensor
float  voltage5 =  voltage_raw5 - QOV + 0.012 ;// 0.000 is a value to make voltage zero when there is no current
  float current5 = voltage5 / sensitivity[model];
  
float voltage_raw6 =   (5.0 / 1023.0)* analogRead(VIN5);// Read the voltage from sensor
float  voltage6 =  voltage_raw6 - QOV + 0.012 ;// 0.000 is a value to make voltage zero when there is no current
  float current6 = voltage6 / sensitivity[model];
  
float voltage_raw7 =   (5.0 / 1023.0)* analogRead(VIN6);// Read the voltage from sensor
float  voltage7 =  voltage_raw7 - QOV + 0.012 ;// 0.000 is a value to make voltage zero when there is no current
  float current7 = voltage7 / sensitivity[model];
  
float voltage_raw8 =   (5.0 / 1023.0)* analogRead(VIN7);// Read the voltage from sensor
float  voltage8 =  voltage_raw8 - QOV + 0.012 ;// 0.000 is a value to make voltage zero when there is no current  
  float current8 = voltage8 / sensitivity[model];     
  
if(abs(current1) > cutOffLimit){
    Serial.print("V1: ");
    Serial.print(voltage1,3);// print voltage with 3 decimal places
    Serial.print("V, I: ");
    Serial.print(current1,2); // print the current with 2 decimal places
    Serial.println("A");
  }
 if((abs(current1) < cutOffLimit) && relay1==HIGH)
  {
    Serial.println("load 1 problem");
    //counter1++;
  }
if(abs(current2) > cutOffLimit){
    Serial.print("V2: ");
    Serial.print(voltage2,3);// print voltage with 3 decimal places
    Serial.print("V, I: ");
    Serial.print(current2,2); // print the current with 2 decimal places
    Serial.println("A");
  }
if((abs(current2) < cutOffLimit) && relay2==true)
  {
     Serial.println("load 1 problem");
  }
if(abs(current3) > cutOffLimit){
    Serial.print("V3: ");
    Serial.print(voltage3,3);// print voltage with 3 decimal places
    Serial.print("V, I: ");
    Serial.print(current3,2); // print the current with 2 decimal places
    Serial.println("A");
  } 
  if((abs(current3) < cutOffLimit) && relay2==true)
  {
     Serial.println("load 3 problem");
  }
if(abs(current4) > cutOffLimit){
    Serial.print("V4: ");
    Serial.print(voltage4,3);// print voltage with 3 decimal places
    Serial.print("V, I: ");
    Serial.print(current4,2); // print the current with 2 decimal places
    Serial.println("A");
  }
  if((abs(current4) < cutOffLimit) && relay2==true)
  {
     Serial.println("load 4 problem");
  }
if(abs(current5) > cutOffLimit){
    Serial.print("V5: ");
    Serial.print(voltage5,3);// print voltage with 3 decimal places
    Serial.print("V, I: ");
    Serial.print(current5,2); // print the current with 2 decimal places
    Serial.println("A");
  }
  if((abs(current5) < cutOffLimit) && relay2==true)
  {
     Serial.println("load 5 problem");
  }
if(abs(current6) > cutOffLimit){
    Serial.print("V6: ");
    Serial.print(voltage6,3);// print voltage with 3 decimal places
    Serial.print("V, I: ");
    Serial.print(current6,2); // print the current with 2 decimal places
    Serial.println("A");
  }
  if((abs(current6) < cutOffLimit) && relay2==true)
  {
     Serial.println("load 6 problem");
  }
if(abs(current7) > cutOffLimit){
    Serial.print("V7: ");
    Serial.print(voltage7,3);// print voltage with 3 decimal places
    Serial.print("V, I: ");
    Serial.print(current7,2); // print the current with 2 decimal places
    Serial.println("A");
  }
  if((abs(current7) < cutOffLimit) && relay2==true)
  {
     Serial.println("load 7 problem");
  }
if(abs(current8) > cutOffLimit){
    Serial.print("V8: ");
    Serial.print(voltage8,3);// print voltage with 3 decimal places
    Serial.print("V, I: ");
    Serial.print(current8,2); // print the current with 2 decimal places
    Serial.println("A");
  }
  if((abs(current8) < cutOffLimit) && relay2==true)
  {
     Serial.println("load 8 problem");
  }
}
void lamplife1()
{
   if(lamp1on>lamp1off)
    LAMP1=lamp1on-lamp1off;
    if(lamp1on<lamp1off)
    LAMP1=lamp1off-lamp1on;
    Serial.println(LAMP1);
    LAMP1saveontime();
    EepromReadLAMP1save();
}
void lamplife2()
{
   if(lamp2on>lamp2off)
    LAMP2=lamp2on-lamp2off;
    if(lamp2on<lamp2off)
    LAMP2=lamp2off-lamp2on;
    Serial.println(LAMP2);
    LAMP2saveontime();
    EepromReadLAMP2save();
}
void lamplife3()
{
   if(lamp3on>lamp3off)
    LAMP3=lamp3on-lamp3off;
    if(lamp3on<lamp3off)
    LAMP3=lamp3off-lamp3on;
    Serial.println(LAMP3);
    LAMP3saveontime();
    EepromReadLAMP3save();
}
void lamplife4()
{
   if(lamp4on>lamp4off)
    LAMP4=lamp4on-lamp4off;
    if(lamp4on<lamp4off)
    LAMP4=lamp4off-lamp4on;
    Serial.println(LAMP4);
    LAMP4saveontime();
    EepromReadLAMP4save();
}
void lamplife5()
{
   if(lamp5on>lamp5off)
    LAMP5=lamp5on-lamp5off;
    if(lamp5on<lamp5off)
    LAMP5=lamp5off-lamp5on;
    Serial.println(LAMP5);
    LAMP5saveontime();
    EepromReadLAMP5save();
}
void lamplife6()
{
   if(lamp6on>lamp6off)
    LAMP6=lamp6on-lamp6off;
    if(lamp6on<lamp6off)
    LAMP6=lamp6off-lamp6on;
    Serial.println(LAMP6);
    LAMP6saveontime();
    EepromReadLAMP6save();
}
void lamplife7()
{
   if(lamp7on>lamp7off)
    LAMP7=lamp7on-lamp7off;
    if(lamp7on<lamp7off)
    LAMP7=lamp7off-lamp7on;
    Serial.println(LAMP7);
    LAMP7saveontime();
    EepromReadLAMP7save();
}
void lamplife8()
{
   if(lamp8on>lamp8off)
    LAMP8=lamp8on-lamp8off;
    if(lamp8on<lamp8off)
    LAMP8=lamp8off-lamp8on;
    Serial.println(LAMP8);
    LAMP8saveontime();
    EepromReadLAMP8save();
}
void RESET()
{
 EEPROM.write(EepromLAMP1on,1);
 EEPROM.write(EepromLAMP2on,1);
 EEPROM.write(EepromLAMP3on,1);
 EEPROM.write(EepromLAMP4on,1);
 EEPROM.write(EepromLAMP5on,1);
 EEPROM.write(EepromLAMP6on,1);
 EEPROM.write(EepromLAMP7on,1);
 EEPROM.write(EepromLAMP8on,1);
    u8g2.drawFrame(0, 0, 128, 64);
    u8g2.drawStr(16, 10, "    MODULE RESET    ");
    u8g2.drawStr(0,24, " MODULE");
    u8g2.setCursor(28, 24);
    u8g2.drawStr(45,24,"RESET SUCCESSFUL");
    readlife();
}
void RESET1()
{
 EEPROM.write(EepromLAMP1on,1);
    u8g2.drawFrame(0, 0, 128, 64);
    u8g2.drawStr(16, 10, "    MODULE RESET    ");
    u8g2.drawStr(0,24, " MODULE 1");
    u8g2.setCursor(28, 24);
    u8g2.drawStr(48,24,"RESET SUCCESS");
    readlife();
}
void RESET2()
{
 
 EEPROM.write(EepromLAMP2on,1);
    u8g2.drawFrame(0, 0, 128, 64);
    u8g2.drawStr(16, 10, "    MODULE RESET    ");
    u8g2.drawStr(0,24, " MODULE 2");
    u8g2.setCursor(28, 24);
    u8g2.drawStr(48,24,"RESET SUCCESS");
    readlife();
}
void RESET3()
{
 
 EEPROM.write(EepromLAMP3on,1);
     u8g2.drawFrame(0, 0, 128, 64);
    u8g2.drawStr(16, 10, "    MODULE RESET    ");
    u8g2.drawStr(0,24, " MODULE 3");
    u8g2.setCursor(28, 24);
    u8g2.drawStr(48,24,"RESET SUCCESS");
    readlife();
}
void RESET4()
{
 
 EEPROM.write(EepromLAMP4on,1);
     u8g2.drawFrame(0, 0, 128, 64);
    u8g2.drawStr(16, 10, "    MODULE RESET    ");
    u8g2.drawStr(0,24, " MODULE 4");
    u8g2.setCursor(28, 24);
    u8g2.drawStr(48,24,"RESET SUCCESS");
    readlife();
}
void RESET5()
{
  EEPROM.write(EepromLAMP5on,1);
     u8g2.drawFrame(0, 0, 128, 64);
    u8g2.drawStr(16, 10, "    MODULE RESET    ");
    u8g2.drawStr(0,24, " MODULE 5");
    u8g2.setCursor(28, 24);
    u8g2.drawStr(48,24,"RESET SUCCESS");
    readlife();
}
void RESET6()
{
  EEPROM.write(EepromLAMP6on,1);
    u8g2.drawFrame(0, 0, 128, 64);
    u8g2.drawStr(16, 10, "    MODULE RESET    ");
    u8g2.drawStr(0,24, " MODULE 6");
    u8g2.setCursor(28, 24);
    u8g2.drawStr(48,24,"RESET SUCCESS");
    readlife();
}
void RESET7()
{
 
 EEPROM.write(EepromLAMP7on,1);
    u8g2.drawFrame(0, 0, 128, 64);
    u8g2.drawStr(16, 10, "    MODULE RESET    ");
    u8g2.drawStr(0,24, " MODULE 7");
    u8g2.setCursor(28, 24);
    u8g2.drawStr(48,24,"RESET SUCCESS");
    readlife();
}
void RESET8()
{
   EEPROM.write(EepromLAMP8on,1);
    u8g2.drawFrame(0, 0, 128, 64);
    u8g2.drawStr(16, 10, "    MODULE RESET    ");
    u8g2.drawStr(0,24, " MODULE 8");
    u8g2.setCursor(28, 24);
    u8g2.drawStr(48,24,"RESET SUCCESS");
    readlife();
}
void rledon()
{
  digitalWrite(rled,LOW);
  digitalWrite(gled,HIGH);
  digitalWrite(bled,HIGH);
}
void gledon()
{
  digitalWrite(rled,HIGH);
  digitalWrite(gled,LOW);
  digitalWrite(bled,HIGH);
}
void bledon()
{
digitalWrite(rled,HIGH);
  digitalWrite(gled,HIGH);
  digitalWrite(bled,LOW);
}
