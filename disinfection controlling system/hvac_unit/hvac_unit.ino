#include <Arduino.h>
#include <menu.h>
#include <menuIO/u8g2Out.h>
#include <menuIO/encoderIn.h>
#include <menuIO/keyIn.h>
#include <menuIO/chainStream.h>
#include <menuIO/serialOut.h>
#include <menuIO/serialIn.h>
#include <menuIO/U8GLibOut.h>
#include <TimerOne.h>
#include <ClickEncoder.h>
#include <menuIO/clickEncoderIn.h>
#include <MemoryFree.h>
#include <SD.h>
#include <TaskScheduler.h>
#include <ArduinoJson.h>
#include "DHT.h"
#include <RTClib.h>
#include <RTClib.h>
#include <Time.h>
#include <TimeLib.h>
char t[32];
#include "Countimer.h"

 int Year =2021;
 int Month=01;
 int Date=06;

 int Hr =00;
 int Min =00;

RTC_DS3231 rtc;
char daysOfTheWeek[7][12] = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};
// LCD /////////////////////////////////////////
#define RS 16
#define EN 17
LiquidCrystal lcd(RS, EN, 23, 25, 27, 29);

// Encoder /////////////////////////////////////
#define encA 33
#define encB 31
//this encoder has a button here
#define encBtn 35
 //--------------EEPROM ADDRESS-----------------
int Eepromhur1 = 200;
int Eeprommin1 = 203;
int EepromhurD1 = 206;
int EepromminD1 = 209;

int Eepromhur2 = 212;
int Eeprommin2 = 215;
int EepromhurD2 = 218;
int EepromminD2 = 225;

int Eepromhur3 = 224;
int Eeprommin3 = 227;
int EepromhurD3 = 230;
int EepromminD3 = 233;

int Eepromhur4 = 236;
int Eeprommin4 = 238;
int EepromhurD4 = 241;
int EepromminD4 = 244;

int Eepromhur5 = 247;
int Eeprommin5 = 250;
int EepromhurD5 = 253;
int EepromminD5 = 256;

int Eepromhur6 = 259;
int Eeprommin6 = 261;
int EepromhurD6 = 264;
int EepromminD6 = 267;

int Eepromhur7 = 270;
int Eeprommin7 = 273;
int EepromhurD7 = 276;
int EepromminD7 = 279;

int Eepromhur8 = 282;
int Eeprommin8 = 285;
int EepromhurD8 = 288;
int EepromminD8 = 295;
int HOUR,MIN;
int R0=2,R1=3,R2=4,R3=5,R4=6,R5=7,R6=8,R7=9;
int const u1=A0,u2=A1,u3=A2,u4=A3,u5=A4,u6=A5,u7=A6,u_8=A7;
int p1=34,p2=36,p3=38,p4=40,p5=42,p6=44,p7=46,p8=48;
int S1Hr,S1Min,S2Hr,S2Min,S3Hr,S3Min,S4Hr,S4Min,S5Hr,S5Min,S6Hr,S6Min,S7Hr,S7Min,S8Hr,S8Min;
int D1Hr,D1Min,D2Hr,D2Min,D3Hr,D3Min,D4Hr,D4Min,D5Hr,D5Min,D6Hr,D6Min,D7Hr,D7Min,D8Hr,D8Min;
int counter1=111,counter2=222,counter3=333,counter4=444,counter5=555,counter6=666,counter7=777,counter8=888;
float UV_index,UV_index1,UV_index2,UV_index3,UV_index4,UV_index5,UV_index6,UV_index7;
bool ScheduleAuto;
bool count;
String st1="OFF",st2="OFF",st3="OFF",st4="OFF",st5="OFF",st6="OFF",st7="OFF",st8="OFF";
// Callback methods prototypes
void t1Callback();
void t2Callback();
//void t3Callback();
Scheduler runner;

//Tasks
Task t4();
Task t1(2000, TASK_FOREVER, &t1Callback);
Task t2(3000, TASK_FOREVER, &t2Callback);
//Task t3(5000, TASK_FOREVER, &t3Callback);


void unit1read()
{
  S1Hr=EEPROM.read(Eepromhur1);
  S1Min=EEPROM.read(Eeprommin1);
  D1Hr=EEPROM.read(EepromhurD1);
  D1Min=EEPROM.read(EepromminD1);
}
void unit2read()
{
  S2Hr=EEPROM.read(Eepromhur2);
  S2Min=EEPROM.read(Eeprommin2);
  D2Hr=EEPROM.read(EepromhurD2);
  D2Min=EEPROM.read(EepromminD2);
}
void unit3read()
{
  S3Hr=EEPROM.read(Eepromhur3);
  S3Min=EEPROM.read(Eeprommin3);
  D3Hr=EEPROM.read(EepromhurD3);
  D3Min=EEPROM.read(EepromminD3);
}
void unit4read()
{
  S4Hr=EEPROM.read(Eepromhur4);
  S4Min=EEPROM.read(Eeprommin4);
  D4Hr=EEPROM.read(EepromhurD4);
  D4Min=EEPROM.read(EepromminD4);
}
void unit5read()
{
  S5Hr=EEPROM.read(Eepromhur5);
  S5Min=EEPROM.read(Eeprommin5);
  D5Hr=EEPROM.read(EepromhurD5);
  D5Min=EEPROM.read(EepromminD5);
}
void unit6read()
{
  S6Hr=EEPROM.read(Eepromhur6);
  S6Min=EEPROM.read(Eeprommin6);
  D6Hr=EEPROM.read(EepromhurD6);
  D6Min=EEPROM.read(EepromminD6);
}
void unit7read()
{
  S7Hr=EEPROM.read(Eepromhur7);
  S7Min=EEPROM.read(Eeprommin7);
  D7Hr=EEPROM.read(EepromhurD7);
  D7Min=EEPROM.read(EepromminD7);
}
void unit8read()
{
  S8Hr=EEPROM.read(Eepromhur8);
  S8Min=EEPROM.read(Eeprommin8);
  D8Hr=EEPROM.read(EepromhurD8);
  D8Min=EEPROM.read(EepromminD8);
}
result Shift1Clockhr() {
  EEPROM.write(Eepromhur1, S1Hr);
   EEPROM.write(Eeprommin1, S1Min);
   EEPROM.write(EepromhurD1, D1Hr);
   EEPROM.write(EepromminD1, D1Min);
  return proceed;
}
result Shift2Clockhr() {
  EEPROM.write(Eepromhur2, S2Hr);
   EEPROM.write(Eeprommin2, S2Min);
    EEPROM.write(EepromhurD2, D2Hr);
     EEPROM.write(EepromminD2, D2Min);
  return proceed;
}
result Shift3Clockhr() {
  EEPROM.write(Eepromhur3, S3Hr);
  EEPROM.write(Eeprommin3, S3Min);
  EEPROM.write(EepromhurD3, D3Hr);
   EEPROM.write(EepromminD3, D3Min);
  return proceed;
}
result Shift4Clockhr() {
  EEPROM.write(Eepromhur4, S4Hr);
  EEPROM.write(Eeprommin4, S4Min);
  EEPROM.write(EepromhurD4, D4Hr);
   EEPROM.write(EepromminD4, D4Min);
  return proceed;
}
result Shift5Clockhr() {
  EEPROM.write(Eepromhur5, S5Hr);
  EEPROM.write(Eeprommin5, S5Min);
  EEPROM.write(EepromhurD5, D5Hr);
  EEPROM.write(EepromminD5, D5Min);
  return proceed;
}
result Shift6Clockhr() {
  EEPROM.write(Eepromhur6, S6Hr);
  EEPROM.write(Eeprommin6, S6Min);
  EEPROM.write(EepromhurD6, D6Hr);
   EEPROM.write(EepromminD6, D6Min);
  return proceed;
}
result Shift7Clockhr() {
  EEPROM.write(Eepromhur7, S7Hr);
  EEPROM.write(Eeprommin7, S7Min);
  EEPROM.write(EepromhurD7, D7Hr);
  EEPROM.write(EepromminD7, D7Min);
  return proceed;
}
result Shift8Clockhr() {
  EEPROM.write(Eepromhur8, S8Hr);
  EEPROM.write(Eeprommin8, S8Min);
  EEPROM.write(EepromhurD8, D8Hr);
  EEPROM.write(EepromminD8, D8Min);
  return proceed;
}
ClickEncoder clickEncoder(encA,encB,encBtn,2);
ClickEncoderStream encStream(clickEncoder,1);
MENU_INPUTS(in,&encStream);
void timerIsr() {clickEncoder.service();}

//a keyboard with only one key as the encoder button
keyMap encBtn_map[]={{-encBtn,defaultNavCodes[enterCmd].ch}};//negative pin numbers use internal pull-up, this is on when low
keyIn<1> encButton(encBtn_map);//1 is the number of keys

//input from the encoder + encoder button + serial
serialIn serial(Serial);

result doAlert(eventMask e, prompt &item);
result showEvent(eventMask e,navNode& nav,prompt& item) {
//  Serial.print("event: ");
//  Serial.println(e);
  return proceed;
}

int test=55;
result action1(eventMask e,navNode& nav, prompt &item) {
//  Serial.print("action1 event: ");
//  Serial.print(e);
//  Serial.println(", proceed menu");
//  Serial.flush();
  return proceed;
}

result action2(eventMask e,navNode& nav, prompt &item) {
//  Serial.print("action2 event: ");
//  Serial.print(e);
//  Serial.print(", quiting menu.");
//  Serial.flush();
  return quit;
}
//TOGGLE(ScheduleAuto,AutoSchedule,"Auto Schedule :",doNothing,noEvent,noStyle//,doExit,enterEvent,noStyle
//  ,VALUE("STOP",true,doNothing,enterEvent)
//  ,VALUE("START",false,doExit,noEvent)
//);

int ledCtrl=LOW;

result myLedOn() {
  ledCtrl=HIGH;
  return proceed;
}
result myLedOff() {
  ledCtrl=LOW;
  return proceed;
}

//customizing a prompt look!
//by extending the prompt class
class altPrompt:public prompt {
public:
  altPrompt(constMEM promptShadow& p):prompt(p) {}
  Used printTo(navRoot &root,bool sel,menuOut& out, idx_t idx,idx_t len,idx_t) override {
    return out.printRaw(F("special prompt!"),len);;
  }
};
result showEvent1() {
  //ledCtrl=HIGH;
  lcd.clear();
  lcd.print("unit 1");
  return proceed;
}
void nowtime()
{
  count=true;
  t1.enable();
}
void t2Callback()
{
 
    DateTime now = rtc.now();
    HOUR=now.hour();
    MIN=now.minute();
    lcd.clear(); 
    lcd.setCursor(1, 0);
    lcd.print("TIME"); 
    lcd.setCursor(10, 0);
    lcd.print("DATE"); 
    lcd.setCursor(1, 1);
    lcd.print(HOUR);
    lcd.print(":");
    lcd.print(MIN);
    lcd.setCursor(4,1);
    lcd.print(now.minute());
    lcd.setCursor(8,1);
    lcd.print( now.day());
    lcd.print("/");
    lcd.print( now.month());
    lcd.print("/");
    lcd.print( now.year());
//    Serial.print(HOUR, DEC);
  //  Serial.print(':');
    //Serial.print(MIN, DEC);
    //Serial.println();
    delay(1000);
    UVSTATUS();
    UVSTATUS1();
    sending();
//------------UNIT1---------------
            
}
void UVSTATUS()
{
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("1-");
  lcd.setCursor(2,0);
  lcd.print("S:");
  lcd.setCursor(4,0);
  lcd.print(st1);
  lcd.setCursor(8,0);
  lcd.print("F:");
  lcd.setCursor(15,0);
  lcd.print("I:");
  
  lcd.setCursor(0,1);
  lcd.print("2-");
  lcd.setCursor(2,1);
  lcd.print("S:");
  lcd.setCursor(4,1);
  lcd.print(st2);
  lcd.setCursor(8,1);
  lcd.print("F:");
  lcd.setCursor(15,1);
  lcd.print("I:");

  lcd.setCursor(0,2);
  lcd.print("3-");
  lcd.setCursor(2,2);
  lcd.print("S:");
  lcd.setCursor(4,2);
  lcd.print(st3);
  lcd.setCursor(8,2);
  lcd.print("F:");
  lcd.setCursor(15,2);
  lcd.print("I:");

  lcd.setCursor(0,3);
  lcd.print("4-");
  lcd.setCursor(2,3);
  lcd.print("S:");
  lcd.setCursor(4,3);
  lcd.print(st4);
  lcd.setCursor(8,3);
  lcd.print("F:");
  lcd.setCursor(15,3);
  lcd.print("I:");
  
  int sensor_value = analogRead(u1); // Get raw sensor reading
  float volts = sensor_value * 5.0 / 1024.0;
  UV_index = volts * 1.64;
  //Serial.print ("  UV Index: ");
  //Serial.println (UV_index);
  lcd.setCursor(10,0);
  lcd.print(UV_index);
 
  int sensor = digitalRead(p1); // Get raw sensor reading
  //Serial.print ("person: ");
  //Serial.println (sensor);
  
  if(sensor==HIGH)
  {
   counter1++;
  lcd.setCursor(17,0);
  lcd.print(counter1);
  } 
  else
  {
 lcd.setCursor(17,0);
  lcd.print(counter1);
  }  
  int sensor_value1 = analogRead(u2); // Get raw sensor reading
  float volts1 = sensor_value1 * 5.0 / 1024.0;
   UV_index1 = volts1 * 1.64;
  //Serial.print ("UV Index: ");
  //Serial.println (UV_index1);
  lcd.setCursor(10,1);
  lcd.print(UV_index1);

  int sensor1 = digitalRead(p2); // Get raw sensor reading
  //Serial.print ("person: ");
  //Serial.println (sensor1);
  
  if(sensor1==HIGH)
  {
   counter2++;
  lcd.setCursor(17,1);
  lcd.print(counter2);
  } 
  else
  {
 lcd.setCursor(17,1);
  lcd.print(counter2);
  }  
  int sensor_value2 = analogRead(u3); // Get raw sensor reading
  float volts2 = sensor_value2 * 5.0 / 1024.0;
   UV_index2 = volts2 * 1.64;
  //Serial.print ("  UV Index: ");
  //Serial.println (UV_index2);
  lcd.setCursor(10,2);
  lcd.print(UV_index2);

  int sensor2 = digitalRead(p3); // Get raw sensor reading
  //Serial.print ("person: ");
  //Serial.println (sensor2);
  
  if(sensor2==HIGH)
  {
   counter3++;
  lcd.setCursor(17,2);
  lcd.print(counter3);
  } 
  else
  {
 lcd.setCursor(17,2);
  lcd.print(counter3);
  }  
  int sensor_value3 = analogRead(u4); // Get raw sensor reading
  float volts3 = sensor_value3 * 5.0 / 1024.0;
   UV_index3 = volts3 * 1.64;
  //Serial.print ("  UV Index: ");
  //Serial.println (UV_index3);
  lcd.setCursor(10,3);
  lcd.print(UV_index3);

   int sensor3 = digitalRead(p4); // Get raw sensor reading
  //Serial.print ("person: ");
  //Serial.println (sensor3);
  
  if(sensor3==HIGH)
  {
   counter4++;
  lcd.setCursor(17,3);
  lcd.print(counter4);
  } 
  else
  {
 lcd.setCursor(17,3);
  lcd.print(counter4);
  }  
  delay(1500);  // Take reading every second

  
}
void UVSTATUS1() 
{
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("5-");
  lcd.setCursor(2,0);
  lcd.print("S:");
  lcd.setCursor(4,0);
  lcd.print(st5);
  lcd.setCursor(8,0);
  lcd.print("F:");
  lcd.setCursor(15,0);
  lcd.print("I:");
  
  lcd.setCursor(0,1);
  lcd.print("6-");
  lcd.setCursor(2,1);
  lcd.print("S:");
  lcd.setCursor(4,1);
  lcd.print(st6);
  lcd.setCursor(8,1);
  lcd.print("F:");
  lcd.setCursor(15,1);
  lcd.print("I:");

  lcd.setCursor(0,2);
  lcd.print("7-");
  lcd.setCursor(2,2);
  lcd.print("S:");
  lcd.setCursor(4,2);
  lcd.print(st7);
  lcd.setCursor(8,2);
  lcd.print("F:");
  lcd.setCursor(15,2);
  lcd.print("I:");

  lcd.setCursor(0,3);
  lcd.print("8-");
  lcd.setCursor(2,3);
  lcd.print("S:");
  lcd.setCursor(4,3);
  lcd.print(st8);
  lcd.setCursor(8,3);
  lcd.print("F:");
  lcd.setCursor(15,3);
  lcd.print("I:");
  
  int sensor_value4 = analogRead(u5); // Get raw sensor reading
  float volts4 = sensor_value4 * 5.0 / 1024.0;
  UV_index4 = volts4 * 1.64;
  //Serial.print ("  UV Index: ");
  //Serial.println (UV_index4);
  lcd.setCursor(10,0);
  lcd.print(UV_index4);
 
  int sensor4 = digitalRead(p5); // Get raw sensor reading
  //Serial.print ("person: ");
  //Serial.println (sensor4);
  
  if(sensor4==HIGH)
  {
   counter5++;
  lcd.setCursor(17,0);
  lcd.print(counter5);
  } 
  else
  {
 lcd.setCursor(17,0);
  lcd.print(counter5);
  }  
  int sensor_value5 = analogRead(u6); // Get raw sensor reading
  float volts5 = sensor_value5 * 5.0 / 1024.0;
  UV_index5 = volts5 * 1.64;
  //Serial.print ("UV Index: ");
  //Serial.println (UV_index5);
  lcd.setCursor(10,1);
  lcd.print(UV_index5);

  int sensor5 = digitalRead(p6); // Get raw sensor reading
  //Serial.print ("person: ");
  //Serial.println (sensor5);
  
  if(sensor5==HIGH)
  {
   counter6++;
  lcd.setCursor(17,1);
  lcd.print(counter6);
  } 
  else
  {
 lcd.setCursor(17,1);
  lcd.print(counter6);
  }  
  int sensor_value6 = analogRead(u7); // Get raw sensor reading
  float volts6 = sensor_value6 * 5.0 / 1024.0;
  UV_index6 = volts6 * 1.64;
  //Serial.print ("  UV Index: ");
  //Serial.println (UV_index6);
  lcd.setCursor(10,2);
  lcd.print(UV_index6);

  int sensor6 = digitalRead(p7); // Get raw sensor reading
  //Serial.print ("person: ");
  //Serial.println (sensor6);
  
  if(sensor6==HIGH)
  {
   counter7++;
  lcd.setCursor(17,2);
  lcd.print(counter7);
  } 
  else
  {
 lcd.setCursor(17,2);
  lcd.print(counter7);
  }
    
 int sensor_value7 = analogRead(u_8); // Get raw sensor reading
  float volts7 = sensor_value7 * 5.0 / 1024.0;
  UV_index7 = volts7 * 1.64;
  lcd.setCursor(10,3);
  lcd.print(UV_index7);

   int sensor7 = digitalRead(p8); // Get raw sensor reading
  //Serial.print ("person: ");
  //Serial.println (sensor7);
  
  if(sensor7==HIGH)
  {
   counter8++;
  lcd.setCursor(17,3);
  lcd.print(counter8);
  } 
  else
  {
 lcd.setCursor(17,3);
  lcd.print(counter8);
  }  
  delay(1500);  // Take reading every second
}
void sending()
{
StaticJsonBuffer<1000> jsonBuffer;
JsonObject& data= jsonBuffer.createObject();
data["STATUS1"] =st1;
data["UVC INTENSITY1"] =UV_index; 
data["INTRODER1"] =counter1;

data["STATUS2"] =st2;
data["UVC INTENSITY2"] =UV_index1; 
data["INTRODER2"] =counter2;

data["STATUS3"] =st3;
data["UVC INTENSITY3"] =UV_index2; 
data["INTRODER3"] =counter3;

data["STATUS4"] =st4;
data["UVC INTENSITY4"] =UV_index3; 
data["INTRODER4"] =counter4;

data["STATUS5"] =st5;
data["UVC INTENSITY5"] =UV_index4; 
data["INTRODER5"] =counter5;

data["STATUS6"] =st6;
data["UVC INTENSITY6"] =UV_index5; 
data["INTRODER6"] =counter6;

data["STATUS7"] =st7;
data["UVC INTENSITY7"] =UV_index6; 
data["INTRODER7"] =counter7;

data["STATUS8"] =st8;
data["UVC INTENSITY8"] =UV_index7; 
data["INTRODER8"] =counter8;

data.printTo(Serial3);
jsonBuffer.clear(); 
}
void scheduling()
  {
  ScheduleAuto=true;
  }
 void t1Callback(){    
    t2Callback();
 if(HOUR==S1Hr && MIN==S1Min) 
  {
    st1="ON ";
   digitalWrite(R0,HIGH);
  }   
  if(HOUR==S1Hr+D1Hr && MIN==S1Min+D1Min)
  { 
    st1="OFF";
   digitalWrite(R0,LOW);   
  }
  if(HOUR==S2Hr && MIN==S2Min)
  {
    st2="ON ";
   digitalWrite(R1,HIGH);
  }   
  if(HOUR==S2Hr+D2Hr && MIN==S2Min+D2Min)
  {
    st2="OFF";
   digitalWrite(R1,LOW);   
  }
  if(HOUR==S3Hr && MIN==S3Min)
  {
    st3="ON ";
   digitalWrite(R2,HIGH);
  }   
 if(HOUR==S3Hr+D3Hr && MIN==S3Min+D3Min)
  {
    st3="OFF";
   digitalWrite(R2,LOW);   
  }
  if(HOUR==S4Hr && MIN==S4Min)
  {
    st4="ON ";
   digitalWrite(R3,HIGH); 
  }  
 if(HOUR==S4Hr+D4Hr && MIN==S4Min+D4Min)
  {
    st4="OFF";
   digitalWrite(R3,LOW);   
  }
  if(HOUR==S5Hr && MIN==S5Min)
  {
    st5="ON ";
   digitalWrite(R4,HIGH);
  }   
 if(HOUR==S5Hr+D5Hr && MIN==S5Min+D5Min)
  {
    st5="OFF";
   digitalWrite(R4,LOW);   
  }
  if(HOUR==S6Hr && MIN==S6Min)
  {
    st6="ON ";
   digitalWrite(R5,HIGH); 
  }  
  if(HOUR==S6Hr+D6Hr && MIN==S6Min+D6Min)
  {
    st6="OFF";
   digitalWrite(R5,LOW);   
  }
  if(HOUR==S7Hr && MIN==S7Min)
  {
    st7="ON ";
   digitalWrite(R6,HIGH);
  }   
  if(HOUR==S7Hr+D7Hr && MIN==S7Min+D7Min)
  {
    st7="OFF";
   digitalWrite(R6,LOW);   
  }
  if(HOUR==S8Hr && MIN==S8Min)
  {
    st8="ON ";
   digitalWrite(R7,HIGH); 
  }  
  if(HOUR==S8Hr+D8Hr && MIN==S8Min+D8Min)
  {
    st8="OFF";
   digitalWrite(R7,LOW);   
  }
    
  }
  result Home(eventMask e, prompt &item) {
   //t2Callback();
   nowtime();
  }
//----------------------menu setting--------------------
MENU(monitior,"Monitior",showEvent,enterEvent,wrapStyle
  ,OP("Time",Home,enterEvent)
  ,EXIT("<Back")
);
MENU(WIFI_CONNECTION,"WIFI CONNECTION",showEvent,enterEvent,wrapStyle
  ,OP("Sub1",showEvent,enterEvent)
  ,OP("Sub2",showEvent,enterEvent)
  ,OP("Sub3",showEvent,enterEvent)
  ,EXIT("<Back")
);
altMENU(menu, TS1, "T", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(S1Hr, "", "(HH)", 0, 23, 1, 0, Shift1Clockhr, enterEvent, wrapStyle)
        , FIELD(S1Min, "", "(MM)", 0, 59, 10, 1, Shift1Clockhr, enterEvent, wrapStyle)
);
altMENU(menu, DS1, "T", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(D1Hr, "", "(HH)", 0, 23, 1, 0, Shift1Clockhr, enterEvent, wrapStyle)
        , FIELD(D1Min, "", "(MM)", 0, 59, 10, 1, Shift1Clockhr, enterEvent, wrapStyle)
);
altMENU(menu, TS2, "T", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(S2Hr, "", "(HH)", 0, 23, 1, 0, Shift2Clockhr, enterEvent, wrapStyle)
        , FIELD(S2Min, "", "(MM)", 0, 59, 10, 1,Shift2Clockhr, enterEvent, wrapStyle)
);
altMENU(menu, DS2, "T", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(D2Hr, "", "(HH)", 0, 23, 1, 0, Shift2Clockhr, enterEvent, wrapStyle)
        , FIELD(D2Min, "", "(MM)", 0, 59, 10, 1,Shift2Clockhr, enterEvent, wrapStyle)
);
altMENU(menu, TS3, "T", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(S3Hr, "", "(HH)", 0, 23, 1, 0, Shift3Clockhr, enterEvent, wrapStyle)
        , FIELD(S3Min, "", "(MM)", 0, 59, 10, 1,Shift3Clockhr, enterEvent, wrapStyle)
);
altMENU(menu, DS3, "T", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(D3Hr, "", "(HH)", 0, 23, 1, 0, Shift3Clockhr, enterEvent, wrapStyle)
        , FIELD(D3Min, "", "(MM)", 0, 59, 10, 1,Shift3Clockhr, enterEvent, wrapStyle)
);
altMENU(menu, TS4, "T", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(S4Hr, "", "(HH)", 0, 23, 1, 0, Shift4Clockhr, enterEvent, wrapStyle)
        , FIELD(S4Min, "", "(MM)", 0, 59, 10, 1, Shift4Clockhr, enterEvent, wrapStyle)
);
altMENU(menu, DS4, "T", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(D4Hr, "", "(HH)", 0, 23, 1, 0, Shift4Clockhr, enterEvent, wrapStyle)
        , FIELD(D4Min, "", "(MM)", 0, 59, 10, 1,Shift4Clockhr, enterEvent, wrapStyle)
);
altMENU(menu, TS5, "T", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(S5Hr, "", "(HH)", 0, 23, 1, 0, Shift5Clockhr, enterEvent, wrapStyle)
        , FIELD(S5Min, "", "(MM)", 0, 59, 10, 1, Shift5Clockhr, enterEvent, wrapStyle)
);
altMENU(menu, DS5, "T", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(D5Hr, "", "(HH)", 0, 23, 1, 0,  Shift5Clockhr, enterEvent, wrapStyle)
        , FIELD(D5Min, "", "(MM)", 0, 59, 10, 1, Shift5Clockhr, enterEvent, wrapStyle)
);
altMENU(menu, TS6, "T", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(S6Hr, "", "(HH)", 0, 23, 1, 0, Shift6Clockhr, enterEvent, wrapStyle)
        , FIELD(S6Min, "", "(MM)", 0, 59, 10, 1,Shift6Clockhr, enterEvent, wrapStyle)
);
altMENU(menu, DS6, "T", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(D6Hr, "", "(HH)", 0, 23, 1, 0,Shift6Clockhr, enterEvent, wrapStyle)
        , FIELD(D6Min, "", "(MM)", 0, 59, 10, 1,Shift6Clockhr, enterEvent, wrapStyle)
);
altMENU(menu, TS7, "T", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(S7Hr, "", "(HH)", 0, 23, 1, 0, Shift7Clockhr, enterEvent, wrapStyle)
        , FIELD(S7Min, "", "(MM)", 0, 59, 10, 1,Shift7Clockhr, enterEvent, wrapStyle)
);
altMENU(menu, DS7, "T", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(D7Hr, "", "(HH)", 0, 23, 1, 0,Shift7Clockhr, enterEvent, wrapStyle)
        , FIELD(D7Min, "", "(MM)", 0, 59, 10, 1,Shift7Clockhr, enterEvent, wrapStyle)
);
altMENU(menu, TS8, "T", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(S8Hr, "", "(HH)", 0, 23, 1, 0, Shift8Clockhr, enterEvent, wrapStyle)
        , FIELD(S8Min, "", "(MM)", 0, 59, 10, 1,Shift8Clockhr, enterEvent, wrapStyle)
);
altMENU(menu, DS8, "T", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
        , FIELD(D8Hr, "", "(HH)", 0, 23, 1, 0, Shift8Clockhr, enterEvent, wrapStyle)
        , FIELD(D8Min, "", "(MM)", 0, 59, 10, 1,Shift8Clockhr, enterEvent, wrapStyle)
);

MENU(UNIT_1, "unit-1 time set", showEvent, enterEvent, wrapStyle 
   , SUBMENU(TS1)  
   , SUBMENU(DS1)
   ,OP("Conform",unit1read,enterEvent)
   , EXIT("<Back")
);
MENU(UNIT_2, "unit-2 time set", showEvent,enterEvent, wrapStyle
   , SUBMENU(TS2)  
   , SUBMENU(DS2)
   ,OP("Conform",unit2read,enterEvent)
   , EXIT("<Back")
);
MENU(UNIT_3, "unit-3 time set", showEvent, enterEvent, wrapStyle 
   , SUBMENU(TS3)  
   , SUBMENU(DS3)
   ,OP("Conform",unit3read,enterEvent)
   , EXIT("<Back")
);
MENU(UNIT_4, "unit-4 time set", showEvent,enterEvent, wrapStyle
   , SUBMENU(TS4)  
   , SUBMENU(DS4)
   ,OP("Conform",unit4read,enterEvent)
   , EXIT("<Back")
);
MENU(UNIT_5, "unit-5 time set", showEvent,enterEvent, wrapStyle 
   , SUBMENU(TS5)  
   , SUBMENU(DS5)
   ,OP("Conform",unit5read,enterEvent)
   , EXIT("<Back")
);
MENU(UNIT_6, "unit-6 time set", showEvent,enterEvent, wrapStyle 
   , SUBMENU(TS6)  
   , SUBMENU(DS6)
   ,OP("Conform",unit6read,enterEvent)
   , EXIT("<Back")
);
MENU(UNIT_7, "unit-7 time set", showEvent,enterEvent, wrapStyle
   , SUBMENU(TS7)  
   , SUBMENU(DS7)
   ,OP("Conform",unit7read,enterEvent)
   , EXIT("<Back")
);
MENU(UNIT_8,"unit-8 time set", showEvent,enterEvent, wrapStyle 
   , SUBMENU(TS8)  
   , SUBMENU(DS8)
   ,OP("Conform",unit8read,enterEvent)
   , EXIT("<Back")
);
//altMENU(menu, START, "T", showEvent, enterEvent, noStyle, (systemStyles)(_asPad | Menu::_menuData | Menu::_canNav | _parentDraw)
//        , FIELD(START, t1Callback, enterEvent, wrapStyle)
//);
MENU(timer_setting,"Timer Setting",showEvent,enterEvent,wrapStyle
  ,SUBMENU(UNIT_1)
  ,SUBMENU(UNIT_2)
  ,SUBMENU(UNIT_3)
  ,SUBMENU(UNIT_4)
  ,SUBMENU(UNIT_5)
  ,SUBMENU(UNIT_6)
  ,SUBMENU(UNIT_7)
  ,SUBMENU(UNIT_8)
  //,SUBMENU(START)
  ,OP("START",scheduling,enterEvent)
  ,EXIT("<Back") 
);
MENU(mainMenu,"Main menu",doNothing,noEvent,wrapStyle
     ,SUBMENU(monitior)
     ,SUBMENU(timer_setting)
     ,SUBMENU(WIFI_CONNECTION)
     
);
#define MAX_DEPTH 5

MENU_OUTPUTS(out, MAX_DEPTH
  ,LIQUIDCRYSTAL_OUT(lcd,{0,0,20,4})
  ,NONE
);

NAVROOT(nav,mainMenu,MAX_DEPTH,in,out);//the navigation root object

result alert(menuOut& o,idleEvent e) {
  if (e==idling) {
    o.setCursor(0,0);
    o.print("alert test");
    o.setCursor(0,1);
    o.print("[select] to continue...");
  }
  return proceed;
}

result doAlert(eventMask e, prompt &item) {
  nav.idleOn(alert);
  return proceed;
}

result idle(menuOut& o,idleEvent e) {
  ScheduleAuto=false;
  count=false;
  return proceed;
}

void setup() {
  Timer1.initialize(1000);
  Timer1.attachInterrupt(timerIsr);
  Serial.begin(115200);
  Serial3.begin(115200);
  pinMode(encBtn,INPUT_PULLUP);
  pinMode(R0,OUTPUT);
  pinMode(R1,OUTPUT);
  pinMode(R2,OUTPUT);
  pinMode(R3,OUTPUT);
  pinMode(R4,OUTPUT);
  pinMode(R5,OUTPUT);
  pinMode(R6,OUTPUT);
  pinMode(R7,OUTPUT);
  
  pinMode(u1,INPUT);
  pinMode(u2,INPUT);
  pinMode(u3,INPUT);
  pinMode(u4,INPUT);
  pinMode(u5,INPUT);
  pinMode(u6,INPUT);
  pinMode(u7,INPUT);
  pinMode(u_8,INPUT);

  pinMode(p1,INPUT);
  pinMode(p2,INPUT);
  pinMode(p3,INPUT);
  pinMode(p4,INPUT);
  pinMode(p5,INPUT);
  pinMode(p6,INPUT);
  pinMode(p7,INPUT);
  pinMode(p8,INPUT);
  
  digitalWrite(R0,LOW);
  digitalWrite(R1,LOW);
  digitalWrite(R2,LOW);
  digitalWrite(R3,LOW);
  digitalWrite(R4,LOW);
  digitalWrite(R5,LOW);
  digitalWrite(R6,LOW);
  digitalWrite(R7,LOW);
  while(!Serial);
  Serial.println("Arduino Menu Library");
  Serial.flush();
  lcd.begin(20,4);
  DateTime now=rtc.now();
  rtc.begin();
  rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
  nav.idleTask=idle;//point a function to be used when menu is suspended
  nav.showTitle=false;
  runner.init();
  runner.startNow();
  runner.addTask(t1);
  runner.addTask(t2);
 
  lcd.setCursor(4, 0);
  lcd.print(" Welcome to");
  lcd.setCursor(0, 1);
  lcd.print("Cenaura Technologies ");
  lcd.setCursor(0, 2);
  lcd.print("Disinfection Control");
  lcd.setCursor(4, 3);
  lcd.print("ling System");
  delay(5000);
}

void loop() {
  nav.poll();
  delay(500);//simulate a delay as if other tasks are running
  unit1read();
  unit2read();
  unit3read();
  unit4read();
  unit5read();
  unit6read();
  unit7read();
  unit8read();
if(ScheduleAuto==true)
{
  //scheduling();
  t1.enable();
}
else
 t1.disable();
if(count==true)
{
  t2.enable();
  //nowtime();
}
else
 t2.disable();
  runner.execute();
  }
