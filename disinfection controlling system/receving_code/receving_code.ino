#include <ArduinoJson.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>

int counter1,counter2,counter3,counter4,counter5,counter6,counter7,counter8;
float UV_index, UV_index1, UV_index2, UV_index3, UV_index4, UV_index5, UV_index6, UV_index7;

// Replace with your network credentials
const char* ssid     = "Zeptogreens";
const char* password = "Zeptogreens20";

// Change the variable to your Raspberry Pi IP address, so it connects to your MQTT broker
const char* mqtt_server = "192.168.1.5";
// MQTT Broker IP example
//const char* mqtt_server = "192.168.1.144";

// Initializes the espClient. You should change the espClient name if you have multiple ESPs running in your home automation system
WiFiClient espClient;
PubSubClient client(espClient);

// Don't change the function below. 
// This function connects your ESP8266 to your router
void setup_wifi() {
  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("WiFi connected - ESP IP address: ");
  Serial.println(WiFi.localIP());
}

// This functions is executed when some device publishes a message to a topic that your ESP8266 is subscribed to
// Change the function below to add logic to your program, so when a device publishes a message to a topic that 
// your ESP8266 is subscribed you can actually do something
void callback(String topic, byte* message, unsigned int length) {
  Serial.print("Message arrived on topic: ");
  Serial.print(topic);
  Serial.print(". Message: ");
  String messageTemp;
  
  for (int i = 0; i < length; i++) {
    Serial.print((char)message[i]);
    messageTemp += (char)message[i];
  }
  Serial.println();

  // Feel free to add more if statements to control more GPIOs with MQTT
  // If a message is received on the topic esp8266/output, you check if the message is either on or off. 
  // Turns the output according to the message received
  if(topic=="esp8266/output"){
    Serial.print("Changing output to ");
    if(messageTemp == "on"){
     // digitalWrite(output, LOW);
      Serial.print("on");
    }
    else if(messageTemp == "off"){
//      digitalWrite(output, HIGH);
      Serial.print("off");
    }
  }
  Serial.println();
}

// This functions reconnects your ESP8266 to your MQTT broker
// Change the function below if you want to subscribe to more topics with your ESP8266 
void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Create a random client ID
    String clientId = "ESP8266Client-";
    clientId += String(random(0xffff), HEX);
    // Attempt to connect
    if (client.connect(clientId.c_str())) {
      Serial.println("connected");  
      // Subscribe or resubscribe to a topic
      // You can subscribe to more topics (to control more outputs)
      client.subscribe("esp8266/output");  
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void setup() {
  Serial.begin(115200);
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback); 
}

void loop()
{
if (!client.connected()) {
    reconnect();
  }
  client.loop();

    
while(Serial.available()>0)
{
  StaticJsonBuffer<1000> jsonBuffer;
  JsonObject& data = jsonBuffer.parseObject(Serial);
  if (data == JsonObject::invalid()) {
    jsonBuffer.clear();
    return;
 }
//-------------1-------------------------
 Serial.println("JSON Object Recieved");
 String status1 = data[("STATUS1")];
  Serial.print("Recieved STATUS1:  ");
  Serial.println(status1);
  UV_index=data["MODULE1"]; 
   Serial.print("Recieved INTENSITY1:  ");
  Serial.println(UV_index);
  counter1=data["INTRODER1"] ; 
  Serial.print("Recieved INTRODER1:  ");
  Serial.println(counter1);
  client.publish("esp8266/status1", status1.c_str());
  client.publish("esp8266/intensity1", String(UV_index).c_str());
  client.publish("esp8266/introder", String(counter1).c_str());
//---------------2-----------------------
  String status2 = data[("STATUS2")];
  Serial.print("Recieved STATUS2:  ");
  Serial.println(status2);
  UV_index1=data["MODULE2"]; 
   Serial.print("Recieved INTENSITY2:  ");
  Serial.println(UV_index1);
  counter2=data["INTRODER2"] ; 
  Serial.print("Recieved INTRODER2:  ");
  Serial.println(counter2);
 client.publish("esp8266/status2", status2.c_str());
  client.publish("esp8266/intensity2", String(UV_index1).c_str());
  client.publish("esp8266/introder2", String(counter2).c_str());
//--------------3-----------------------------
   String status3 = data[("STATUS3")];
  Serial.print("Recieved STATUS3:  ");
  Serial.println(status3);
  UV_index2=data["MODULE3"]; 
   Serial.print("Recieved INTENSITY3:  ");
  Serial.println(UV_index2);
  counter3=data["INTRODER3"] ; 
  Serial.print("Recieved INTRODER3:  ");
  Serial.println(counter3);
 client.publish("esp8266/status3", status3.c_str());
  client.publish("esp8266/intensity3", String(UV_index2).c_str());
  client.publish("esp8266/introder3", String(counter3).c_str());
//---------------4------------------------
  String status4 = data[("STATUS4")];
  Serial.print("Recieved STATUS4:  ");
  Serial.println(status4);
  UV_index3=data["MODULE4"]; 
   Serial.print("Recieved INTENSITY3:  ");
  Serial.println(UV_index3);
  counter4=data["INTRODER4"] ; 
  Serial.print("Recieved INTRODER4:  ");
  Serial.println(counter4);
 client.publish("esp8266/status4", status4.c_str());
  client.publish("esp8266/intensity4", String(UV_index3).c_str());
  client.publish("esp8266/introder4", String(counter4).c_str());
//-----------------5----------------------------
String status5 = data[("STATUS5")];
  Serial.print("Recieved STATUS5:  ");
  Serial.println(status5);
  UV_index4=data["MODULE5"]; 
   Serial.print("Recieved INTENSITY5:  ");
  Serial.println(UV_index4);
  counter5=data["INTRODER5"] ; 
  Serial.print("Recieved INTRODER5:  ");
  Serial.println(counter5);
 client.publish("esp8266/status5", status5.c_str());
  client.publish("esp8266/intensity5", String(UV_index4).c_str());
  client.publish("esp8266/introder5", String(counter5).c_str());
//----------------6----------------------
  String status6 = data[("STATUS6")];
  Serial.print("Recieved STATUS6:  ");
  Serial.println(status6);
  UV_index5=data["MODULE6"]; 
   Serial.print("Recieved INTENSITY6:  ");
  Serial.println(UV_index5);
  counter6=data["INTRODER6"] ; 
  Serial.print("Recieved INTRODER6:  ");
  Serial.println(counter6);
 client.publish("esp8266/status6", status6.c_str());
  client.publish("esp8266/intensity6", String(UV_index5).c_str());
  client.publish("esp8266/introder6", String(counter6).c_str());
//----------------------7----------------------
   String status7 = data[("STATUS7")];
  Serial.print("Recieved STATUS7:  ");
  Serial.println(status7);
  UV_index6=data["MODULE7"]; 
   Serial.print("Recieved INTENSITY7:  ");
  Serial.println(UV_index7);
  counter7=data["INTRODER7"] ; 
  Serial.print("Recieved INTRODER7:  ");
  Serial.println(counter7);
   client.publish("esp8266/status7", status7.c_str());
  client.publish("esp8266/intensity7", String(UV_index6).c_str());
  client.publish("esp8266/introder7", String(counter7).c_str());
//--------------------8------------------------
  String status8 = data[("STATUS8")];
  Serial.print("Recieved STATUS8:  ");
  Serial.println(status8);
  UV_index7=data["MODULE8"]; 
   Serial.print("Recieved INTENSITY8:  ");
  Serial.println(UV_index7);
  counter8=data["INTRODER8"] ; 
  Serial.print("Recieved INTRODER8:  ");
  Serial.println(counter8);
   client.publish("esp8266/status8", status8.c_str());
  client.publish("esp8266/intensity8", String(UV_index7).c_str());
  client.publish("esp8266/introder8", String(counter8).c_str());
}
}
